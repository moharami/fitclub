import React from 'react';
import { AppRegistry } from 'react-native';
import App from './src/app';
import {Provider} from 'react-redux';
import {store} from './src/config/store';
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

const Application = () => {
    return ( <App/>
        // <Provider store={store}>
        //
        // </Provider>
    );
};
AppRegistry.registerComponent('fitclub', () => App);


