import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(251, 251, 251)',
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999
    },
    top: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
        paddingRight: 15,
        paddingLeft: 15,
        backgroundColor: 'rgb(229, 57, 57)'

    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    headerTitle: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 20,
        paddingRight: '10%'
    },
    navbar: {
        flexDirection: 'row',
        // position: 'absolute',
        // top: 270,
        // right: 0,
        // left: 0,
        // zIndex: 9999,
        paddingRight: 15,
        paddingLeft: 15,
        backgroundColor: 'white'
    },
    categories: {
        flexDirection: 'row',
        // position: 'absolute',
        // top: 380,
        // right: 0,
        // left: 0,
        // zIndex: 9999,
        paddingRight: 15,
        paddingLeft: 15,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgray',
        marginBottom: 30,
        backgroundColor: 'white'
    },
    slideshowContainer: {
        flexDirection: 'row',
        // position: 'absolute',
        // top: 80,
        // right: 0,
        // left: 0,
        // zIndex: 9999,
        backgroundColor: 'white'

    },
    body: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 15,
        paddingRight: 15,
        marginBottom: 20
    },
    bodyContainer: {
        // paddingBottom: 200,
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 100
    },
    scroll: {
        paddingTop: 60,
        flex: 1
    },
    footerContainer: {
        position: 'absolute',
        flex: 1,
        bottom: 0,
        right: 0,
        left: 0
    },
    buttonTitle: {
        fontSize: 15,
        color: 'lightgray'
    },
    navitemContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        transform: [
            {rotateY: '180deg'},
        ],
        paddingTop: 25,
        paddingBottom: 30
    },
    navContainer: {
        alignItems: "center",
        justifyContent: 'center',
        width: 84,
        height: 96,
        paddingBottom: 40,
        marginLeft: 20

    },
    text: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        // paddingBottom: 55,
        position: 'absolute',
        bottom: 15

    }
});
