import React, {Component} from 'react';
import {ScrollView, View, TouchableOpacity, Text, AsyncStorage, TextInput, Picker, BackHandler, Alert} from 'react-native';
import styles from './styles'
import LinearGradient from 'react-native-linear-gradient'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {Actions} from 'react-native-router-flux'
import FooterMenu from '../../components/footerMenu'
import Axios from 'axios'
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://fitclub.ws';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import {connect} from 'react-redux';
import {store} from '../../config/store';

class AddTickets extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            data: [],
            title: '',
            status: 0,
            part: 1,
            description: '',
            departmentArray: []
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    addTicketMethod() {
        this.setState({loading: true});
        AsyncStorage.getItem('token').then((info) => {
            const newInfo = JSON.parse(info);
            Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
            console.log('comment token', newInfo.token);
            Axios.post('/ticketing/create', {
                title: this.state.title,
                priority: this.state.status,
                content: this.state.description,
                department_id: this.state.part
            }).then(response=> {
                this.setState({loading: false});
                Alert.alert('','تیکت با موفقیت افزوده شد')
                console.log('ttttticket', response)
            })
                .catch((error) => {
                    Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        this.setState({loading: false});
                    }
                );
        });
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

        AsyncStorage.getItem('token').then((info) => {
            const newInfo = JSON.parse(info);
            Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
            console.log('comment token', newInfo.token);
            Axios.get('/training/programs/show').then(response => {
                const data = response.data;
                store.dispatch({type: 'NEW_PROGRAMS_FETCHED', payload: response.data});
                console.log('program in ticket response',response.data);
                this.setState({programs: data})

                Axios.get('/ticketing/department/show').then(response => {
                    // store.dispatch({type: 'NEW_POSTS_FETCHED', payload: response.data.data.blog.data});
                    let resoponses = [];
                    resoponses = response.data.data;
                    resoponses.map((item) => {
                        const itemValue = {id: item.id, title: item.title}
                        if(item.title === 'تغذیه'){
                            console.log('programs',this.props.programs)
                            console.log(this.props.programs)
                            if(this.props.programs.diet){
                                this.state.departmentArray.push(itemValue)
                            }
                        }
                        else if(item.title === 'تمرین'){
                            if(this.props.programs.exercise){
                                this.state.departmentArray.push(itemValue)
                            }
                        }
                        else if(item.title === 'مکمل'){
                            if(this.props.programs.complementary){
                                this.state.departmentArray.push(itemValue)
                            }
                        }
                        else {
                            this.state.departmentArray.push(itemValue)
                        }
                    });
                    console.log('departmentArray',this.state.departmentArray)
                    this.setState({loading: false});
                })
                    .catch(function (error) {
                            console.log(error.response);
                        }
                    );



            })
                .catch((error) =>{
                        console.log(error.response);
                    }
                );
        });




    }
    render() {
        const {programs} = this.props;
        if(this.state.loading)
            return (<Loader txtColor="red" color='red' />);
        else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                            <View style={styles.iconContainer}>
                                <TouchableOpacity onPress={() => Actions.push('tickets')}>
                                    <FIcon name="arrow-left" size={20} color="white" style={{paddingRight: 20}} />
                                </TouchableOpacity>
                                <Icon name="bell-o" size={23} color="white" />
                            </View>
                            <Text style={styles.headerTitle}>افزودن</Text>
                            <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                                <Icon name="bars" size={20} color="white" />
                            </TouchableOpacity>
                        </LinearGradient>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <Text style={styles.text}>عنوان</Text>
                            <TextInput
                                placeholder={this.props.placeholder}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.title}
                                style={{
                                    height: 40,
                                    backgroundColor: 'white',
                                    paddingRight: 15,
                                    width: '100%',
                                    color: 'gray',
                                    fontSize: 14,
                                    textAlign: 'right',
                                    fontFamily: 'IRANSansMobile(FaNum)',
                                    borderColor: 'lightgray',
                                    borderWidth: 1

                                }}
                                onChangeText={(text) => this.setState({title: text})}
                            />
                            <Text style={styles.text}>وضعیت</Text>
                            <Picker
                                mode="dropdown"
                                style={styles.picker}
                                selectedValue={this.state.status}
                                onValueChange={itemValue => this.setState({ status: itemValue })}>
                                <Picker.Item  label="اضطراری" value={0} />
                                <Picker.Item  label="مهم" value={1} />
                                <Picker.Item  label="عادی" value={3} />
                            </Picker>
                            <Text style={styles.text}>بخش</Text>
                            <Picker
                                mode="dropdown"
                                style={styles.picker}
                                selectedValue={this.state.part}
                                onValueChange={itemValue => this.setState({ part: itemValue })}>
                                <Picker.Item  label="تغذیه" value={1} />
                            </Picker>
                            <Text style={styles.text}>نوع دپارتمان</Text>
                            <Picker
                                mode="dropdown"
                                style={styles.picker}
                                selectedValue={this.state.status}
                                onValueChange={itemValue => this.setState({ status: itemValue })}>
                                {
                                    this.state.departmentArray.map((item, index) =>
                                        <Picker.Item  label={item.title} value={item.id} key={index} />
                                    )}
                            </Picker>
                            <Text style={styles.text}>توضیح کامل</Text>
                            <TextInput
                                multiline = {true}
                                numberOfLines = {3}
                                value={this.state.text}
                                onChangeText={(text) => this.setState({description: text})}
                                placeholderTextColor={'rgb(142, 142, 142)'}
                                underlineColorAndroid='transparent'
                                placeholder="توضیحات..."
                                style={{
                                    // height: 45,
                                    paddingRight: 15,
                                    width: '100%',
                                    fontSize: 18,
                                    color: 'rgb(142, 142, 142)',
                                    textAlign:'right',
                                    borderWidth: 1,
                                    borderColor: 'rgb(236, 236, 236)',
                                    borderRadius: 5,
                                    backgroundColor: 'white'
                                }}
                            />
                            <View style={styles.btnContainer}>
                                <TouchableOpacity style={[styles.item, {backgroundColor: 'rgb(0, 123, 255)'}]} onPress={() => this.addTicketMethod()}>
                                    <Text style={styles.buttonText}>ارسال</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                    <View style={styles.footerContainer}>
                        <FooterMenu />
                    </View>
                </View>
            );
    }
}
function mapStateToProps(state) {
    return {
        programs: state.posts.programs,
    }
}
export default connect(mapStateToProps)(AddTickets);
