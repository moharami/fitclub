import React, {Component} from 'react';
import {
    ImageBackground,
    View,
    TouchableOpacity,
    Text,
    TextInput,
    Image,
    AsyncStorage,
    BackHandler,
    Alert,
    KeyboardAvoidingView
} from 'react-native';
import logo from '../../assets/confirmLogo.png'
import styles from './styles'
import {Actions} from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import Axios from 'axios'

Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://fitclub.ws';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'

class Confirmation extends Component {

    constructor(props) {
        super(props);
        this.state = {
            text: '',
            loading: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }

    confirmCode() {
        if (this.state.text === '')
            Alert.alert('', 'لطفا کد را وارد کنید');
        else {
            this.setState({loading: true});
            Axios.post('/signin', {
                mobile: this.props.mobile,
                code: this.state.text
            }).then(response => {
                this.setState({loading: false});
                console.log('confirm data', response);

                const info = {'token': response.data, 'mobile': this.props.mobile, 'code': this.state.text};
                const newwwwAsync = AsyncStorage.setItem('token', JSON.stringify(info));
                console.log('newwwwAsync', newwwwAsync);
                Actions.push('home');
            })
                .catch((error) => {
                        Alert.alert('', 'خطایی رخ داده مجددا تلاش نمایید');
                        this.setState({loading: false});
                    }
                );
        }
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    onBackPress() {
        Actions.pop({refresh: {refresh: Math.random()}});
        return true;
    };

    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }

    render() {
        return (
            <View style={{backgroundColor: 'rgb(253, 87, 87)', flex: 1}}>
                {
                    this.state.loading ?
                        <View style={{paddingTop: '70%', paddingBottom: '30%'}}>
                            <Loader send={false}/>
                        </View>
                        :
                        <LinearGradient colors={['rgb(253, 87, 87)', 'rgb(229, 57, 57)', 'rgb(223, 50, 52)']}
                                        style={{flex:1}}>
                            <View style={styles.container}>

                                <Image source={logo} style={styles.image}/>
                                {/*</View>*/}
                                <View style={styles.body}>
                                    <View style={styles.content}>
                                        <View style={styles.row}>
                                            <TextInput
                                                placeholder="کد فعالسازی"
                                                placeholderTextColor={'white'}
                                                underlineColorAndroid='transparent'
                                                value={this.state.text}
                                                style={{
                                                    height: 55,
                                                    backgroundColor: 'rgb(205, 49, 52)',
                                                    paddingRight: 15,
                                                    width: '40%',
                                                    fontSize: 18,
                                                    color: 'white',
                                                    textAlign: 'right'
                                                }}
                                                onLayout={this.onLayout}
                                                onChangeText={(text) => this.setState({text})}/>
                                            <TouchableOpacity onPress={() => this.confirmCode()}
                                                              style={styles.advertise}>
                                                <View>
                                                    <Text style={styles.buttonTitle}>تایید کد</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                        <Text style={styles.label}>کد فعال سازی به شماره {this.props.mobile} ارسال شد
                                            برای
                                            ورود به برنامه لازم است کد فعال سازی را در کادر زیر وارد کنید.</Text>
                                    </View>
                                    <View style={styles.footer}>
                                        <TouchableOpacity onPress={() => Actions.push('Login')}>
                                            <Text style={styles.footerTxt}>تغییر شماره موبایل</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => Actions.push('Login')}>
                                            <Text style={styles.footerTxt}>ارسال مجدد کد فعال سازی (15)</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </LinearGradient>
                }
            </View>

        );
    }
}

export default Confirmation;