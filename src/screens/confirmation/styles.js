import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        // paddingTop: 100,

    },
    imageContainer: {
        position: 'relative',
        alignItems: 'center',
        justifyContent: 'center',
        top: 60,
        right: 0,
        left: 0
    },
    image: {
        // width: '100%',
        // position: 'absolute',
        // top: 60,
        // height: '100%',
        // alignItems: 'center',
        // justifyContent: 'center',
        // top: 60,
        // zIndex: 9999,


    },
    body: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: '25%'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    label: {
        paddingBottom: 10,
        color: 'rgba(255, 255, 255, .8)',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingRight: 60,
        paddingLeft: 60,
        paddingTop: 15
    },
    advertise: {
        width: '23%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        padding: 15,
        marginLeft: 15

    },
    buttonTitle: {
        fontSize: 13,
        color: 'gray',
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    footer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 20,
        paddingTop: 50,
    },
    footerTxt: {
        color: 'lightgray',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12


    }

});
