import React, {Component} from 'react';
import {ScrollView, View, TouchableOpacity, Text, AsyncStorage, TextInput, BackHandler, Alert} from 'react-native';
import styles from './styles'
import AppSearchBar from '../../components/searchBar'
import SearchItem from '../../components/searchItem'
import LinearGradient from 'react-native-linear-gradient'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {Actions} from 'react-native-router-flux'
import FooterMenu from '../../components/footerMenu'
import Axios from 'axios'
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://fitclub.ws';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import Question from '../../components/question'


class TicketDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            data: [],
            description: ''
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    reply() {

        this.setState({loading: true});
        AsyncStorage.getItem('token').then((info) => {
            const newInfo = JSON.parse(info);
            Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
            console.log('comment token', newInfo.token);
            Axios.post('/ticketing/answer/create', {
                id: this.props.item.id,
                content: this.state.description
            }).then(response=> {
                this.setState({loading: false});
                Alert.alert('پاسخ جدیدافزوده شد');
                console.log('ttttticket', response)
            })
            .catch((error) => {
                Alert.alert('', 'خطایی رخ داده مجددا تلاش نمایید');
                this.setState({loading: false});

                }
            );
        });
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        if(this.state.loading)
            return (<Loader txtColor="red" color='red' />);
        else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                            <View style={styles.iconContainer}>
                                <TouchableOpacity onPress={() => Actions.push('tickets')}>
                                    <FIcon name="arrow-left" size={20} color="white" style={{paddingRight: 20}} />
                                </TouchableOpacity>
                                <Icon name="bell-o" size={23} color="white" />
                            </View>
                            <Text style={styles.headerTitle}>تیکت ها</Text>
                            <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                                <Icon name="bars" size={20} color="white" />
                            </TouchableOpacity>
                        </LinearGradient>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <View style={styles.ticketContainer}>
                                <Text style={styles.title}> تیکت {this.props.item.id}</Text>
                                <Text style={[styles.title, {paddingTop: 15}]}>{this.props.item.title}</Text>
                                <Text style={[styles.titleText, {paddingTop: 10}]}>{this.props.item.content}</Text>
                                <View style={styles.statusContainer}>
                                    <View style={[styles.item, {backgroundColor: 'rgb(255, 193, 7)', marginRight: 15}]}>
                                        <Text style={styles.buttonText}>{this.props.item.status === 1 ? 'پاسخ مشتری' : (this.props.item.status === 0 ? 'بسته شده' : 'پاسخ داده شده')}</Text>
                                    </View>
                                    <View style={[styles.item, {backgroundColor: 'rgb(255, 193, 7)'}]}>
                                        <Text style={styles.buttonText}>{this.props.item.priority === 1 ? 'مهم' : (this.props.item.status === 0 ? 'اضطراری' : 'عادی')}</Text>
                                    </View>
                                </View>
                            </View>
                            <Text style={styles.title}>پاسخ ها</Text>
                            {
                                this.props.item.answers.map((item) =>
                                    <View style={styles.replyContainer} key={item.id}>
                                        <Text style={styles.titleText}>{item.content}</Text>
                                    </View>
                                )
                            }
                            <TextInput
                                multiline = {true}
                                numberOfLines = {2}
                                value={this.state.text}
                                onChangeText={(text) => this.setState({description: text})}
                                placeholderTextColor={'rgb(142, 142, 142)'}
                                underlineColorAndroid='transparent'
                                placeholder="ارسال پاسخ جدید"
                                style={{
                                    // height: 45,
                                    paddingRight: 15,
                                    width: '100%',
                                    fontSize: 18,
                                    color: 'rgb(142, 142, 142)',
                                    textAlign:'right',
                                    borderWidth: 1,
                                    borderColor: 'rgb(236, 236, 236)',
                                    borderRadius: 5,
                                    marginTop: 30,
                                    backgroundColor: 'white'
                                }}
                            />
                            <View style={styles.btnContainer}>
                                <TouchableOpacity style={[styles.item, {backgroundColor: 'rgb(0, 123, 255)', width: '30%'}]} onPress={() => this.reply()}>
                                    <Text style={styles.buttonText}>ارسال</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                    <View style={styles.footerContainer}>
                        <FooterMenu />
                    </View>
                </View>
            );
    }
}
export default TicketDetail;
