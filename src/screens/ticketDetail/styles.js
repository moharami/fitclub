
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(251, 251, 251)',
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999
    },
    top: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
        paddingRight: 15,
        paddingLeft: 15
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    headerTitle: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 16,
        paddingRight: '10%'
    },
    searchContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 60,
        marginBottom: 20
    },
    body: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
        paddingLeft: 15,
        paddingRight: 15,
        marginBottom: 20,
        direction: 'rtl'
    },
    bodyContainer: {
        paddingBottom: 200,
        padding: 30
    },
    filtering: {
        position: 'absolute',
        bottom: 25,
        width: '60%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 15,
        paddingLeft: 25,
        backgroundColor: 'rgb(231, 21, 56)',
        borderRadius: 30
    },
    scroll: {
        paddingTop: 60
    },
    filterContainer: {
        // flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonTitle: {
        fontSize: 15,
        color: 'lightgray'
    },
    footerContainer: {
        position: 'absolute',
        flex: 1,
        bottom: 0,
        right: 0,
        left: 0
    },
    text: {
        color: 'black',
        paddingTop: 10,
        paddingBottom: 10,
    },
    picker: {
        height: 40,
        backgroundColor: 'white',
        width: '100%',
        color: 'gray',
        borderColor: 'lightgray',
        borderWidth: 1
    },
    title: {
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14,
        textAlign: 'right'

    },
    titleText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        textAlign: 'right'
    },
    statusContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 15
    },
    item: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 5,
        paddingBottom: 5,
        paddingRight: 20,
        paddingLeft: 20,
        borderRadius: 5
    },
    buttonText: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14
    },
    label: {
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 16,
        paddingBottom: 10
    },
    replyContainer: {
        width: '100%',
        alignItems: 'flex-end',
        justifyContent: 'center',
        padding: 15,
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1
    },
    btnContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        paddingTop: 30
    }
});
