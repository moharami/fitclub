import React, {Component} from 'react';
import { View, TouchableOpacity, Text, Image, ScrollView, BackHandler} from 'react-native';
import logo from '../../assets/profilePhoto.png'
import styles from './styles'
import {Actions} from 'react-native-router-flux';
import AppTextinput from '../../components/appTextinput'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import ImagePicker from 'react-native-image-picker';
import layer from '../../assets/layer.png'
import PersianCalendarPicker from'react-native-persian-calendar-picker';
import Axios from 'axios'
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://fitclub.ws';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import moment from 'moment'

class Profile extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            active: 'male',
            fill1: 0,
            fill2: 0,
            fill3: 0,
            fill4: 0,
            fill5: 0,
            imgSrc: '',
            natinalId: false,
            emailCorrect: true,
            selectedStartDate: null,
            showPicker: false,
            loading: false,
            iconComponent: <Icon name="close" size={16} color="red" style={styles.icon}  />
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    sexaulity(type){
        if(type === 'male')
            this.setState({active: 'male'});
        else
            this.setState({active: 'female'});
    }
    checkTextInput(fill, num){
        switch (num) {
            case 1:
                this.setState({fill1: fill});
                break;
            case 2:
                this.setState({fill2: fill});
                break;
            case 3:
                this.setState({fill3: fill});
                break;
            case 4:
                let iconComponent = null;
                const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                if (reg.test(fill) === true){
                    iconComponent =  <Icon name="check" size={16} color="rgb(64, 168, 159)" style={styles.icon} />;
                    this.setState({iconComponent: iconComponent, emailCorrect: true})
                }
                this.setState({fill4: fill});
                break;
            case 5:
                this.setState({fill5: fill});
                if(fill.length ===10) {
                    this.setState({natinalId: true});
                }
                else
                    this.setState({natinalId: false});

                break;
            default:
                break;
        }
        this.setState()
    }
    selectPhotoTapped() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    imgSrc: response.uri
                })
            }
        });
    }
    onDateChange(date) {
        this.setState({ selectedStartDate: date });
        setTimeout(() => {this.setState({showPicker: false})}, 500)

    }
    openPicker() {
        this.setState({ showPicker: true });
    }
    signUp() {
        if(this.state.fill1.length < 2 || this.state.fill2.length < 2 || this.state.selectedStartDate === null || !this.state.natinalId || !this.state.emailCorrect ){
            alert('لطفا داده ها را درست وارد کنید');
        }
        else{
            console.log( moment(this.state.selectedStartDate).format('Y-M-D'));
            this.setState({loading: true});
            Axios.post('/signup', {
                fname: this.state.fill1,
                lname: this.state.fill2,
                mobile: this.props.mobile,
                sex: this.state.active,
                birthday:  moment(this.state.selectedStartDate).format('Y-M-D'),
                national_id: this.state.fill5,
                email: this.state.fill4,
                photo: this.state.imgSrc
            }).then(response=> {
                this.setState({loading: false});
                console.log('signup info', response);
                if(response.data.data === true){
                    Actions.push('Login')
                }
                else {
                    Actions.push('Login')
                }
                // if(response.data.data === false){
                //     Actions.profile({mobile: this.state.text});
                // }
                // else {
                //     Actions.confirmation({mobile: this.state.text});
                // }
            })
            .catch((error) => {
                alert('خطایی رخ داده مجددا تلاش نمایید');
                this.setState({loading: false});
                }
            );
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        return (
        <ScrollView style={{backgroundColor: 'rgb(253, 87, 87)'}}>
            {
                this.state.loading ?
                    <View style={{paddingTop: '70%', paddingBottom: '20%'}}>
                        <Loader send={false}/>
                    </View>
                    :
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <View style={styles.top}>
                                <Icon name="arrow-left" size={20} color="white"/>
                                <Text style={styles.title}>تکمیل پروفایل</Text>
                            </View>
                            <TouchableOpacity onPress={() => this.selectPhotoTapped()}>
                                {
                                    this.state.imgSrc !== '' ?
                                        <View style={styles.imageContainer}>
                                            <Image style={styles.layer} source={layer}/>
                                            <Image style={styles.image} source={{uri: this.state.imgSrc}}/>
                                        </View>
                                        : <Image source={logo} style={styles.image}/>
                                }
                            </TouchableOpacity>
                        </View>

                        <View style={styles.body}>
                            <View style={styles.sexuality}>
                                <TouchableOpacity onPress={() => this.sexaulity('female')}
                                                  style={[styles.sexualityButton, {
                                                      backgroundColor: this.state.active === 'female' ? 'white' : 'rgb(225, 75, 76)',
                                                      marginRight: 8
                                                  }]}>
                                    <Icon name="venus" size={28}
                                          color={this.state.active === "female" ? "rgb(205, 49, 52)" : "white"}/>
                                    <Text style={[styles.sexualityText, {color: this.state.active === 'female' ? "rgb(205, 49, 52)" : "white"}]}>زن</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.sexaulity('male')}
                                                  style={[styles.sexualityButton, {
                                                      backgroundColor: this.state.active === 'male' ? 'white' : 'rgb(225, 75, 76)',
                                                      marginLeft: 8
                                                  }]}>
                                    <Icon name="mars" size={28}
                                          color={ this.state.active === "male" ? "rgb(205, 49, 52)" : "white"}/>
                                    <Text
                                        style={[styles.sexualityText, {color: this.state.active === "male" ? "rgb(205, 49, 52)" : "white"}]}>مرد</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.textinputContainer}>
                                <AppTextinput placeholder="نام" check={(fill) => this.checkTextInput(fill, 1)}
                                              fieldNumber={1}/>
                                {
                                    this.state.fill1.length > 1 ?
                                        <Icon name="check" size={16} color="rgb(64, 168, 159)" style={styles.icon}/> :
                                        <Icon name="close" size={16} color="red" style={styles.icon}/>
                                }
                            </View>
                            <View style={styles.textinputContainer}>
                                <AppTextinput placeholder="نام خانوادگی"
                                              check={(fill) => this.checkTextInput(fill, 2)}/>
                                {
                                    this.state.fill2.length > 1 ?
                                        <Icon name="check" size={16} color="rgb(64, 168, 159)" style={styles.icon}/> :
                                        <Icon name="close" size={16} color="red" style={styles.icon}/>
                                }
                            </View>
                            <View style={styles.textinputContainer}>
                                <AppTextinput placeholder="تاریخ تولد" check={(fill) => this.checkTextInput(fill, 3)}
                                              birthday openPicker={() => this.openPicker()}
                                              date={this.state.selectedStartDate}/>
                                {
                                    this.state.selectedStartDate !== null ?
                                        <Icon name="check" size={16} color="rgb(64, 168, 159)" style={styles.icon}/> :
                                        <Icon name="close" size={16} color="red" style={styles.icon}/>
                                }
                            </View>
                            <View style={styles.textinputContainer}>
                                <AppTextinput placeholder="ایمیل (اختیاری)"
                                              check={(fill) => this.checkTextInput(fill, 4)}/>
                                {
                                    this.state.iconComponent
                                }
                            </View>
                            <View style={styles.textinputContainer}>
                                <AppTextinput placeholder="کد ملی" check={(fill) => this.checkTextInput(fill, 5)}/>
                                {
                                    this.state.natinalId ?
                                        <Icon name="check" size={16} color="rgb(64, 168, 159)" style={styles.icon}/> :
                                        <Icon name="close" size={16} color="red" style={styles.icon}/>
                                }
                            </View>
                            {
                                this.state.showPicker ?
                                    <View style={{
                                        position: 'absolute',
                                        bottom: '30%',
                                        zIndex: 9999,
                                        backgroundColor: 'white'
                                    }}>
                                        <PersianCalendarPicker
                                            onDateChange={(date) => this.onDateChange(date)}
                                        />
                                    </View>
                                    : null
                            }

                            <TouchableOpacity onPress={() => this.signUp()} style={styles.advertise}>
                                <View>
                                    <Text style={styles.buttonTitle}>ذخیره اطلاعات</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
            }
        </ScrollView>
        );
    }
}
export default Profile;