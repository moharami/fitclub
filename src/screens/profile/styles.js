import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
        backgroundColor: 'rgb(252, 86, 86)',
        paddingBottom: 40,
        paddingTop: 20

    },
    top: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginBottom: 20,
        paddingLeft: 15
    },
    header: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        color: 'white',
        paddingLeft: '30%',
        fontSize: 18,
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    image: {
        width: 100,
        height: 110,
        marginBottom: 30
    },
    selectedImage: {
        width: 197,
        height: 182,
        marginBottom: 60

    },
    imageContainer: {
        position: 'relative',
        width: 100,
        height: 110,
        zIndex: 0
    },
    layer: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: 100,
        height: 110,
        zIndex: 9999
    },
    body: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 30,
        backgroundColor: 'rgb(252, 86, 86)',

    },
    label: {
        paddingBottom: 10,
        color: 'rgba(255, 255, 255, .8)',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingRight: 60,
        paddingLeft: 60,
    },
    sexuality: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 30
    },
    sexualityButton: {
        flexDirection: 'row',
        width: '33%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        padding: 10,
        marginTop: 10

    },
    advertise: {
        flexDirection: 'row',
        width: '70%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        padding: 8,
    },
    sexualityText: {
        fontSize: 14,
        paddingLeft: 20
    },
    buttonTitle: {
        fontSize: 14,
        color: 'red',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    textinputContainer: {
        position: 'relative',
        width: '70%',
        marginBottom: 20,
        height: 35,

    },
    icon: {
        position: 'absolute',
        top: 13,
        left: 10
    }

});
