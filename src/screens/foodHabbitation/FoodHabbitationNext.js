
import React, {Component} from 'react';
import {View, ScrollView, Text, StatusBar, Image, TextInput, TouchableOpacity, BackHandler} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/Feather';
import LinearGradient from 'react-native-linear-gradient'
import FooterMenu from '../../components/footerMenu'
import habbitation from "../../assets/habbitation.png";
import Slider from "react-native-slider";
import SwitchButton from 'switch-button-react-native';
import {Actions} from 'react-native-router-flux'

class FoodHabbitationNext extends Component {
    constructor(props){
        super(props);
        this.state = {
            value: .5,
            activeSwitch: 1,
            color: 'rgb(227, 5, 26)',
            color2: 'rgb(227, 5, 26)',
            text: '',
            text2: '',
            text3: '',
            text4: '',
            text5: '',
            activeButton: 3
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    nextStep() {
        let data = this.props.data;
        data.meal = this.state.activeButton;
        data.habit = this.state.text2;
        data.twiceMeal  = this.state.text3;
        data.dontUseMeal   = this.state.text4;
        data.dietDescription    = this.state.text5;
        Actions.personalHabits({data: data, titleName: this.props.titleName, price: this.props.price});
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    }
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                        <TouchableOpacity onPress={() => Actions.push('foodHabbitation')} style={styles.iconContainer}>
                            <Icon name="arrow-left" size={25} color="white" />
                        </TouchableOpacity>
                        <View style={styles.headerTitleContainer}>
                            <Text style={styles.headerTitle}>عادت های غذایی</Text>
                        </View>
                    </LinearGradient>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <Image source={habbitation} />
                        <View style={styles.numberOfMealsContainer}>
                            <Text style={[styles.questionText, {padding: 20}]}>تعداد وعده های غذایی در شبانه روز</Text>
                            <View style={styles.numberOfMeals}>
                                <TouchableOpacity onPress={() => this.setState({activeButton: 5})}>
                                    <View style={[styles.circle, {backgroundColor: this.state.activeButton === 5 ? 'rgb(277, 5, 26)' : 'transparent'}]}>
                                        <Text style={[styles.questionText, {fontSize: 18, color: this.state.activeButton === 5 ? 'white' : 'black'}]}>5</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.setState({activeButton: 4})}>
                                    <View style={[styles.circle, {backgroundColor: this.state.activeButton === 4 ? 'rgb(277, 5, 26)' : 'transparent'}]}>
                                        <Text style={[styles.questionText, {fontSize: 18, color: this.state.activeButton === 4 ? 'white' : 'black' }]}>4</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.setState({activeButton: 3})}>
                                    <View style={[styles.circle, {backgroundColor: this.state.activeButton === 3 ? 'rgb(277, 5, 26)' : 'transparent'}]}>
                                        <Text style={[styles.questionText, {fontSize: 18, color: this.state.activeButton === 3 ? 'white' : 'black'}]}>3</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.setState({activeButton: 2})}>
                                    <View style={[styles.circle, {backgroundColor: this.state.activeButton === 2 ? 'rgb(277, 5, 26)' : 'transparent'}]}>
                                        <Text style={[styles.questionText, {fontSize: 18, color: this.state.activeButton === 2 ? 'white' : 'black'}]}>2</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.setState({activeButton: 1})}>
                                    <View style={[styles.circle, {backgroundColor: this.state.activeButton === 1 ? 'rgb(277, 5, 26)' : 'transparent'}]}>
                                        <Text style={[styles.questionText, {fontSize: 18, color: this.state.activeButton === 1 ? 'white' : 'black'}]}>1</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.question}>
                            <SwitchButton
                                onValueChange={(val) => this.setState({ activeSwitch: val, color: this.state.color === 'rgb(227, 5, 26)' ? 'rgb(32, 193, 189)': 'rgb(227, 5, 26)' })}
                                text1 = 'خیر'
                                text2 = 'بله'
                                switchWidth = {78}
                                switchHeight = {38}
                                switchdirection = 'ltr'
                                switchBorderRadius = {100}
                                switchSpeedChange = {500}
                                switchBorderColor = '#d4d4d4'
                                switchBackgroundColor = '#fff'
                                btnBorderColor = 'transparent'
                                btnBackgroundColor = {this.state.color}
                                fontColor = '#b1b1b1'
                                activeFontColor = '#fff'
                            />
                            <View style={styles.questionTextContainer}>
                                <Text style={styles.questionText}>عادت بد غذایی دارید؟</Text>
                                <Text style={styles.questionSubText}>مصرف تنقلات شیرینیجات فست فود یا خوردن غذا در ساعات نامناسب</Text>
                            </View>
                        </View>
                        <TextInput
                            multiline = {true}
                            numberOfLines = {2}
                            value={this.state.text2}
                            onChangeText={(text) => this.setState({text2: text})}
                            placeholderTextColor={'rgb(142, 142, 142)'}
                            underlineColorAndroid='transparent'
                            placeholder="توضیحات..."
                            style={{
                                // height: 45,
                                paddingRight: 15,
                                width: '90%',
                                fontSize: 18,
                                color: 'rgb(142, 142, 142)',
                                textAlign:'right',
                                borderWidth: 1,
                                borderColor: 'rgb(236, 236, 236)',
                                borderRadius: 5
                            }}
                        />
                        <View style={styles.question2}>
                            <Text style={[styles.questionText, {textAlign: 'right'}]}>مواد غذایی که بیشتر از دو بار در هفته استفاده می شود؟</Text>
                        </View>
                        <TextInput
                            multiline = {true}
                            numberOfLines = {2}
                            value={this.state.text3}
                            onChangeText={(text) => this.setState({text3: text})}
                            placeholderTextColor={'rgb(142, 142, 142)'}
                            underlineColorAndroid='transparent'
                            placeholder="توضیحات..."
                            style={{
                                // height: 45,
                                paddingRight: 15,
                                width: '90%',
                                fontSize: 18,
                                color: 'rgb(142, 142, 142)',
                                textAlign:'right',
                                borderWidth: 1,
                                borderColor: 'rgb(236, 236, 236)',
                                borderRadius: 5
                            }}
                        />
                        <View style={styles.question2}>
                            <Text style={[styles.questionText, {textAlign: 'right'}]}>وعده های غذایی که در طول روز مصرف نمی شود؟</Text>
                        </View>
                        <TextInput
                            multiline = {true}
                            numberOfLines = {2}
                            value={this.state.text4}
                            onChangeText={(text) => this.setState({text4: text})}
                            placeholderTextColor={'rgb(142, 142, 142)'}
                            underlineColorAndroid='transparent'
                            placeholder="توضیحات..."
                            style={{
                                // height: 45,
                                paddingRight: 15,
                                width: '90%',
                                fontSize: 18,
                                color: 'rgb(142, 142, 142)',
                                textAlign:'right',
                                borderWidth: 1,
                                borderColor: 'rgb(236, 236, 236)',
                                borderRadius: 5
                            }}
                        />
                        <View style={styles.question2}>
                            <Text style={[styles.questionText, {textAlign: 'right'}]}>آیا سایقه رژیم دارید؟</Text>
                        </View>
                        <TextInput
                            multiline = {true}
                            numberOfLines = {2}
                            value={this.state.text5}
                            onChangeText={(text) => this.setState({text5: text})}
                            placeholderTextColor={'rgb(142, 142, 142)'}
                            underlineColorAndroid='transparent'
                            placeholder="توضیحات..."
                            style={{
                                // height: 45,
                                paddingRight: 15,
                                width: '90%',
                                fontSize: 18,
                                color: 'rgb(142, 142, 142)',
                                textAlign:'right',
                                borderWidth: 1,
                                borderColor: 'rgb(236, 236, 236)',
                                borderRadius: 5
                            }}
                        />
                        <TouchableOpacity style={[styles.nextContainer, {marginTop: 40}]} onPress={() => this.nextStep()}>
                            <Icon name="arrow-right" size={25} color="rgb(77, 205, 202)" />
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                <View style={styles.footerContainer}>
                    <FooterMenu />
                </View>
            </View>
        );
    }
}
export default FoodHabbitationNext;

