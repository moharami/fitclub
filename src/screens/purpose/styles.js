
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(246, 246, 246)'
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999
    },
    top: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
        paddingRight: 15,
        paddingLeft: 15
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    headerTitleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerTitle: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 16,
        paddingRight: '45%'
    },
    bodyContainer: {
        paddingBottom: 120,
        borderWidth: 1,
        borderColor: 'rgb(237, 237, 237)',
        backgroundColor: 'white',
        paddingRight: 20,
        paddingLeft: 20,
        paddingTop: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },
    scroll: {
        paddingTop: 60,
        paddingRight: 15,
        paddingLeft: 15,
        marginBottom: 50,
    },
    footerContainer: {
        position: 'absolute',
        flex: 1,
        bottom: 0,
        right: 0,
        left: 0
    },
    featureRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonTitle: {
        fontSize: 15,
        color: 'lightgray'
    },
    nextContainer: {
        width: '60%',
        borderWidth: 1,
        borderColor: 'rgb(237, 237, 237)',
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 25
    }
});
