import React, {Component} from 'react';
import {View, ScrollView, Text, StatusBar, TouchableOpacity, BackHandler} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/Feather';
import LinearGradient from 'react-native-linear-gradient'
import FooterMenu from '../../components/footerMenu'
import PurposItem from "../../components/purposItem";
import PurposeModal from "./purposModal";
import {Actions} from 'react-native-router-flux';

class Purpose extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: .1,
            modalVisible: false,
            modalNumber: 0,
            data: [],
            data2: [],
            pointWeight: null
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    handlePress(id, type) {
        if(id ===7 || id ===8) {
            this.setState({modalVisible: !this.state.modalVisible, modalNumber: id-6});
        }
        else {
            const newData = this.state.data;
            newData[id-1] = type;
            this.setState({data: newData})
            // const newData = this.state.data;
            // newData.push(type);
            // this.setState({data: newData})
        }
    }
    setModalVisible() {
        this.setState({modalVisible: !this.state.modalVisible});
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    handleChange(value) {
        const newData = this.state.data;
        newData[this.state.modalNumber+6-1] = value.data;
        this.setState({data: newData, pointWeight: value.data })
    }
    nextStep() {
        let data = this.props.data;
        data.point = this.state.data;
        data.pointWeight = this.state.pointWeight;
        console.log('alll',data);
        Actions.pregnant({data: data, titleName: this.props.titleName, price: this.props.price});
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                        <TouchableOpacity onPress={() => Actions.push('practiceInfo')} style={styles.iconContainer}>
                            <Icon name="arrow-left" size={25} color="white" />
                        </TouchableOpacity>
                        <View style={styles.headerTitleContainer}>
                            <Text style={styles.headerTitle}>هدف</Text>
                        </View>
                    </LinearGradient>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <View style={styles.featureRow}>
                            <PurposItem imgSrc="image2" purpose="افزایش وزن" label="وزن هدف:"  labelValue="کیلوگرم" id={8} handlePress={() => this.handlePress(8)} data={this.state.data}  line />
                            <PurposItem imgSrc="image1" purpose="کاهش وزن" label="وزن هدف:" labelValue="کیلوگرم" id={7} handlePress={() => this.handlePress(7)} data={this.state.data}  line />
                        </View>
                        <View style={styles.featureRow}>
                            <PurposItem imgSrc="image4" purpose="چربی سوزی" label="" labelValue="" id={2} handlePress={() => this.handlePress(2, 'چربی سوزی')} data={this.state.data} />
                            <PurposItem imgSrc="image3" purpose="عضله سازی" label="" labelValue="" id={1} handlePress={() => this.handlePress(1, 'عضله سازی')} data={this.state.data} />
                        </View>
                        <View style={styles.featureRow}>
                            <PurposItem imgSrc="image6" purpose="پرورش اندام" label="" labelValue="" id={4} handlePress={() => this.handlePress(4, "پرورش اندام")} data={this.state.data} />
                            <PurposItem imgSrc="image5" purpose="فیتنس"  label="" labelValue="" id={3} handlePress={() => this.handlePress(3, "فیتنس")} data={this.state.data} />
                        </View>
                        <View style={styles.featureRow}>
                            <PurposItem imgSrc="image8" purpose="تندرستی" label="" labelValue="" id={6} handlePress={() => this.handlePress(6, 'تندرستی')} data={this.state.data} />
                            <PurposItem imgSrc="image7" purpose="درمان بیماری" label=""  id={5} handlePress={() => this.handlePress(5, 'درمان بیماری')} data={this.state.data} />
                        </View>
                        <TouchableOpacity style={styles.nextContainer} onPress={() => this.nextStep()}>
                            <Icon name="arrow-right" size={25} color="rgb(77, 205, 202)" />
                        </TouchableOpacity>
                        <PurposeModal
                            closeModal={() => this.closeModal()}
                            modalVisible={this.state.modalVisible}
                            modalNumber={this.state.modalNumber}
                            handleChange={(value) => this.handleChange(value)}
                            onChange={(visible) => this.setModalVisible(visible)}
                        />
                    </View>
                </ScrollView>
                <View style={styles.footerContainer}>
                    <FooterMenu />
                </View>
            </View>
        );
    }
}
export default Purpose;
