import React, {Component} from 'react';
import {ScrollView, View, TouchableOpacity, Text, AsyncStorage, BackHandler} from 'react-native';
import styles from './styles'
import LinearGradient from 'react-native-linear-gradient'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {Actions} from 'react-native-router-flux'
import FooterMenu from '../../components/footerMenu'
import Axios from 'axios'
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://fitclub.ws';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import Question from '../../components/question'


class CommonQuestion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: [],
            loading: true,
            data: []
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        AsyncStorage.getItem('token').then((info) => {
            const newInfo = JSON.parse(info);
            Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
            console.log('commmon token', newInfo.token);
            Axios.get('/qa/show').then(response => {
                const data = response.data.data;
                console.log('question response',response.data);
                this.setState({data: data, loading: false})
            })
            .catch((error) =>{
                console.log(error.response);
                }
            );
        });
    }
    render() {
        if(this.state.loading)
            return (<Loader txtColor="red" color='red' />);
        else return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                        <View style={styles.iconContainer}>
                            <TouchableOpacity onPress={() => Actions.push('UserProfile')}>
                                <FIcon name="arrow-left" size={20} color="white" style={{paddingRight: 20}} />
                            </TouchableOpacity>
                            <Icon name="bell-o" size={23} color="white" />
                        </View>
                        <Text style={styles.headerTitle}>سوالات متداول</Text>
                        <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                            <Icon name="bars" size={20} color="white" />
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        {/*<View style={styles.searchContainer}>*/}
                            {/*<AppSearchBar placeholder="جستجو ..." center />*/}
                        {/*</View>*/}
                        {
                            this.state.data? this.state.data.map((item) => <Question question={item.question} answer={item.answer} key={item.id} />): null
                        }
                    </View>
                </ScrollView>
                <View style={styles.footerContainer}>
                    <FooterMenu />
                </View>
            </View>
        );
    }
}
export default CommonQuestion;
