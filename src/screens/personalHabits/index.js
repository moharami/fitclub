import React, {Component} from 'react';
import {View, ScrollView, Text, StatusBar, Image, TextInput, TouchableOpacity, BackHandler} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/Feather';
import LinearGradient from 'react-native-linear-gradient'
import FooterMenu from '../../components/footerMenu'
import habits from "../../assets/habits.png";
import Slider from "react-native-slider";
// import CircularSlider from 'react-native-circular-slider';
import {TimePicker } from 'react-native-wheel-picker-android'
import {Actions} from 'react-native-router-flux'
import moment from 'moment'


class PersonalHabits extends Component {
    constructor(props){
        super(props);
        this.state = {
            value: .5,
            activeSwitch: 1,
            color: 'rgb(227, 5, 26)',
            color2: 'rgb(227, 5, 26)',
            text: '',
            text2: '',
            data1: 0,
            data2: 0
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    nextStep() {
        let data = this.props.data;
        data.wakeUp = moment(this.state.data1).format('LT');
        data.sleep =  moment(this.state.data2).format('LT');
        Actions.practiceInfo({data: data,  titleName: this.props.titleName, price: this.props.price});
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        // let arr = [1,2,3, 4, 5, 6, 7, 8, 9, 10 ,11 ,12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24];
        const minutesArray = ['00', '15', '30', '45'];
        const now = new Date();
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                        <TouchableOpacity onPress={() => Actions.push('foodHabbitationNext')} style={styles.iconContainer}>
                            <Icon name="arrow-left" size={25} color="white" />
                        </TouchableOpacity>
                        <View style={styles.headerTitleContainer}>
                            <Text style={styles.headerTitle}>عادت های شخصی</Text>
                        </View>
                    </LinearGradient>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <Image source={habits} />
                        <Text style={[styles.questionText, {alignSelf: 'flex-end'}]}>زمان بیدارشدن: </Text>
                        <TimePicker
                            minutes={minutesArray}
                            onTimeSelected={(date)=>this.setState({data1: date})}
                            initDate={now.toISOString()}/>

                        <Text style={[styles.questionText, {alignSelf: 'flex-end'}]}> زمان خوابیدن: </Text>
                        <TimePicker
                            minutes={minutesArray}
                            onTimeSelected={(date)=>this.setState({data2: date})}
                            initDate={now.toISOString()}/>
                        <TextInput
                            multiline = {true}
                            numberOfLines = {2}
                            value={this.state.text2}
                            onChangeText={(text) => this.setState({text2: text})}
                            placeholderTextColor={'rgb(142, 142, 142)'}
                            underlineColorAndroid='transparent'
                            placeholder="توضیحات..."
                            style={{
                                // height: 45,
                                paddingRight: 15,
                                width: '90%',
                                fontSize: 18,
                                color: 'rgb(142, 142, 142)',
                                textAlign:'right',
                                borderWidth: 1,
                                borderColor: 'rgb(236, 236, 236)',
                                borderRadius: 5
                            }}
                        />
                        <TouchableOpacity style={[styles.nextContainer, {marginTop: 15}]} onPress={() => this.nextStep()}>
                            <Icon name="arrow-right" size={25} color="rgb(77, 205, 202)" />
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                <View style={styles.footerContainer}>
                    <FooterMenu />
                </View>
            </View>
        );
    }
}
export default PersonalHabits;

