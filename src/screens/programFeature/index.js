import React, {Component} from 'react';
import {View, ScrollView, Text, StatusBar, TouchableOpacity, BackHandler} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/Feather';
import LinearGradient from 'react-native-linear-gradient'
import FooterMenu from '../../components/footerMenu'
import PublicFeature from "../../components/publicFeature";
import Slider from "react-native-slider";
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux'

class ProgramFeature extends Component {
    constructor(props){
        super(props);
        this.state = {
            value: .1
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        const {user} = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                        <TouchableOpacity onPress={() => Actions.push('AddProgram')} style={styles.iconContainer}>
                            <Icon name="arrow-left" size={25} color="white" />
                        </TouchableOpacity>
                        <View style={styles.headerTitleContainer}>
                            <Text style={styles.headerTitle}>مشخصات عمومی</Text>
                        </View>
                    </LinearGradient>
            </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <PublicFeature label="نام" placeholder={user.fname}  />
                        <PublicFeature label="نام خانوادگی" placeholder={user.lname}  />
                        <PublicFeature label="تاریخ تولد" placeholder={user.birthday}  />
                        <PublicFeature label="جنسیت"  placeholder={user.sex}  />
                        <PublicFeature label="کد ملی"  placeholder={user.national_id} />
                        <PublicFeature label="موبایل"  placeholder={user.mobile} />
                        <TouchableOpacity style={styles.nextContainer} onPress={() => Actions.bodyFeatures({titleName: this.props.titleName, price: this.props.price})}>
                            <Icon name="arrow-right" size={25} color="rgb(77, 205, 202)" />
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                {/*<View style={styles.sliderContainer}>*/}
                    {/*<Slider*/}
                        {/*thumbTintColor="#e9eaed"*/}
                        {/*minimumTrackTintColor="#e9eaed"*/}
                        {/*value={this.state.value}*/}
                        {/*thumbStyle={{borderWidth: 1, borderColor: '#e9eaed', backgroundColor: '#e9eaed', width: 15, height: 15}}*/}
                        {/*maximumTrackTintColor="red"*/}
                        {/*trackStyle={{width: '100%', height: 15, borderRadius: 15, backgroundColor: 'white', transform: [{ scaleX: -1}]}}*/}
                        {/*style={{width: '70%', height: 10, borderRadius: 15, backgroundColor: 'rgb(32, 193, 188)', transform: [{ scaleX: -1}] }}*/}
                        {/*onValueChange={value => this.setState({value: value })}*/}
                    {/*/>*/}
                {/*</View>*/}
                <View style={styles.footerContainer}>
                    <FooterMenu />
                </View>
            </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        user: state.auth.user
    }
}
export default connect(mapStateToProps)(ProgramFeature);
