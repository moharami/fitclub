import React, {Component} from 'react';
import {ScrollView, View, TouchableOpacity, Image, Text, TextInput, AsyncStorage, BackHandler, Share, Alert} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/Feather';
import FIcon from 'react-native-vector-icons/dist/FontAwesome';
import {Actions} from 'react-native-router-flux'
import ArticleSlideShow from '../../components/articleSlideshow'
import RelatedArticleItem from '../../components/relatedArticleItem'
import Comment from '../../components/comment'
import Axios from 'axios'
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://fitclub.ws';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import {store} from '../../config/store';
import {connect} from 'react-redux';
import 'moment/locale/fa';
import HTML from 'react-native-render-html';

class ArticleDeatil extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: ''
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        try {
            Axios.post('/blog/show', {id: this.props.item.id }).then(response=> {
                store.dispatch({type: 'DETAIL_POST_FETCHED', payload: response.data.data});
                this.setState({loading: false});
            })
            .catch((error) => {
                Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    this.setState({loading: false});
                }
            );
        }
        catch (error) {
            Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
            this.setState({loading: false});

        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    commentSend() {
        AsyncStorage.getItem('token').then((info) => {
        const newInfo = JSON.parse(info);
         Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
         console.log('comment token', newInfo.token);
         try {
             console.log('comment id',this.props.item.id);
             Axios.post('/comment', {comment: this.state.text, id: this.props.item.id}).then(response=> {
                 // store.dispatch({type: 'COMMENT_POST_FETCHED', payload: response.data});
                 Alert.alert('','نظر شما با موفقیت ثبت شد');

                 // if(response.data) {
                 //     this.setState({loading: false});
                 //     alert('نظر شما با موفقیت ثبت شد');
                 //     console.log('category detail', response.data);
                 // }

             })
                 .catch((error) => {
                     Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                     this.setState({loading: false});
                     }
                 );
         }
         catch (error) {
             Alert.alert('','خطا');
             this.setState({loading: false});

         }

        });
    }
    shareArticle() {
        Share.share({
            url: 'http://www.fitclub.ws.info/',
            title: 'سایت ما را با آدرس فوق مشاهده کنید'
        })
    }
    likeArticle() {
        AsyncStorage.getItem('token').then((info) => {
            const newInfo = JSON.parse(info);
            Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
            try {
                Axios.post('/blog/like', {id: this.props.item.id}).then(response=> {
                    Alert.alert('','مقاله لایک شد');
                })
                .catch((error) => {
                    Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    this.setState({loading: false});
                });
            }
            catch (error) {
                Alert.alert('','خطا');
                this.setState({loading: false});

            }

        });
    }
    render() {
        const {categoryDetail} = this.props;
        const myHtml = '<p style="color: white; lineHeight: 30;">' + this.props.categoryDetail.content + '</p>';

        return (
            <View style={styles.container}>
                {
                    this.props.categoryDetail.title ?
                        <View>
                            <View style={styles.header}>
                                <View style={styles.top}>
                                    <TouchableOpacity style={styles.iconContainer} onPress={() => Actions.push('home')}>
                                        <Icon name="arrow-left" size={25} color="white" />
                                    </TouchableOpacity>
                                    <View style={styles.headerTitleContainer}>
                                        {/*<Text style={styles.day}>27</Text>*/}
                                        {/*<Text style={styles.month}>خرداد</Text>*/}
                                    </View>
                                    <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                                        <Icon name="menu" size={20} color="white" />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <ScrollView style={styles.scroll}>
                                <View style={styles.scrollContainer}>
                                    <ArticleSlideShow sliderSrc={this.props.categoryDetail.attachments[0].uid} />
                                    <View style={styles.content}>
                                        <View style={styles.imageIconContainer}>
                                            <View style={styles.right}>
                                                <TouchableOpacity onPress={() => this.shareArticle()}>
                                                    <Icon name={"share-2"} size={22} color="gray" style={{paddingRight: 20}} />
                                                </TouchableOpacity>
                                                <Icon name={"bookmark"} size={22} color="gray" />
                                            </View>
                                            <View style={styles.left}>
                                                <Text style={styles.iconText}>{this.props.categoryDetail.commentcount}</Text>
                                                <Icon name={"message-circle"} size={22} color="gray" style={{paddingRight: 15}} />
                                                <Text style={styles.iconText}>{this.props.categoryDetail.like}</Text>
                                                <TouchableOpacity onPress={() => this.likeArticle()}>
                                                    <FIcon  name={"heart"} size={22} color="red" />
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        <Text style={styles.title}>{this.props.categoryDetail.title}</Text>
                                        <HTML html={myHtml} tagsStyles={{
                                            p: {
                                                direction: "rtl",
                                                paddingRight: 20,
                                                paddingLeft: 20,
                                                color: 'black',
                                                fontSize: 12,
                                                fontFamily: 'IRANSansMobile(FaNum)',
                                                lineHeight: 25

                                            },
                                            img: {
                                                width: 300,
                                                height: 200,
                                                marginRight: 'auto',
                                                marginLeft: 'auto'
                                            }
                                        }} />

                                        {/*<Text style={styles.contentText}>*/}
                                        {/*رژیم غذایی نوردیک در سال ۲۰۰۴ توسط گروهی از متخصصان تغذیه، دانشمندان و سرآشپزها به منظور بررسی نرخ چاقی در حال رشد و فعالیت‌های کشاورزی ناپایدارِ کشورهای اسکاندیناوی متمرکز شده است. به‌طور متوسط در مقایسه با رژیم‌های غذایی غرب، این رژیم شامل قند کمتر، چربی کمتر، دو برابر فیبر و دو برابر ماهی و غذاهای دریایی می‌شود.*/}
                                        {/*رژیم غذایی نوردیک در سال ۲۰۰۴ توسط گروهی از متخصصان تغذیه، دانشمندان و سرآشپزها به منظور بررسی نرخ چاقی در حال رشد و فعالیت‌های کشاورزی ناپایدارِ کشورهای اسکاندیناوی متمرکز شده است. به‌طور متوسط در مقایسه با رژیم‌های غذایی غرب، این رژیم شامل قند کمتر، چربی کمتر، دو برابر فیبر و دو برابر ماهی و غذاهای دریایی می‌شود.*/}
                                        {/*</Text>*/}
                                        {/*<Image source={{uri: 'https://cdn.isna.ir/d/2017/02/03/3/57431707.jpg?ts=1498045331768'}} style={styles.image} />*/}
                                        {/*<Text style={styles.title}>سال جدید انگیزه جدید</Text>*/}
                                        {/*<Text style={styles.contentText}>*/}
                                        {/*رژیم غذایی نوردیک در سال ۲۰۰۴ توسط گروهی از متخصصان تغذیه، دانشمندان و سرآشپزها به منظور بررسی نرخ چاقی در حال رشد و فعالیت‌های کشاورزی ناپایدارِ کشورهای اسکاندیناوی متمرکز شده است. به‌طور متوسط در مقایسه با رژیم‌های غذایی غرب، این رژیم شامل قند کمتر، چربی کمتر، دو برابر فیبر و دو برابر ماهی و غذاهای دریایی می‌شود.*/}
                                        {/*</Text>*/}
                                    </View>
                                    <View style={styles.relatedContent}>
                                        <Text style={styles.relatedTitle}>مطالب مرتبط</Text>
                                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ transform: [{ scaleX: -1}]}}>
                                            <View style={styles.slideContainers}>
                                                {
                                                    this.props.relatedItems.map((item)=> <RelatedArticleItem item={item} key={item.id} relatedItems={this.props.relatedItems} />
                                                    )
                                                }
                                            </View>
                                        </ScrollView>
                                    </View>
                                    <View style={styles.profile}>
                                        {this.props.categoryDetail.user.attachments ?
                                            <Image source={{uri: "http://fitclub.ws/files?uid="+this.props.categoryDetail.user.attachments.uid+"&width=300&height=100"}} style={styles.profileImage} />
                                        : <Image source={{uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTQzoL7LYphmQJUakxuGQMoTicLEMGTdb9fhBXoabCzfW44aNaZEA'}} style={styles.profileImage} />

                                        }
                                        <Text style={styles.name}>{this.props.categoryDetail.user.fname} {this.props.categoryDetail.user.lname}</Text>
                                    </View>
                                    <View style={styles.textinputContainer}>
                                        <TextInput
                                            multiline = {true}
                                            numberOfLines = {4}
                                            value={this.state.text}
                                            onChangeText={(text) => this.setState({text: text})}
                                            placeholderTextColor={'lightgray'}
                                            underlineColorAndroid='transparent'
                                            placeholder="دیدگاه خود را وارد کنید..."
                                            style={{
                                                // height: 45,
                                                paddingRight: 15,
                                                width: '90%',
                                                fontSize: 20,
                                                color: 'gray',
                                                textAlign:'right',
                                                borderWidth: 1,
                                                borderColor: 'lightgray',
                                                borderRadius: 5
                                            }}
                                        />
                                    </View>
                                    <View style={styles.commentButtonContainer}>
                                        <TouchableOpacity onPress={() => this.commentSend()} style={styles.commentButton}>
                                            <View>
                                                <Text style={styles.buttonText}>ارسال دیدگاه</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    {
                                        this.props.categoryDetail.comments.length !== 0 ? <Text style={styles.commentText}>دیدگاه ها</Text> : null

                                    }
                                    {
                                        this.props.categoryDetail.comments.map((item) => <Comment item={item} key={item.id} />)
                                    }


                                </View>
                            </ScrollView>
                        </View>
                        : null
                }

            </View>
        );
    }
}


function mapStateToProps(state) {
    return {
        categoryDetail: state.posts.categoryDetail,
    }
}
export default connect(mapStateToProps)(ArticleDeatil);
