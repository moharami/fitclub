
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({

    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(251, 251, 251)',
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999
    },
    top: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'transparent',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        paddingRight: 15,
        paddingLeft: 15
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    headerTitleContainer: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    day: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 25,
        textAlign: 'center',
        position: 'absolute',
        bottom: -10,
        zIndex: 9999,
    },
    month: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14,
        textAlign: 'center',
        position: 'absolute',
        top: 0,
        // right: 0,
        zIndex: 9999,

    },
    scroll: {
        paddingBottom: 240

    },
    scrollContainer: {
        backgroundColor: 'white',
        // paddingBottom: 200,
        // shadowOffset:{  width: 10,  height: 10,  },
        // shadowColor: 'black',
        // shadowOpacity: .5,
        elevation: 4,
        // shadowRadius: 20
    },
    content: {
        padding: 15
    },
    imageIconContainer: {
        flexDirection:'row',
        justifyContent: "space-between",
        alignItems: "center",
    },
    iconContent: {
        backgroundColor: "transparent",
        fontWeight: "bold",
        fontSize: 12,
        color: "white",
    },
    right: {
        flexDirection:'row',
        justifyContent: "flex-end",
        alignItems: "center",
    },
    left: {
        flexDirection:'row',
        justifyContent: "flex-start",
        alignItems: "center",
    },
    iconText: {
        paddingRight: 5,
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 16
    },
    title: {
        color: 'black',
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingBottom: 5,
        paddingTop: 15
    },
    contentText: {
        color: 'black',
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        lineHeight: 25
    },
    image: {
        width: '100%',
        height: 280,
        marginTop: 15
    },
    relatedContent: {
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderTopColor: 'rgb(237, 237, 237)',
        borderBottomColor: 'rgb(237, 237, 237)',
        paddingTop: 20,
        paddingBottom: 20,
        paddingRight: 15
    },
    relatedTitle: {
        color: 'black',
        fontSize: 18,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingBottom: 20,
    },
    slideContainers: {
        flexDirection: 'row',
        transform: [
            {rotateY: '180deg'},
        ],
    },
    profile: {
        justifyContent: "center",
        alignItems: "center",
        padding: 30
    },
    profileImage: {
        width: 55,
        height: 55,
        borderRadius: 55
    },
    name: {
        color: 'black',
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingTop: 10
    },
    textareaContainer: {
        height: 200,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        borderWidth: 1,
        borderColor: 'lightgray',
        borderRadius: 5
    },
    textarea: {
        width: '100%',
        textAlignVertical: 'top',  // hack android
        height: 170,
        fontSize: 12,

    },
    textinputContainer: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    commentButtonContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 40,
        borderBottomWidth: 1,
        borderBottomColor: 'rgb(237, 237, 237)',
        marginRight: 15,
        marginLeft: 15
    },
    commentButton: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgb(224, 6, 23)',
        padding: 12,
        borderRadius: 5,
        paddingRight: 15,
        paddingLeft: 15
    },
    buttonText: {
        fontSize: 15,
        color: 'rgba(255, 255, 255, .8)',
        fontWeight: 'bold',
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    commentText: {
        fontSize: 20,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingTop: 30,
        paddingRight: 15
    }
});
