import React, {Component} from 'react';
import {ScrollView, View, TouchableOpacity, Image, Text, TextInput, AsyncStorage, Linking, BackHandler} from 'react-native';
import styles from './styles'
import logo from '../../assets/payment.png'
import LinearGradient from 'react-native-linear-gradient'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {Actions} from 'react-native-router-flux'
import FooterMenu from '../../components/footerMenu'
import SwitchButton from '../../components/switchButton'
// import {store} from '../../config/store';
// import {connect} from 'react-redux';
import InputModal from './inputModal'

import Axios from 'axios';
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://fitclub.ws';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'

class Payment extends Component {
    constructor(props){
        super(props);
        this.state = {
            posts: [],
            loading: false,
            active: '',
            friTab: true,
            depTab: false,
            activeSwitch: 1,
            activeButton: 'right',
            discountCode: '',
            price: this.props.price,
            zarinpal: null,
            wallet: null,
            carts: null,
            modalVisible: false,
            invoice_id: null,
            gatways: null

        };
        this.onBackPress = this.onBackPress.bind(this);
        this.handleOpenURL = this.handleOpenURL.bind(this);1
    }
    setModalVisible() {
        this.setState({modalVisible: !this.state.modalVisible});
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
        Linking.removeEventListener('url', this.handleOpenURL);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        this.setState({loading: true});
            AsyncStorage.getItem('token').then((info) => {
                const newInfo = JSON.parse(info);
                Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
                console.log('comment token', newInfo.token);

                Axios.get('/gateways').then(response => {
                    console.log(response.data);
                    this.setState({loading: false });
                    let arr = [];
                    arr = response.data;
                    arr.map((item)=>{
                        const id = item.id;
                        if(item.slug === 'zarinpal')
                            this.setState({zarinpal: id })
                        else if(item.slug === 'wallet')
                            this.setState({wallet: id})
                        if(item.slug === 'carts')
                            this.setState({carts: id})
                    })
                })
                .catch(function (error) {
                        console.log(error.response);
                    }
                );
            });
    }
    handleOpenURL(event) {
        console.log(event.url);
        Actions.push('Receipt')
    }
    paymentMethod(){
        this.setState({loading: true});
        let typeArray = [];
        try {
            console.log('all of data', this.props.data);
            if(this.props.titleName === 'diet'){
                typeArray.push('diet');
            }
            else if(this.props.titleName === 'exercise'){
                typeArray.push('diet');
                typeArray.push('exercise');
            }
            else if(this.props.titleName === 'complementary'){
                typeArray.push('diet');
                typeArray.push('exercise');
                typeArray.push('complementary');
            }
            AsyncStorage.getItem('token').then((info) => {
                const newInfo = JSON.parse(info);
                Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
                console.log('comment token', newInfo.token);
                console.log(this.state.discountCode)
                Axios.post('/financial/create_invoice', {
                    data: JSON.stringify(this.props.data),
                    type: typeArray,
                    discountCode: this.state.discountCode
                }).then(response=> {
                    this.setState({loading: false});
                    console.log('payment info', response.data);
                    Linking.addEventListener('url', this.handleOpenURL);
                    if(this.state.activeButton === 'right'){
                        Linking.openURL('http://www.fitclub.ws/pay_invoice?invoice='+response.data+'&gateway='+this.state.zarinpal);
                    }
                    else if(this.state.activeButton === 'left'){
                        Linking.openURL('http://www.fitclub.ws/pay_invoice?invoice='+response.data+'&gateway='+this.state.wallet);
                    }
                    else{
                        this.setState({invoice_id: response.data, modalVisible: true, gatways: this.state.carts})
                    }
                })
                .catch((error) => {
                    alert('خطا');
                    this.setState({loading: false});
                });
            });
            console.log(Axios.defaults.headers.common['Authorization']);

        }
        catch (error) {
            console.log(error)
            alert('خطایی رخ داده مجددا تلاش نمایید');
            this.setState({loading: false});
        }
    }
    offMethod() {
        this.setState({loading: true});
        try {
            console.log('all of data', this.props.data);
            let typeArray = [];
            typeArray.push(this.props.titleName);
            AsyncStorage.getItem('token').then((info) => {
                const newInfo = JSON.parse(info);
                Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
                console.log('comment token', newInfo.token);

                Axios.get('/financial/prices?discountCode='+this.state.discountCode).then(response => {
                    console.log(response.data);
                    this.setState({loading: false });
                    if(response.data.message === 'ok'){
                        const typeP = this.props.titleName;
                        if(typeP === 'diet') {
                            this.setState({price: response.data.data.diet })
                        }
                        else if(typeP === 'exercise' ) {
                            this.setState({price: response.data.data.diet_exercise })
                        }
                        if(typeP === 'complementary') {
                            this.setState({price: response.data.data.diet_exercise_complementary })
                        }
                    }
                    else{
                        alert('کد نامعتبر است');
                    }

                })
                    .catch(function (error) {
                            console.log(error.response);
                        }
                    );
            });
        }
        catch (error) {
            console.log(error)
            this.setState({loading: false});
            alert('خطایی رخ داده مجددا تلاش نمایید');
        }
    }
    render() {
        let programType = '';
        if(this.props.titleName === 'diet'){
            programType = 'رژیم';
        }
        else if(this.props.titleName === 'exercise'){
            programType = 'رژیم و تمرین';
        }
        else if(this.props.titleName === 'complementary'){
            programType = 'رژیم و تمرین و مکمل';
        }
        if(this.state.loading)
            return (<Loader txtColor="red" color='red' />);
        else return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                        <View style={styles.iconContainer}>
                            <TouchableOpacity onPress={() => Actions.push('finalConfirm')}>
                                <FIcon name="arrow-left" size={27} color="white" style={{paddingRight: 20}} />
                            </TouchableOpacity>
                            <Icon name="bell-o" size={23} color="white" />
                        </View>
                        <Text style={styles.headerTitle}>پرداخت</Text>
                        <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                            <Icon name="bars" size={20} color="white" />
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.scrollContainer}>
                        <View style={styles.contentTop}>
                            <View style={styles.imageContainer}>
                                <Image source={logo} style={styles.image} />
                            </View>
                            <View style={styles.regimContainer}>
                                <View style={styles.right}>
                                    {/*<Text style={styles.value}>FC-4536998</Text>*/}
                                    {/*<Text style={styles.label}>کد برنامه: </Text>*/}
                                </View>
                                <Text style={styles.value}>برنامه لاغری</Text>
                            </View>
                            <View style={styles.regimContainer}>
                                <View style={styles.right}>
                                    <Text style={styles.value}>{programType}</Text>
                                    <Text style={styles.label}>نوع برنامه: </Text>
                                </View>
                                <View style={styles.left}>
                                    <Text style={styles.value}>دو ماه</Text>
                                    <Text style={styles.label}>طول برنامه: </Text>
                                </View>
                            </View>
                            <View style={styles.priceContainer}>
                                <Text style={styles.priceValue}>{this.state.price}</Text>
                                <Text style={styles.priceLabel}> هزینه قابل پرداخت: </Text>
                            </View>
                            <View style={styles.reductionContainer}>
                                <View style={styles.reduction}>
                                    <SwitchButton />
                                    <Text style={styles.redlabel}>کد تخفیف دارید؟</Text>
                                </View>
                                <TextInput
                                    placeholder="کد تخفیف را وارد کنید ..."
                                    placeholderTextColor={'gray'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.discountCode}
                                    style={{
                                        height: 36,
                                        backgroundColor: 'rgb(246, 246, 246)',
                                        paddingRight: 15,
                                        width: '90%',
                                        borderRadius: 20,
                                        borderWidth: 1,
                                        borderColor: 'lightgray',
                                        direction: 'rtl'
                                    }}
                                    onChangeText={(discountCode) => this.setState({discountCode})} />
                                <TouchableOpacity onPress={() => this.offMethod()} style={styles.redButton}>
                                    <View>
                                        <Text style={styles.buttonText}>تایید کد تخفیف</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.contentBottomContainer}>
                            <View style={styles.contentBottom}>
                                <View style={styles.paymentButtonContainer}>
                                    <TouchableOpacity onPress={() => this.setState({activeButton: 'center'})} style={[styles.paymentButton, {backgroundColor: this.state.activeButton === 'center' ? 'red' : 'white' }]}>
                                        <View>
                                            <Text style={[styles.paymentText, {color: this.state.activeButton === 'center' ? 'white' : 'red', fontFamily: this.state.activeButton === 'center' ? 'IRANSansMobile(FaNum)' : 'IRANSansMobile(FaNum)' }]}>کارت به کارت</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.setState({activeButton: 'left'})} style={[styles.paymentButton, {backgroundColor: this.state.activeButton === 'left' ? 'red' : 'white' }]}>
                                        <View>
                                            <Text style={[styles.paymentText, {color: this.state.activeButton === 'left' ? 'white' : 'red', fontFamily: this.state.activeButton === 'left' ? 'IRANSansMobile(FaNum)' : 'IRANSansMobile(FaNum)' }]}>پرداخت از کیف پول</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.setState({activeButton: 'right'})} style={[styles.paymentButton, {backgroundColor: this.state.activeButton === 'right' ? 'red' : 'white' }]}>
                                        <View>
                                            <Text style={[styles.paymentText, {color: this.state.activeButton === 'right' ? 'white' : 'red', fontFamily: this.state.activeButton === 'right' ? 'IRANSansMobile(FaNum)' : 'IRANSansMobile(FaNum)' }]}>پرداخت آنلاین</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                {/*<Text style={styles.portText}>انتخاب درگاه</Text>*/}
                                {/*<PaymentOptions />*/}
                            </View>
                            <View style={styles.paymentButtonContainer}>
                                <TouchableOpacity onPress={() => this.paymentMethod()} style={styles.paymentBtn}>
                                    <View>
                                        <Text style={styles.buttonText}>پرداخت</Text>
                                    </View>
                                </TouchableOpacity>
                                <InputModal
                                    gatways={this.state.gatways}
                                    invoice_id={this.state.invoice_id}
                                    modalVisible={this.state.modalVisible}
                                    onChange={(visible) => this.setModalVisible(visible)}
                                />
                            </View>
                        </View>

                    </View>
                </ScrollView>
                <View style={styles.footerContainer}>
                    <FooterMenu />
                </View>
            </View>
        );
    }
}
export default Payment;