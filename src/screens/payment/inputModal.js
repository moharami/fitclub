import React, { Component } from 'react';
import {
    Modal,
    Text,
    View,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    AsyncStorage,
    Linking
}
    from 'react-native'

import Axios from 'axios';
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://fitclub.ws';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'

class InputModal extends Component {
    state = {
        modalVisible: false,
        text: ''
    }
    handleSearch(){
        try {
            AsyncStorage.getItem('token').then((info) => {
                const newInfo = JSON.parse(info);
                Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
                Axios.get('/pay_invoice?invoice='+this.props.invoice_id+'&trace='+this.state.text+'&gateway='+this.props.gatways).then(response => {
                    this.setState({loading: false, modalVisible: false});
                    // if(response.)
                    console.log('payment carts', response.data);
                })
                .catch((error) => {
                    this.setState({loading: false});
                    console.log(error);
                    alert('خطا');
                });
            });
        }
        catch (error) {
            console.log(error)
            this.setState({loading: false});
            alert('خطایی رخ داده مجددا تلاش نمایید');
        }
    }
    render() {
        if(this.state.loading)
            return (<Loader txtColor="red" color='red' />);
        else return (
            <View style = {styles.container}>
                <Modal animationType = {"fade"} transparent = {true}
                       visible = {this.props.modalVisible}
                       onRequestClose = {() => this.props.onChange(false)}
                       onBackdropPress= {() => this.props.onChange(!this.state.modalVisible)}
                >
                    <View style = {styles.modal}>
                        <View style={styles.box}>
                            <TextInput
                                placeholder="شماره پیگیری"
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.text}
                                style={{
                                    height: 36,
                                    backgroundColor: 'rgb(246, 246, 246)',
                                    paddingRight: 15,
                                    width: '90%',
                                    borderWidth: 1,
                                    borderColor: 'lightgray',
                                    direction: 'rtl'
                                }}
                                onChangeText={(text) => this.setState({text})}
                            />
                            <TouchableOpacity style={styles.searchButton} onPress={()=>this.handleSearch()}>
                                <Text style={styles.searchText}>ثبت</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}
export default InputModal

const styles = StyleSheet.create ({
    container: {
        alignItems: 'center',
        backgroundColor: 'white',
        padding: 100,
        position: 'absolute',
        bottom: -200
    },
    modal: {
        flexGrow: 1,
        justifyContent: 'center',
        // flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,.6)',
        paddingRight: 30,
        paddingLeft: 30,

    },
    text: {
        color: '#3f2949',
        marginTop: 10
    },
    box: {
        width: '100%',
        height: 120,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    input: {
        width: '100%',
        fontSize: 16,
        paddingTop: 0,
        textAlign: 'right',
    },
    searchButton: {
        marginTop: 10,
        width: '30%',
        borderRadius: 7,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(20, 122, 170)'
    },
    searchText:{
        color: 'white'
    },
    picker: {
        height:50,
        width: "100%",
        alignSelf: 'flex-end'
    }
})