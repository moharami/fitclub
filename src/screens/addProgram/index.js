import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, AsyncStorage, BackHandler, Alert} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/Feather';
import FIcon from 'react-native-vector-icons/dist/FontAwesome';
import MIcon from 'react-native-vector-icons/dist/MaterialIcons';
import LinearGradient from 'react-native-linear-gradient'
import FooterMenu from '../../components/footerMenu'
import AddProgramType from "../../components/addProgramType/index";
import Axios from 'axios'
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://fitclub.ws';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import price1 from '../../assets/prices/price1.png'
import price2 from '../../assets/prices/price2.png'
import price3 from '../../assets/prices/price3.png'
import {Actions} from 'react-native-router-flux'
import {store} from '../../config/store';
import {connect} from 'react-redux';

class AddProgram extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: true,
            prices: {}
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        if(this.props.prices.data){
            this.setState({loading: false});
        }
        else{
            try {
                this.setState({loading: true});
                AsyncStorage.getItem('token').then((info) => {
                    const newInfo = JSON.parse(info);
                    Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
                    console.log('comment token', newInfo.token);
                    Axios.get('/financial/prices').then(response => {
                        store.dispatch({type: 'PRICES_INFO_FETCHED', payload: response.data});
                        this.setState({ loading: false, prices: response.data.data });
                    })
                        .catch(function (error) {
                            console.log(error.response);
                        });
                });
            }
            catch (error) {
                console.log(error)
                Alert.alert('خطایی رخ داده مجددا تلاش نمایید');
                this.setState({loading: false});
            }
        }
    }
    render() {
        const {prices} = this.props;
        if(this.state.loading){
            return (<Loader txtColor="red" color='red' />)
        }
        else return(
                <View style={styles.container}>
                    <View style={styles.header}>
                        <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}}
                                        colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                            <TouchableOpacity onPress={() => Actions.push('UserPrograms')} style={styles.iconContainer}>
                                <Icon name="arrow-left" size={25} color="white"/>
                            </TouchableOpacity>
                            <View style={styles.headerTitleContainer}>
                                <Text style={styles.headerTitle}>انتخاب برنامه</Text>
                            </View>
                        </LinearGradient>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <AddProgramType title="رژیم" titleName="diet" subLabel1="رژیم غذایی" sublabel2="" price={this.state.prices.diet} imgSrc={price1} />
                            <AddProgramType title="رژیم و تمرین" titleName="exercise" subLabel1="برنامه تمرینی" sublabel2="آموزش ویدئویی حرکات" price={this.state.prices.diet_exercise} imgSrc={price2}/>
                            <AddProgramType title="رژیم و تمرین و مکمل" titleName="complementary" subLabel1="رژیم غذایی + برنامه تمرینی" sublabel2="آموزش ویدئویی حرکات" price={this.state.prices.diet_exercise_complementary} imgSrc={price3}/>
                        </View>
                    </ScrollView>
                    <View style={styles.footerContainer}>
                        <FooterMenu />
                    </View>
                </View>

        );
    }
}
function mapStateToProps(state) {
    return {
        prices: state.auth.prices,

    }
}
export default connect(mapStateToProps)(AddProgram);
