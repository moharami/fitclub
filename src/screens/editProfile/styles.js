
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({

    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(251, 251, 251)',
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999
    },
    top: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'rgb(252, 86, 86)',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        paddingRight: 15,
        paddingLeft: 15
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    headerTitle: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 18,
        paddingRight: 50,
        paddingLeft: '12%'
    },
    scroll: {
        paddingTop: 50,
        flex: 1
        // paddingBottom: 200,
    },
    scrollContainer: {
        // paddingTop: 10,
        paddingBottom: 100,
    },
    contentTop: {
        // alignItems: 'center',
        // justifyContent: 'center',
        paddingBottom: 10,
    },
    label: {
       fontSize: 14, color: 'rgb(107, 107, 107)',
        paddingBottom: 15,
        paddingTop: 20
    },
    contentContainer: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        // paddingBottom: 250,
        zIndex: 0,
    },
    userTopContainer: {
        flexDirection: 'row',
        width: '100%',
        paddingBottom: 30,
        paddingTop: 20,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: 'rgb(252, 86, 86)'
    },
    imageContainer: {
        position: 'relative',
        width: 140,
        height: 150,
        zIndex: 0,
        marginRight: 20,
        marginLeft: 15
    },
    layer: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: 140,
        height: 150,
        zIndex: 9999
    },
    image: {
        position: 'absolute',
        top: 0,
        left: 0,
        // width: 120,
        // height: 130,
        width: 140,
        height: 150,
        zIndex: 1
    },
    updateBtn: {
        width: '56%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgb(32, 193, 188)',
        padding: 15,
        borderRadius: 30,
        position: 'absolute',
        bottom: 0,
        left: '22%',
        zIndex: 9999
    },
    footerContainer: {
        position: 'absolute',
        flex: 1,
        bottom: 0,
        right: 0,
        left: 0
    },
    historyText: {
        color: 'black',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    contentBottom: {
        padding: 15
    },
    redButtonContainer: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    redButton: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgb(277, 5, 26)',
        padding: 12,
        marginTop: 30
    },
    buttonText: {
        fontSize: 15,
        color: 'white',
        fontWeight: 'bold',
        fontFamily: 'IRANSansMobile(FaNum)',
    },

});
