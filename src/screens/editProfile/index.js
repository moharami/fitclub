import React, {Component} from 'react';
import {ScrollView, View, TouchableOpacity, Text, Image, AsyncStorage, BackHandler, Alert} from 'react-native';
import styles from './styles'
import LinearGradient from 'react-native-linear-gradient'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {Actions} from 'react-native-router-flux'
import FooterMenu from '../../components/footerMenu'
import layer from '../../assets/layer.png'
import EditItem from '../../components/editItem'
import ImagePicker from 'react-native-image-picker';
import {connect} from 'react-redux';
import Axios from 'axios';
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://fitclub.ws';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import moment from 'moment'
import PersianCalendarPicker from'react-native-persian-calendar-picker';

class EditProfile extends Component {
    constructor(props){
        super(props);
        this.state = {
            posts: [],
            loading: false,
            active: '',
            friTab: true,
            depTab: false,
            activeSwitch: 1,
            text: '',
            activeButton: 'center',
            modalVisible: false,
            imgSrc: '',
            fname: '',
            lname: '',
            sex: '',
            birthday: '',
            email: '',
            showPicker: false,
            selectedStartDate: null
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    onDateChange(date) {
        setTimeout(() => {this.setState({showPicker: false})}, 500)
        this.setState({ selectedStartDate: date });

    }
    selectPhotoTapped() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    imgSrc: response.uri
                })
            }
        });
    }
    checkTextInput(fill, num){
        switch (num) {
            case 1:
                this.setState({fname: fill});
                break;
            case 2:
                this.setState({lname: fill});
                break;
            case 3:
                this.setState({sex: fill});
                break;
            case 4:
                this.setState({birthday: fill});
                break;
            case 5:
                this.setState({email: fill});
                break;
            default:
                break;
        }
    }
    updateInfo() {
        this.setState({loading: true});
        AsyncStorage.getItem('token').then((info) => {
            const newInfo = JSON.parse(info);
            Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
            Axios.defaults.headers.common['Content-Type'] = 'multipart/form-data' ;
            let formdata = new FormData();

            // formdata.append("photo", this.state.imgSrc)
            formdata.append("fname", this.state.fname === '' ? this.props.user.fname : this.state.fname)
            formdata.append("lname", this.state.lname === '' ? this.props.user.lname : this.state.lname)
            formdata.append("email", this.state.email === '' ? this.props.user.email : this.state.email)
            formdata.append("sex", this.state.sex === '' ? this.props.user.sex : this.state.sex)
            formdata.append("birthday", this.state.birthday === '' ? this.props.user.birthday : moment(this.state.selectedStartDate).format('Y-M-D'))
            if(this.state.imgSrc !== '') {
                formdata.append("type", "image");
                formdata.append("file", {
                    uri: this.state.imgSrc,
                    type: 'image/jpeg',
                    name: 'file'
                });
            }
            Axios.post('/core/update', formdata
            ).then(response=> {
                if(response.data.message === 'ok'){
                    this.setState({loading: false});
                    console.log('edited item', response.data);
                    Alert.alert('','پروفایل با موفقیت تغییر یافت');
                }
            })
            .catch((error) => {
                console.log(error)
                Alert.alert('','خطا');
                this.setState({loading: false});

            });
        });
        console.log(Axios.defaults.headers.common['Authorization']);

    }
    openPicker() {
        this.setState({ showPicker: true });
    }
    logoutMethod() {
        AsyncStorage.removeItem('token');
        Actions.push('Info')

    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        const {user} = this.props;
        if(this.state.loading)
            return (<Loader txtColor="red" color='red' />);
        else
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.top}>
                        <TouchableOpacity onPress={() => Actions.push('UserProfile')} style={styles.iconContainer}>
                            <FIcon name="arrow-left" size={25} color="white" />
                        </TouchableOpacity>
                        <Text style={styles.headerTitle}>تغییر اطلاعات</Text>
                        <TouchableOpacity onPress={() => this.updateInfo()}>
                            <FIcon name="check" size={20} color="white" />
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView style={styles.scroll} >
                    <View style={styles.scrollContainer}>
                        <View style={styles.contentTop}>
                            <View style={styles.contentContainer}>
                                <LinearGradient style={styles.userTopContainer} colors={['rgb(255, 88, 88)', 'rgb(247, 78, 79)']}>
                                    <TouchableOpacity onPress={() => this.selectPhotoTapped()} >
                                        {
                                            this.state.imgSrc !== '' ?
                                                <View style={styles.imageContainer}>
                                                    <Image style={styles.layer} source={layer} />
                                                    <Image style={styles.image} source={{uri: this.state.imgSrc}} />
                                                </View>
                                                :
                                                <View style={styles.imageContainer}>
                                                        <View style={{
                                                        backgroundColor:'rgba(0,0,0,.6)',
                                                        alignItems: "center",
                                                        justifyContent: "center",
                                                        position: 'absolute',
                                                        top: 0,
                                                        left: 0,
                                                        width: 140,
                                                        height: 150,
                                                        zIndex: 9999
                                                    }}>
                                                        <Icon name="camera" size={25} color="white" />
                                                    </View>
                                                    <Image style={styles.layer} source={layer} />
                                                    {
                                                        this.props.user.attachments ? <Image source={{uri: "http://fitclub.ws/files?uid="+this.props.user.attachments.uid+"&width=104&height=92" }} style={styles.image} />
                                                            :
                                                            <Image style={styles.image} source={{uri: 'http://simpleicon.com/wp-content/uploads/user-5.png'}} />                                                    }
                                                </View>
                                        }
                                    </TouchableOpacity>
                                </LinearGradient>
                            </View>
                        </View>
                        <View style={styles.contentBottom}>
                            <Text style={styles.label}>عمومی</Text>
                            <EditItem label="نام" border check={(fill) => this.checkTextInput(fill, 1)} value={this.props.user.fname} />
                            <EditItem label="نام خانوادگی" border check={(fill) => this.checkTextInput(fill, 2)} value={this.props.user.lname} />
                            <EditItem label="جنسیت" border check={(fill) => this.checkTextInput(fill, 3)} value={this.props.user.sex} />
                            <EditItem label="تاریخ تولد" check={(fill) => this.checkTextInput(fill, 4)} value={this.state.selectedStartDate ? moment(this.state.selectedStartDate).format('Y-M-D') : this.props.user.birthday} birthday openPicker={() => this.openPicker()} date={this.state.selectedStartDate}/>
                            <Text style={styles.label}>امنیتی</Text>
                            <EditItem label="ایمیل" border check={(fill) => this.checkTextInput(fill, 5)} value={this.props.user.email} />
                            <View style={styles.redButtonContainer}>
                                <TouchableOpacity onPress={() => this.logoutMethod()} style={styles.redButton}>
                                    <View>
                                        <Text style={styles.buttonText}>خروج از حساب کاربری</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            {
                                this.state.showPicker ?
                                    <View style={{
                                        position: 'absolute',
                                        bottom: '30%',
                                        zIndex: 9999,
                                        backgroundColor: 'white'
                                    }}>
                                        <PersianCalendarPicker
                                            onDateChange={(date) => this.onDateChange(date)}
                                        />
                                    </View>
                                    : null
                            }
                        </View>
                    </View>
                </ScrollView>
                <View style={styles.footerContainer}>
                    <FooterMenu />
                </View>
            </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        user: state.auth.user
    }
}
export default connect(mapStateToProps)(EditProfile);