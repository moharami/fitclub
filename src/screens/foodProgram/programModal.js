import React, { Component } from 'react';
import {
    Modal,
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    ScrollView
}
    from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import Icon from 'react-native-vector-icons/dist/Feather';
import MIcon from 'react-native-vector-icons/dist/MaterialIcons';
import {Actions} from 'react-native-router-flux'

class ProgramModal extends Component {
    state = {
        modalVisible: false,
        value: '',
        activeProgram: 'left'
    };
    handleTouch(direction) {
        if(direction === 'right') {
            this.setState({activeProgram: direction});
            Actions.push('program')
        }
        else {
            this.setState({activeProgram: direction});
            this.props.closeModal();
        }
    }
    render() {
        return (
            <View style = {styles.container}>
                <Modal animationType = {"fade"} transparent = {true}
                       visible = {this.props.modalVisible}
                       onRequestClose = {() => this.props.onChange(false)}
                       onBackdropPress= {() => this.props.onChange(!this.state.modalVisible)}
                >
                    <View style = {styles.modal} >
                        <View style={styles.box}>
                            <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                                <View style={styles.iconContainer}>
                                    <Icon name="arrow-left" size={25} color="white" />
                                </View>
                                <View style={styles.headerTitleContainer}>
                                    <TouchableOpacity onPress={() => this.props.closeModal()}>
                                        <MIcon name="keyboard-arrow-up" size={22} color="white" />
                                    </TouchableOpacity>
                                    <Text style={styles.headerTitle}>برنامه شماره 1</Text>
                                </View>
                            </LinearGradient>
                            <View style={styles.programTypeContainer}>
                                <TouchableOpacity onPress={()=> this.handleTouch('left')}>
                                    <View style={[styles.programType, {marginRight: 10, backgroundColor: this.state.activeProgram === 'left' ? 'rgb(227, 5, 26)': 'white'}]}>
                                        <Text style={[styles.Title, {color: this.state.activeProgram === 'left' ? 'white': 'black'}]}>برنامه غذایی</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={()=> this.handleTouch('right')}>
                                    <View style={[styles.programType, {marginLeft: 10,  backgroundColor: this.state.activeProgram === 'right' ? 'rgb(227, 5, 26)': 'white'}]}>
                                        <Text style={[styles.Title, {color: this.state.activeProgram === 'right' ? 'white': 'black'}]}>برنامه تمرینی</Text>
                                    </View>
                                </TouchableOpacity>

                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}
export default ProgramModal

const styles = StyleSheet.create ({
    container: {
        alignItems: 'center',
        backgroundColor: 'white',
        padding: 100,
        position: 'absolute',
        bottom: -200
    },
    modal: {
        flexGrow: 1,
        justifyContent: 'flex-start',
        // flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,.5)',
    },
    box: {
        width: '100%',
        // height: 120,
        // flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        fontSize: 15,
        color: 'white',
        fontWeight: 'bold',
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    top: {
        // flex: 1,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        height: 60,
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
        paddingRight: 15,
        paddingLeft: 15
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    headerTitleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerTitle: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 16,
        paddingRight: '35%'
    },
    programTypeContainer: {
        paddingTop: 30,
        paddingBottom: 30,
        paddingRight: 15,
        paddingLeft: 15,
        flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
    programType: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        paddingBottom: 3,
        paddingTop: 3,
        borderWidth: 1,
        borderColor: 'lightgray',
        borderRadius: 15,
    },
    Title: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
    }
});