import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Linking, BackHandler} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/Feather';
import FIcon from 'react-native-vector-icons/dist/FontAwesome';
import MIcon from 'react-native-vector-icons/dist/MaterialIcons';
import LinearGradient from 'react-native-linear-gradient'
import FooterMenu from '../../components/footerMenu'
import FoodItem from '../../components/foodItem'
// import ProgramModal from './programModal'
import {Actions} from 'react-native-router-flux'

class FoodProgram extends Component {
    constructor(props){
        super(props);
        this.state = {
            activeText: 1,
            activeSwitch: 1,
            modalVisible: false
        };
        this.onBackPress = this.onBackPress.bind(this);

    }
    // setModalVisible() {
    //     this.setState({modalVisible: !this.state.modalVisible});
    // }
    // closeModal() {
    //     this.setState({modalVisible: false});
    //
    // }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    openPdf() {
        Linking.openURL('http://www.fitclub.ws.info/panel/'+this.props.pName+'Single/'+this.props.item.id+'/print/pdf');
    }
    render() {
        // const info = JSON.parse(this.props.item.days[0].data);
        // console.log(info)
        // let foodData = null;
        // const myInfo = JSON.parse(this.props.item.days[this.state.activeText-1].data);
        // for(const k in myInfo){
        //     foodData = myInfo[k].map((item)=> <FoodItem key={item.id} item={item} type={this.props.item.type} /> )
        // }


        let foodData = null;
        const days = this.props.item.days;
        console.log('daysss', days)
        for(const k in days) {
            const myInfo = JSON.parse(days[k].data);
            let ff=[];
            ff= myInfo;
            console.log('daysss json', myInfo)

            foodData = ff.map((item)=> <FoodItem key={item.end} item={item} type={this.props.item.type} /> )
        }
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                        <TouchableOpacity onPress={() => Actions.push('UserPrograms')} style={styles.iconContainer}>
                            <Icon name="arrow-left" size={25} color="white" />
                        </TouchableOpacity>
                        <View style={styles.headerTitleContainer}>
                            <TouchableOpacity onPress={() => this.setState({modalVisible: !this.state.modalVisible })}>
                                <MIcon name="keyboard-arrow-down" size={22} color="white" />
                            </TouchableOpacity>
                            <Text style={styles.headerTitle}>برنامه شماره 1</Text>
                        </View>
                    </LinearGradient>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <View style={styles.headerContainer}>
                            <TouchableOpacity onPress={() => this.openPdf()} style={styles.headerIconContainer}>
                                <FIcon name="file-pdf-o" size={20} color="white" />
                            </TouchableOpacity>
                            <Text style={styles.label}>برنامه غذایی</Text>
                        </View>
                        {/*{*/}
                            {/*info.map((item) => <FoodItem key={item.id} item={item} type={this.props.item.type} />*/}
                            {/*)*/}
                        {/*}*/}
                        {
                            foodData
                        }
                        {/*<ProgramModal*/}
                            {/*closeModal={() => this.closeModal()}*/}
                            {/*modalVisible={this.state.modalVisible}*/}
                            {/*onChange={(visible) => this.setModalVisible(visible)}*/}
                        {/*/>*/}
                    </View>
                </ScrollView>
                <View style={styles.footerContainer}>
                    <FooterMenu />
                </View>
            </View>
        );
    }
}
export default FoodProgram;
