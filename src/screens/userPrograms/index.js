import React, {Component} from 'react';
import {ScrollView, View, TouchableOpacity, Image, Text, AsyncStorage, BackHandler} from 'react-native';
import styles from './styles'
import LinearGradient from 'react-native-linear-gradient'
import Icon from 'react-native-vector-icons/Feather';
import {Actions} from 'react-native-router-flux'
import FooterMenu from '../../components/footerMenu'
import ProgramType from '../../components/programType'
import Axios from 'axios'
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://fitclub.ws';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import {store} from '../../config/store';
import {connect} from 'react-redux';
class UserPrograms extends Component {
    constructor(props){
        super(props);
        this.state = {
            data: [],
            loading: false,
            text: '',
            textiNFO: null
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        if(Object.keys(this.props.programs).length !== 0) {
            console.log('program fech from cash')
        }
        else{
            AsyncStorage.getItem('token').then((info) => {
                const newInfo = JSON.parse(info);
                Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
                console.log('comment token', newInfo.token);
                Axios.get('/training/programs/show').then(response => {
                    const data = response.data;
                    store.dispatch({type: 'NEW_PROGRAMS_FETCHED', payload: response.data});
                    let textiNFO1 = null;
                    if(!this.props.programs.complementary && !this.props.programs.diet && !this.props.programs.exercise){
                        textiNFO1 = <Text style={{color: 'red', textAlign: 'center', padding: 25}}>موردی برای نمایش وجود ندارد</Text>
                    }
                    console.log('question response',response.data);
                    this.setState({data: data, loading: false, textiNFO: textiNFO1})
                })
                    .catch((error) =>{
                            console.log(error.response);
                        }
                    );
            });
        }
    }
    render() {
        const {programs} = this.props;
        if(this.state.loading)
            return (<Loader txtColor="red" color='red' />);
        else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                            <View style={styles.iconContainer}>
                                <TouchableOpacity onPress={() => Actions.push('AddProgram')}>
                                    <Icon name="plus" size={30} color="white" style={{paddingRight: 20}} />
                                </TouchableOpacity>
                                <Icon name="bell" size={23} color="white" />
                            </View>
                            <Text style={styles.headerTitle}>برنامه های من</Text>
                            <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                                <Icon name="menu" size={25} color="white" />
                            </TouchableOpacity>
                        </LinearGradient>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.scrollContainer}>
                            <View style={styles.innerContainer}>

                                <View style={styles.btnContainer}>
                                    <TouchableOpacity style={styles.addItem} onPress={() => Actions.push('AddProgram')}>
                                        <Text style={styles.buttonText}>افزودن</Text>
                                    </TouchableOpacity>
                                </View>
                                {this.state.textiNFO !== null ? this.state.textiNFO :
                                    <View>
                                        {
                                            this.props.programs.complementary ?
                                                this.props.programs.complementary.map((item) => <View style={styles.programBottomContainer} key={item.id}>
                                                    <ProgramType item={item} pName="complementary"/>
                                                </View>)
                                                : null
                                        }
                                        {  this.props.programs.diet ?
                                            this.props.programs.diet.map((item) => <View style={styles.programBottomContainer} key={item.id}>
                                                <ProgramType item={item} pName="diet"/>
                                            </View>)
                                            : null
                                        }
                                        { this.props.programs.exercise ?
                                            this.props.programs.exercise.map((item) => <View style={styles.programBottomContainer} key={item.id}>
                                                <ProgramType item={item} pName="exercise" />
                                            </View>)
                                            : null
                                        }
                                    </View>


                                }

                            </View>
                            {/*<View style={styles.programTopContainer}>*/}
                            {/*<ProgramType type="برنامه لاغری" statusText="در حال پرداخت" statusTextColor="red" iconTop/>*/}
                            {/*</View>*/}
                            {/*</View>*/}
                            {/*<View style={styles.programBottomContainer}>*/}
                            {/*<ProgramType type="برنامه حجم دهی" waiting statusText="منقضی" statusTextColor="gray" />*/}
                            {/*</View>*/}
                            {/*<View style={styles.programBottomContainer}>*/}
                            {/*<ProgramType type="برنامه حجم دهی" waiting statusText="منقضی" statusTextColor="red" />*/}
                            {/*</View>*/}
                        </View>
                    </ScrollView>
                    <View style={styles.footerContainer}>
                        <FooterMenu active="program"/>
                    </View>
                </View>
            );
    }
}
function mapStateToProps(state) {
    return {
        programs: state.posts.programs,
    }
}
export default connect(mapStateToProps)(UserPrograms);