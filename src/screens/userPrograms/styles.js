
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({

    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(251, 251, 251)',
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999
    },
    top: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
        paddingRight: 15,
        paddingLeft: 15
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    headerTitle: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 18,
        paddingRight: 50
    },
    searchContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 60,
        marginBottom: 20
    },
    scroll: {
        paddingTop: 80,
        paddingRight: 13,
        paddingLeft: 13,
        paddingBottom: 200,
        backgroundColor:'white'
    },
    scrollContainer: {
        paddingBottom: 230,
    },
    innerContainer: {
        // borderColor: 'rgb(237, 237, 237)',
        // borderWidth: 1,
        backgroundColor: 'white',
        marginBottom: 10,
    },
    programTopContainer: {
        margin: 10,
        // borderBottomColor: 'rgb(237, 237, 237)',
        // borderBottomWidth: 1,
        backgroundColor: 'white',

        paddingBottom: 20
    },
    programBottomContainer: {
        margin: 10,
        borderColor: 'rgb(237, 237, 237)',
        borderWidth: 1,
        paddingBottom: 20,
        marginBottom: 30,
        backgroundColor: 'white',
        padding: 10
    },
    statusContainer: {
        margin: 10,
        borderBottomColor: 'rgb(237, 237, 237)',
        borderBottomWidth: 1,
        paddingBottom: 20
    },
    infoContainer: {
        margin: 10,
        borderBottomColor: 'rgb(237, 237, 237)',
        borderBottomWidth: 1,
        paddingBottom: 20,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    statusText: {
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 18,
        // paddingRight: 10,
        paddingBottom: 10,
        alignSelf: 'flex-end'
    },
    regimContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 8,
    },
    left: {
        flexDirection: 'row',
        paddingLeft: 30
    },
    right: {
        flexDirection: 'row'
    },
    label: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14
    },
    value: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black',
        fontSize: 14
    },
    redValue: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'red',
        fontSize: 15
    },
    recordedContainer: {
        margin: 10,
        paddingBottom: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    slideContainers: {
        flexDirection: 'row',
        transform: [
            {rotateY: '180deg'},
        ]
    },
    payButton: {
        width: '60%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgb(32, 193, 189)',
        padding: 13,
        borderRadius: 25,
        // paddingRight: 80,
        // paddingLeft: 80,
        marginTop: 30
    },
    priceLabel: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14
    },
    priceValue: {
        color: 'rgb(32, 191, 197)',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 25,
        paddingRight: 10
    },
    priceContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%'
    },
    reductionContainer: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    },
    reduction: {
        flexDirection: 'row',
        width: '90%',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderTopColor: 'lightgray',
        borderTopWidth: 1,
        paddingTop: 15,
        paddingBottom: 15
    },
    redButton: {
        width: '92%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgb(92, 158, 255)',
        padding: 12,
        marginTop: 15
    },
    buttonText: {
        fontSize: 15,
        color: 'white',
        fontWeight: 'bold',
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    redlabel: {
        color: 'red',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 16

    },
    buttonTitle: {
        fontSize: 15,
        color: 'lightgray'
    },
    footerContainer: {
        position: 'absolute',
        flex: 1,
        bottom: 0,
        right: 0,
        left: 0
    },
    contentBottomContainer: {
        position: 'relative',
        paddingBottom: 50,
        zIndex: 0
    },
    contentBottom: {
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 15,
        marginTop: 25,
    },
    paymentButtonContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paymentButton: {
        flex: .5,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 9,
        // backgroundColor: 'red',
        borderColor: 'red',
        borderWidth: 1
    },
    paymentText: {
        fontSize: 14
    },
    portText: {
        alignSelf: 'flex-end',
        color: 'black',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingTop: 20

    },
    paymentBtn: {
        width: '56%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgb(32, 193, 188)',
        padding: 12,
        borderRadius: 30,
        position: 'absolute',
        bottom: 30,
        left: '22%',
        zIndex: 9999

    },
    addItem: {
        width: '40%',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 8,
        paddingBottom: 8,
        paddingRight: 8,
        paddingLeft: 8,
        borderRadius: 5,
        backgroundColor: 'rgb(89, 149, 97)'
    },
    btnContainer: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 30
    }
});
