import React, {Component} from 'react';
import { View, TouchableOpacity, Text, TextInput,KeyboardAvoidingView, Image, BackHandler, Alert,Platform} from 'react-native';
import logo from '../../assets/loginLogo.png'
import styles from './styles'
import {Actions} from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import Axios from 'axios'
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://fitclub.ws';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'

class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            login: true,
            loading: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    onLogin(){
        if(this.state.text === '')
            Alert.alert('', 'لطفا شماره موبایل خود را وارد کنید');
        else{
            this.setState({loading: true});
            Axios.post('/check', {
                mobile: this.state.text
            }).then(response=> {
                this.setState({loading: false});
                console.log('info', response.data);
                if(response.data.data === false){
                    Actions.profile({mobile: this.state.text});
                }
                else {
                    Actions.confirmation({mobile: this.state.text});
                }
            })
                .catch((error) => {
                    Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        this.setState({loading: false});

                    }
                );
        }
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        if(Platform.OS==='ios'){
            return(
                    <LinearGradient colors={['rgb(253, 87, 87)','rgb(229, 57, 57)', 'rgb(223, 50, 52)' ]} style={{flex:1}}>
                        <KeyboardAvoidingView behavior="padding" style={styles.container}>
                        {
                            this.state.loading ? <Loader send={false} /> :
                                <View style={styles.send}>
                                    <Image source={logo} style={styles.image} />
                                    <View style={styles.body}>
                                        <Text style={styles.label}>شماره موبایل خود را وارد کنید تا کد فعال سازی از طریق پیامک برای شما ارسال شود.</Text>
                                        <TextInput
                                            placeholder="شماره تلفن"
                                            keyboardType = 'numeric'
                                            placeholderTextColor={'white'}
                                            underlineColorAndroid='transparent'
                                            value={this.state.text}
                                            style={{
                                                height: 55,
                                                backgroundColor: 'rgb(205, 49, 52)',
                                                paddingRight: 15,
                                                width: '70%',
                                                fontSize: 18,
                                                color: 'white',
                                                textAlign:'right'
                                            }}
                                            onLayout={this.onLayout}
                                            onChangeText={(text) => this.setState({text})} />
                                        <TouchableOpacity onPress={() => this.onLogin()} style={styles.advertise}>
                                            <View>
                                                <Text style={styles.buttonTitle}>ارسال کد تایید</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                        }
                        </KeyboardAvoidingView>
                    </LinearGradient>
            )
        }
        return (
            <LinearGradient colors={['rgb(253, 87, 87)','rgb(229, 57, 57)', 'rgb(223, 50, 52)' ]} style={styles.container}>
                {
                    this.state.loading ? <Loader send={false} /> :
                        <View style={styles.send}>
                            <Image source={logo} style={styles.image} />
                            <View style={styles.body}>
                                <Text style={styles.label}>شماره موبایل خود را وارد کنید تا کد فعال سازی از طریق پیامک برای شما ارسال شود.</Text>
                                <TextInput
                                    placeholder="شماره تلفن"
                                    keyboardType = 'numeric'
                                    placeholderTextColor={'white'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.text}
                                    style={{
                                        height: 55,
                                        backgroundColor: 'rgb(205, 49, 52)',
                                        paddingRight: 15,
                                        width: '70%',
                                        fontSize: 18,
                                        color: 'white',
                                        textAlign:'right'
                                    }}
                                    onLayout={this.onLayout}
                                    onChangeText={(text) => this.setState({text})} />
                                <TouchableOpacity onPress={() => this.onLogin()} style={styles.advertise}>
                                    <View>
                                        <Text style={styles.buttonTitle}>ارسال کد تایید</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                }
            </LinearGradient>
        );
    }
}
export default Login;