import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        // paddingTop: 100

    },
    imageContainer: {
        position: 'relative',
        alignItems: 'center',
        justifyContent: 'center',
        top: 60,
        right: 0,
        left: 0
    },
    image: {
        // width: '100%',
        // position: 'absolute',
        // // top: 60,
        // height: '100%',
        // alignItems: 'center',
        // justifyContent: 'center',
        // top: 80,
        // zIndex: 9999,

    },
    body: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 100
    },
    label: {
        paddingBottom: 10,
        color: 'rgba(255, 255, 255, .8)',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingRight: 60,
        paddingLeft: 60,
        paddingTop: '23%'
    },
    advertise: {
        width: '70%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        padding: 10,
        marginTop: 10

    },
    buttonTitle: {
        fontSize: 15,
        color: 'red',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    send: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    }
});
