
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({

    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(251, 251, 251)',
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999
    },
    top: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
        paddingRight: 15,
        paddingLeft: 15
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    headerTitle: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 18,
        // paddingRight: 50
    },
    scroll: {
        paddingTop: 80,
        paddingRight: 15,
        paddingLeft: 15,
        paddingBottom: 200,
        marginBottom: 40

    },
    scrollContainer: {
        backgroundColor: 'white',
        paddingTop: 10,
        paddingBottom: 200,
        // shadowOffset:{  width: 10,  height: 10,  },
        // shadowColor: 'black',
        // shadowOpacity: .5,
        elevation: 4,
        // shadowRadius: 20
        padding: 10
    },
    contentHeader: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 50,
        paddingBottom: 50,
    },
    buttonTitle: {
        fontSize: 15,
        color: 'lightgray'
    },
    contentHeaderTitle: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 18,
    },
    userNameContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    userName: {
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 16,
    },
    dearText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'gray'
    },
    regimProgramContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 30
    },
    rightText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14
    },
    leftText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 16,
    },
    code: {
        paddingTop: 10,
        paddingBottom: 20,
        color: 'rgb(50, 50, 50)',
        fontSize: 15
    },
    info: {
        paddingTop: 20,
        paddingBottom: 20,
        borderTopWidth: 1,
        borderTopColor: "lightgray",
        borderStyle: 'dashed'
    },
    infoRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    label: {
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    value: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'rgb(32, 193, 188)'
    },
    footerText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 13,
        color: 'lightgray'
    },
    footerContainer: {
        position: 'absolute',
        flex: 1,
        bottom: 0,
        right: 0,
        left: 0
    },
    buttonContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 20,
        paddingTop: 20
    },
    pageButton: {
        flex: .47,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 9,
        borderRadius: 5
    },


});
