import React, {Component} from 'react';
import {ScrollView, View, TouchableOpacity, Image, Text, BackHandler} from 'react-native';
import styles from './styles'
import logo from '../../assets/fitclubLogo.png'
import LinearGradient from 'react-native-linear-gradient'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {Actions} from 'react-native-router-flux'
import FooterMenu from '../../components/footerMenu'
import Dash from 'react-native-dash';

class Receipt extends Component {
    constructor(props){
        super(props);
        this.state = {
            posts: [],
            loading: false,
            active: '',
            friTab: true,
            depTab: false,
            activeSwitch: 1,
            text: '',
            activeButton: 'right'
        };
        this.onBackPress = this.onBackPress.bind(this);

    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                        <View style={styles.iconContainer}>
                            <Icon name="bell-o" size={23} color="white" />
                        </View>
                        <Text style={styles.headerTitle}>رسید</Text>
                        <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                            <Icon name="bars" size={20} color="white" />
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.scrollContainer}>
                        <View style={styles.contentHeader}>
                            <Text style={styles.contentHeaderTitle}>با تشکر از خرید شما</Text>
                            <Image source={logo} style={styles.image} />
                            <Text style={styles.footerText}>شنبه 18 خرداد 1397 - 17: 18 </Text>
                        </View>
                        <View style={styles.userNameContainer}>
                            <Text style={styles.dearText}>عزیز</Text>
                            <Text style={styles.userName}>محمد رضا علیمردانی </Text>
                        </View>
                        <Text style={styles.footerText}>رسید پرداخت شما به شرح زیر می باشد</Text>
                        <View style={styles.regimProgramContainer}>
                            <Text style={styles.leftText}>1,500,000 ریال</Text>
                            <Text style={styles.rightText}>برنامه رژیم و تمرین</Text>
                        </View>
                        <Text style={styles.footerText}>رژیم غذایی + برنامه تمرینی</Text>
                        <Text style={styles.footerText}>آموزش ویدئویی حرکات + پشتیبانی 24 ساعته</Text>
                        <Text style={styles.code}>کد برنامه: FC-47965322</Text>
                        <View style={styles.info}>
                            <View style={styles.infoRow}>
                                <Text style={styles.value}>8887628689888796545</Text>
                                <Text style={styles.label}>کد پیگیری پرداخت شما: </Text>
                            </View>
                            <View style={styles.infoRow}>
                                <Text style={styles.value}>درگاه آسان پرداخت</Text>
                                <Text style={styles.label}>پرداخت از طریق: </Text>
                            </View>
                        </View>
                        <Dash style={{width: '100%', height:3}} dashColor="red" dashLength={6} dashGap={5} />
                        <View style={styles.buttonContainer}>
                            <TouchableOpacity onPress={() => this.setState({activeButton: 'left'})} style={[styles.pageButton, {marginRight: 5, backgroundColor: this.state.activeButton === 'left' ? 'rgb(92, 158, 255)' : 'lightgray' }]}>
                                <View>
                                    <Text style={{color: this.state.activeButton === 'left' ? 'white' : 'gray', fontFamily: this.state.activeButton === 'left' ? 'IRANSansMobile(FaNum)' : 'IRANSansMobile(FaNum)', fontSize: 13 }}>بازگشت به صفحه اصلی</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({activeButton: 'right'})} style={[styles.pageButton, {marginLeft: 5, backgroundColor: this.state.activeButton === 'right' ? 'rgb(92, 158, 255)' : 'lightgray' }]}>
                                <View>
                                    <Text style={[styles.paymentText, {color: this.state.activeButton === 'right' ? 'white' : 'gray', fontFamily: this.state.activeButton === 'right' ? 'IRANSansMobile(FaNum)' : 'IRANSansMobile(FaNum)' }]}>مشاهده برنامه</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
                <View style={styles.footerContainer}>
                    <FooterMenu />
                </View>
            </View>
        );
    }
}
export default Receipt;