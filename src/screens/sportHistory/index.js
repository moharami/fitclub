import React, {Component} from 'react';
import {View, ScrollView, Text, StatusBar, Image, TextInput, TouchableOpacity, BackHandler} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/Feather';
import LinearGradient from 'react-native-linear-gradient'
import FooterMenu from '../../components/footerMenu'
import sport from "../../assets/sportHistory.png";
import Slider from "react-native-slider";
import SwitchButton from 'switch-button-react-native';
import sliderImage from '../../assets/sliderImage.png';
import {Actions} from 'react-native-router-flux'

class SportHistory extends Component {
    constructor(props){
        super(props);
        this.state = {
            value: 0,
            value2: .3,
            activeSwitch: 1,
            color: 'rgb(227, 5, 26)',
            text: '',
            timeText : 'کمتر از 6 ماه'
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    sliderChange(value) {
        this.setState({value: value });
        let timeText = '';
        if( this.state.value >= 0 && this.state.value <= .33) {
            timeText =  'کمتر از 6 ماه';
        }
        else if(this.state.value > .33 && this.state.value <= .66) {
            timeText =  'کمتر از 1 سال';
        }
        else {
            timeText =  'بیشتر از 1 سال ';
        }
        this.setState({timeText: timeText})
    }
    nextStep() {
        let data = this.props.data;
        data.trainYet = this.state.timeText;
        data.trainYetDescription = this.state.text;
        console.log('componentDta', this.props.data)
        Actions.secondQuestion({data: data, titleName: this.props.titleName, price: this.props.price});
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {

        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                        <TouchableOpacity onPress={() => Actions.push('bodyFeatures')} style={styles.iconContainer}>
                            <Icon name="arrow-left" size={25} color="white" />
                        </TouchableOpacity>
                        <View style={styles.headerTitleContainer}>
                            <Text style={styles.headerTitle}>سوابق ورزشی</Text>
                        </View>
                    </LinearGradient>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                    <Image source={sport}  />
                        <View style={styles.question}>
                            <SwitchButton
                                onValueChange={(val) => this.setState({ activeSwitch: val, color: this.state.color === 'rgb(227, 5, 26)' ? 'rgb(32, 193, 189)': 'rgb(227, 5, 26)' })}
                                text1 = 'خیر'
                                text2 = 'بله'
                                switchWidth = {78}
                                switchHeight = {38}
                                switchdirection = 'ltr'
                                switchBorderRadius = {100}
                                switchSpeedChange = {500}
                                switchBorderColor = '#d4d4d4'
                                switchBackgroundColor = '#fff'
                                btnBorderColor = 'transparent'
                                btnBackgroundColor = {this.state.color}
                                fontColor = '#b1b1b1'
                                activeFontColor = '#fff'
                            />
                            <Text style={styles.questionText}>آیا سابقه تمرین قبلی دارید؟</Text>
                        </View>
                        <Text style={styles.timeText}>{this.state.timeText}</Text>
                        <Slider
                            minimumValue={0}
                            maximumValue={1}
                            thumbTintColor="#e9eaed"
                            minimumTrackTintColor="#e9eaed"
                            value={this.state.value}
                            thumbStyle={{borderWidth: 1, borderColor: '#e9eaed', backgroundColor: '#e9eaed', width: 15, height: 15}}
                            maximumTrackTintColor="red"
                            trackStyle={{width: '100%', height: 15, borderRadius: 15, backgroundColor: 'white'}}
                            style={{width: '70%', height: 10, borderRadius: 15, backgroundColor: 'rgb(32, 193, 188)'}}
                            onValueChange={(value) => this.sliderChange(value)}
                        />
                        <TextInput
                            multiline = {true}
                            numberOfLines = {3}
                            value={this.state.text}
                            onChangeText={(text) => this.setState({text: text})}
                            placeholderTextColor={'rgb(142, 142, 142)'}
                            underlineColorAndroid='transparent'
                            placeholder="توضیحات..."
                            style={{
                                // height: 45,
                                paddingRight: 15,
                                width: '90%',
                                fontSize: 18,
                                color: 'rgb(142, 142, 142)',
                                textAlign:'right',
                                borderWidth: 1,
                                borderColor: 'rgb(236, 236, 236)',
                                borderRadius: 5,
                                marginTop: 30
                            }}
                        />
                        <TouchableOpacity style={styles.nextContainer} onPress={() => this.nextStep()}>
                            <Icon name="arrow-right" size={25} color="rgb(77, 205, 202)" />
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                {/*<View style={styles.sliderContainer}>*/}
                    {/*<Slider*/}
                        {/*thumbTintColor="#e9eaed"*/}
                        {/*minimumTrackTintColor="#e9eaed"*/}
                        {/*value={this.state.value2}*/}
                        {/*thumbStyle={{borderWidth: 1, borderColor: '#e9eaed', backgroundColor: '#e9eaed', width: 15, height: 15}}*/}
                        {/*maximumTrackTintColor="red"*/}
                        {/*trackStyle={{width: '100%', height: 15, borderRadius: 15, backgroundColor: 'white', transform: [{ scaleX: -1}]}}*/}
                        {/*style={{width: '70%', height: 10, borderRadius: 15, backgroundColor: 'rgb(32, 193, 188)', transform: [{ scaleX: -1}] }}*/}
                        {/*onValueChange={value => this.setState({value2: value })}*/}

                    {/*/>*/}
                {/*</View>*/}
                <View style={styles.footerContainer}>
                    <FooterMenu />
                </View>
            </View>
        );
    }
}
export default SportHistory;
