import React, {Component} from 'react';
import {ScrollView, View, TouchableOpacity, Text, AsyncStorage, BackHandler} from 'react-native';
import styles from './styles'
import AppSearchBar from '../../components/searchBar'
import SearchItem from '../../components/searchItem'
import LinearGradient from 'react-native-linear-gradient'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {Actions} from 'react-native-router-flux'
import FooterMenu from '../../components/footerMenu'
import Axios from 'axios'
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://fitclub.ws';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import Question from '../../components/question'


class Tickets extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            data: []
        };
        this.onBackPress = this.onBackPress.bind(this);

    }
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        AsyncStorage.getItem('token').then((info) => {
            const newInfo = JSON.parse(info);
            Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
            console.log('comment token', newInfo.token);
            Axios.get('/ticketing/show-all').then(response => {
                const data = response.data.data;
                console.log('question response',response.data.data);
                this.setState({data: data, loading: false})
            })
            .catch((error) =>{
                console.log(error.response);
                    this.setState({loading: false});
                }
            );
        });
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    }
    render() {
        if(this.state.loading)
            return (<Loader txtColor="red" color='red' />)
        else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                            <View style={styles.iconContainer}>
                                <TouchableOpacity onPress={() => Actions.push('addTickets')}>
                                    <FIcon name="plus" size={20} color="white" style={{paddingRight: 20}} />
                                </TouchableOpacity>
                                <Icon name="bell-o" size={23} color="white" />
                            </View>
                            <Text style={styles.headerTitle}>تیکت ها</Text>
                            <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                                <Icon name="bars" size={20} color="white" />
                            </TouchableOpacity>
                        </LinearGradient>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <View style={styles.btnContainer}>
                                <TouchableOpacity style={styles.addItem} onPress={() => Actions.push('addTickets')}>
                                    <Text style={styles.buttonText}>افزودن</Text>
                                </TouchableOpacity>
                            </View>
                            {
                                this.state.data.length !== 0 ? <Text style={styles.label}>لیست تیکت ها</Text>:
                                    <Text style={{color: 'red', textAlign: 'center', padding: 25}}>موردی برای نمایش وجود ندارد</Text>

                            }
                            {
                                this.state.data.map((item) =>
                                    <View style={styles.ticketContainer} key={item.id}>
                                        <Text style={styles.title}>عنوان</Text>
                                        <Text style={[styles.titleText, {paddingBottom: 10}]}>{item.title}</Text>
                                        <Text style={[styles.title, {paddingTop: 20}]}>متن مختصر</Text>
                                        <Text style={styles.titleText}>{item.content}</Text>
                                        <View style={styles.statusContainer}>
                                            <View style={styles.item}>
                                                <Text style={[styles.title, {paddingBottom: 10}]}>عملیات</Text>
                                                <TouchableOpacity style={[styles.item, {backgroundColor: 'rgb(89, 149, 97)'}]} onPress={() => Actions.ticketDetail({item: item})}>
                                                    <Text style={styles.buttonText}>مشاهده</Text>
                                                </TouchableOpacity>
                                            </View>
                                            <View style={styles.item}>
                                                <Text style={[styles.title, {paddingBottom: 10}]}>وضعیت</Text>
                                                <View style={[styles.item, {backgroundColor: 'rgb(255, 193, 7)'}]}>
                                                    <Text style={styles.buttonText}>{item.status === 1 ? 'پاسخ مشتری' : (item.status === 0 ? 'بسته شده' : 'پاسخ داده شده')}</Text>
                                                </View>
                                            </View>
                                            <View style={styles.item}>
                                                <Text style={[styles.title, {paddingBottom: 10}]}>الویت</Text>
                                                <View style={[styles.item, {backgroundColor: 'rgb(255, 193, 7)'}]}>
                                                    <Text style={styles.buttonText}>{item.priority === 1 ? 'مهم' : (item.status === 0 ? 'اضطراری' : 'عادی')}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                )}
                        </View>
                    </ScrollView>
                    <View style={styles.footerContainer}>
                        <FooterMenu />
                    </View>
                </View>
            );
    }

}
export default Tickets;
