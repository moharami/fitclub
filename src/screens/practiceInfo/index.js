import React, {Component} from 'react';
import {View, ScrollView, Text, StatusBar, Image, TouchableOpacity, BackHandler, TextInput} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/Feather';
import LinearGradient from 'react-native-linear-gradient'
import FooterMenu from '../../components/footerMenu'
import practice from "../../assets/practice.png";
import Slider from "react-native-slider";
import home from '../../assets/home.png';
import gym from '../../assets/gym.png';
import env from '../../assets/park.png';
import {Actions} from 'react-native-router-flux'
import SwitchButton from 'switch-button-react-native';

class PracticeInfo extends Component {
    constructor(props){
        super(props);
        this.state = {
            value: 1,
            value2: .5,
            activeSwitch: 1,
            color: 'rgb(227, 5, 26)',
            color2: 'rgb(227, 5, 26)',
            text: '',
            text2: '',
            text3: '',
            activeImage: ''
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    nextStep() {
        let data = this.props.data;
        let place = '';
        if(this.state.activeImage === 'openEnv') {
            place = 'محیط باز';
        }
        else if(this.state.activeImage === 'gym') {
            place = 'باشگاه';
        }
        else if(this.state.activeImage === 'home') {
            place = 'منزل';
        }
        data.trainPlace[0] = place;
        data.trainDays = Math.floor(this.state.value);
        data.trainingDescription  = this.state.text3;
        data.trainingNow   = this.state.activeSwitch !== 1;
        Actions.purpose({data: data, titleName: this.props.titleName, price: this.props.price});
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                        <TouchableOpacity onPress={() => Actions.push('personalHabits')} style={styles.iconContainer}>
                            <Icon name="arrow-left" size={25} color="white" />
                        </TouchableOpacity>
                        <View style={styles.headerTitleContainer}>
                            <Text style={styles.headerTitle}>نحوه انجام تمرین</Text>
                        </View>
                    </LinearGradient>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <Image source={practice} />
                        <Text style={[styles.questionText, {paddingRight: 20, paddingTop: 30, paddingBottom: 15}]}>مایل به انجام تمرین در کجا هستید؟</Text>
                        <View style={styles.imageContainer}>
                            <TouchableOpacity onPress={() => this.setState({activeImage: 'openEnv'})} style={styles.imageItem}>
                                <Image source={env } style={{tintColor: this.state.activeImage === 'openEnv' ? 'red': 'gray', width: 47, height: 53, resizeMode: 'contain'}} />
                                <Text style={[styles.imageText, {color: this.state.activeImage === 'openEnv' ? 'rgb(231, 35, 57)': 'rgb(142, 142, 142)'}]}>محیط باز</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({activeImage: 'gym'})} style={styles.imageItem}>
                                <Image source={ gym } style={{tintColor: this.state.activeImage === 'gym' ? 'red': 'gray', width: 47, height: 53, resizeMode: 'contain'}}/>
                                <Text style={[styles.imageText, {color: this.state.activeImage === 'gym' ? 'rgb(231, 35, 57)': 'rgb(142, 142, 142)'}]}>باشگاه</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({activeImage: 'home'})} style={styles.imageItem}>
                                <Image source={home} style={{tintColor: this.state.activeImage === 'home' ? 'red': 'gray', width: 47, height: 53, resizeMode: 'contain'}} />
                                <Text style={[styles.imageText, {color: this.state.activeImage === 'home' ? 'rgb(231, 35, 57)': 'rgb(142, 142, 142)'}]}>منزل</Text>
                            </TouchableOpacity>
                        </View>
                        <Text style={[styles.questionText, {paddingTop: 20}]}>در هفته چند جلسه میتوانید به تمرین اختصاص بدهید؟</Text>
                        <Text style={styles.timeText}>{Math.ceil(this.state.value)}</Text>
                        <Slider
                            minimumValue={1}
                            maximumValue={10}
                            thumbTintColor="rgb(221, 165, 187)"
                            minimumTrackTintColor="rgb(122, 175, 225)"
                            value={this.state.value}
                            thumbStyle={{borderWidth: 1, borderColor: '#e9eaed', backgroundColor: '#e9eaed', width: 15, height: 15}}
                            maximumTrackTintColor="red"
                            trackStyle={{width: '100%', height: 15, borderRadius: 15, backgroundColor: 'white'}}
                            style={{width: '70%', height: 10, borderRadius: 15, backgroundColor: 'rgb(32, 193, 188)'}}
                            onValueChange={value => this.setState({value: value })}
                        />
                        <Text style={[styles.questionText, {paddingTop: 10, paddingBottom: 10}]}>توضیحات تمرین</Text>
                        <TextInput
                            multiline = {true}
                            numberOfLines = {2}
                            value={this.state.text3}
                            onChangeText={(text) => this.setState({text3: text})}
                            placeholderTextColor={'rgb(142, 142, 142)'}
                            underlineColorAndroid='transparent'
                            placeholder="توضیحات..."
                            style={{
                                // height: 45,
                                paddingRight: 15,
                                width: '90%',
                                fontSize: 18,
                                color: 'rgb(142, 142, 142)',
                                textAlign:'right',
                                borderWidth: 1,
                                borderColor: 'rgb(236, 236, 236)',
                                borderRadius: 5
                            }}
                        />
                        <View style={styles.question}>
                            <SwitchButton
                                onValueChange={(val) => this.setState({ activeSwitch: val, color: this.state.color === 'rgb(227, 5, 26)' ? 'rgb(32, 193, 189)': 'rgb(227, 5, 26)' })}
                                text1 = 'خیر'
                                text2 = 'بله'
                                switchWidth = {78}
                                switchHeight = {38}
                                switchdirection = 'ltr'
                                switchBorderRadius = {100}
                                switchSpeedChange = {500}
                                switchBorderColor = '#d4d4d4'
                                switchBackgroundColor = '#fff'
                                btnBorderColor = 'transparent'
                                btnBackgroundColor = {this.state.color}
                                fontColor = '#b1b1b1'
                                activeFontColor = '#fff'
                            />
                            <Text style={styles.questionText}> در حال حاضر تمرین دارید؟</Text>
                        </View>
                        <TouchableOpacity style={[styles.nextContainer, {marginTop: 40}]} onPress={() => this.nextStep()}>
                            <Icon name="arrow-right" size={25} color="rgb(77, 205, 202)" />
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                <View style={styles.footerContainer}>
                    <FooterMenu />
                </View>
            </View>
        );
    }
}
export default PracticeInfo;

