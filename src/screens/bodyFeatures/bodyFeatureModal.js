import React, { Component } from 'react';
import {
    Modal,
    Text,
    View,
    StyleSheet,
    TouchableOpacity
}
    from 'react-native'
import Icon from 'react-native-vector-icons/dist/Ionicons';
import FIcon from 'react-native-vector-icons/dist/Feather';
import { WheelPicker, DatePicker, TimePicker } from 'react-native-wheel-picker-android'
import LinearGradient from 'react-native-linear-gradient'
import Slider from "react-native-slider";

class BodyFeatureModal extends Component {
    state = {
        modalVisible: false,
        value: .1,
        language: ''
    };

    render() {
        let size = [], weight = [], bellySize = [], armSize = [], hamSize = [], legSize = [], chestSize = [] ;

        for (let i = 30; i <= 200; i++) {
            size.push(i);
        }
        for (let i = 30; i <= 300; i++) {
            weight.push(i);
        }
        for (let i = 30; i <= 300; i++) {
            bellySize.push(i);
        }
        for (let i = 30; i <= 200; i++) {
            armSize.push(i);
        }
        for (let i = 30; i <= 200; i++) {
            hamSize.push(i);
        }
        for (let i = 30; i <= 200; i++) {
            legSize.push(i);
        }
        for (let i = 30; i <= 200; i++) {
            chestSize.push(i);
        }
        let data = {title: '', bgColor: 'red', unit: '', values: []};
        switch(this.props.modalNumber) {
            case 1:
                data.title = 'گروه خونی';
                data.bgColor = 'rgb(277, 5, 26)';
                data.unit = '';
                data.values = ['-A', '+A', '-B', '+B', '-O', '+O', 'AB-', 'AB+'];
                break;
            case 2:
                data.title = 'قد';
                data.bgColor = 'rgb(75, 178, 1)';
                data.unit = 'سانتی متر';
                data.values = size;
                break;
            case 3:
                data.title = 'وزن';
                data.bgColor = 'rgb(0, 140, 253)';
                data.unit = 'کیلوگرم';
                data.values = weight;
                break;
            case 4:
                data.title = 'دور شکم';
                data.bgColor = 'rgb(122, 128, 143)';
                data.unit = 'سانتی متر';
                data.values = bellySize;
                break;
            case 5:
                data.title = 'دور بازو';
                data.bgColor = 'rgb(54, 22, 162)';
                data.unit = 'سانتی متر';
                data.values = armSize;
                break;
            case 6:
                data.title = 'دور ران';
                data.bgColor = 'rgb(218, 128, 18)';
                data.unit = 'سانتی متر';
                data.values = hamSize;
                break;
            case 7:
                data.title = 'دور ساق پا';
                data.bgColor = 'rgb(1, 178, 111)';
                data.unit = 'سانتی متر';
                data.values = legSize;
                break;
            case 8:
                data.title = 'دور سینه';
                data.bgColor = 'rgb(60, 198, 237)';
                data.unit = 'سانتی متر';
                data.values = chestSize;
                break;
        }
        return (
            <View style = {styles.container}>
                <Modal animationType = {"fade"} transparent = {true}
                       visible = {this.props.modalVisible}
                       onRequestClose = {() => this.props.onChange(false)}
                       onBackdropPress= {() => this.props.onChange(!this.state.modalVisible)}
                >
                    <View style = {styles.modal} >
                        <View style={styles.header}>
                            <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                                <View style={styles.iconContainer}>
                                    <FIcon name="arrow-left" size={25} color="white" />
                                </View>
                                <View style={styles.headerTitleContainer}>
                                    <Text style={styles.headerTitle}>مشخصات بدن</Text>
                                </View>
                            </LinearGradient>
                        </View>
                        <View style={[styles.box, {backgroundColor: data.bgColor}]}>
                            <Text style={styles.title}>{data.title}</Text>
                            <Text style={styles.unit}>{data.unit}</Text>
                            <WheelPicker
                                onItemSelected={(date)=>this.props.handleChange(date)}
                                isCurved={false}
                                isCyclic
                                data={data.values}
                                selectedItemTextColor="white"
                                itemSpace={20}
                                visibleItemCount={5}
                                curtainColor="yellow"
                                itemTextSize={40}
                                renderIndicator
                                itemTextFontFamily="IRANSansMobile(FaNum)"
                                indicatorColor="white"
                                itemTextColor="white"
                                style={{width: '33%', height: 200}}/>
                            <TouchableOpacity onPress={() => this.props.closeModal()}>
                                <Icon name="ios-checkmark-circle" size={40} color="white" style={{paddingTop: 40, paddingBottom: 40}} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.sliderContainer}>
                            <Slider
                                thumbTintColor="#e9eaed"
                                minimumTrackTintColor="#e9eaed"
                                value={this.state.value}
                                thumbStyle={{borderWidth: 1, borderColor: '#e9eaed', backgroundColor: '#e9eaed', width: 15, height: 15}}
                                maximumTrackTintColor="red"
                                trackStyle={{width: '100%', height: 15, borderRadius: 15, backgroundColor: 'white', transform: [{ scaleX: -1}]}}
                                style={{width: '70%', height: 10, borderRadius: 15, backgroundColor: 'rgb(32, 193, 188)', transform: [{ scaleX: -1}] }}
                                onValueChange={value => this.setState({value: value })}
                            />
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}
export default BodyFeatureModal

const styles = StyleSheet.create ({
    container: {
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: 'rgb(246, 246, 248)',
        // padding: 100,
        position: 'absolute',
        // bottom: -200
    },
    modal: {
        flexGrow: 1,
        justifyContent: 'flex-start',
        // flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgb(246, 246, 248)',
        paddingRight: 15,
        paddingLeft: 15
    },
    box: {
        width: '100%',
        // height: 120,
        // flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 100,
        height: '75%',
        paddingTop: 30,
        paddingBottom: 30
    },
    title: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 18,
        paddingTop: 30
    },
    unit: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14,
        paddingBottom: 50
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999
    },
    top: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
        paddingRight: 15,
        paddingLeft: 15
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    headerTitleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerTitle: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 16,
        paddingRight: '37%'
    },
    sliderContainer: {
        position: 'absolute',
        flex: 1,
        bottom: 20,
        right: 0,
        left: '20%',
        zIndex: 9999
    },
});