import React, {Component} from 'react';
import {View, ScrollView, Text, StatusBar, TouchableOpacity, BackHandler, Alert} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/Feather';
import LinearGradient from 'react-native-linear-gradient'
import FooterMenu from '../../components/footerMenu'
import BodyFeature from "../../components/bodyFeature/index";
import BodyFeatureModal from "./bodyFeatureModal";
import {Actions} from 'react-native-router-flux'

class BodyFeatures extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: .1,
            modalVisible: false,
            modalNumber: 0,
            data: [],
            empty: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    handlePress(id) {
        this.setState({modalVisible: !this.state.modalVisible, modalNumber: id});
    }
    setModalVisible() {
        this.setState({modalVisible: !this.state.modalVisible});
    }
    closeModal() {
        this.setState({modalVisible: false});

    }
    handleChange(value) {
        const newData = this.state.data;
        newData[this.state.modalNumber-1] = value;
        this.setState({data: newData})
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    nextStep() {
        for(let i=0; i<8; i++) {
            if(!this.state.data[i] ){
                this.setState({empty: true });
                Alert.alert('','لطفا تمام موارد را پر نمایید');
                return 0;
            }
        }
        let data = {
            point:[],
            height:175,
            weight:75,
            bloodType:'',
            chestSize:48,
            armSize:48,
            stomachSize:48,
            legSize:48,
            footSize:48,
            trainYet:'',
            trainYetDescription:'',
            weightChange:0,
            weightChangeDescription:'',
            usePro:'',
            likePro:'',
            disease:'',
            medicine:'',
            allergy:'',
            habit:'',
            meal:3,
            appetite:'',
            sleep:'',
            wakeUp:'',
            trainPlace:[],
            trainDays:3,
            pointWeight: 0,
            milk: '',
            drink: '',
            ejabat: '',
            injury: '',
            dryMilk: '',
            pregnant: '',
            cigarette: '',
            twiceMeal: '',
            dontUseMeal: '',
            trainingNow: false,
            childAllergy: '',
            bloodExperiment: '',
            dietDescription: '',
            pregnantHistory: '',
            trainingDescription: '',
        };
        data.bloodType = this.state.data[0].data;
        data.height = this.state.data[1].data;
        data.weight = this.state.data[2].data;
        data.stomachSize = this.state.data[3].data;
        data.armSize = this.state.data[4].data;
        data.legSize = this.state.data[5].data;
        data.footSize = this.state.data[6].data;
        data.chestSize = this.state.data[7].data;
        Actions.sportHistory({data: data, titleName: this.props.titleName, price: this.props.price});
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                        <TouchableOpacity onPress={() => Actions.push('programFeature')} style={styles.iconContainer}>
                            <Icon name="arrow-left" size={25} color="white" />
                        </TouchableOpacity>
                        <View style={styles.headerTitleContainer}>
                            <Text style={styles.headerTitle}>مشخصات بدن</Text>
                        </View>
                    </LinearGradient>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <View style={styles.featureRow}>
                            <BodyFeature imgSrc="image2" label="قد"  labelValue="سانتی متر" id={2} handlePress={() => this.handlePress(2)} data={this.state.data} />
                            <BodyFeature imgSrc="image1" label="گروه خونی" id={1} handlePress={() => this.handlePress(1)} data={this.state.data} />
                        </View>
                        <View style={styles.featureRow}>
                            <BodyFeature imgSrc="image4" label="دور شکم" labelValue="سانتی متر" id={4} handlePress={() => this.handlePress(4)} data={this.state.data} />
                            <BodyFeature imgSrc="image3" label="وزن" labelValue="کیلو گرم" id={3} handlePress={() => this.handlePress(3)} data={this.state.data} />
                        </View>
                        <View style={styles.featureRow}>
                            <BodyFeature imgSrc="image6" label="دور ران" labelValue="سانتی متر" id={6} handlePress={() => this.handlePress(6)} data={this.state.data} />
                            <BodyFeature imgSrc="image5" label="دور بازو" labelValue="سانتی متر" id={5} handlePress={() => this.handlePress(5)} data={this.state.data} />
                        </View>
                        <View style={styles.featureRow}>
                            <BodyFeature imgSrc="image8" label="دور سینه" labelValue="سانتی متر" id={8} handlePress={() => this.handlePress(8)} data={this.state.data} />
                            <BodyFeature imgSrc="image7" label="دور ساق پا" labelValue="سانتی متر" id={7} handlePress={() => this.handlePress(7)} data={this.state.data} />
                        </View>
                        <TouchableOpacity style={styles.nextContainer} onPress={()=> this.nextStep()}>
                            <Icon name="arrow-right" size={25} color="rgb(77, 205, 202)" />
                        </TouchableOpacity>
                        <BodyFeatureModal
                            closeModal={() => this.closeModal()}
                            modalVisible={this.state.modalVisible}
                            modalNumber={this.state.modalNumber}
                            handleChange={(value) => this.handleChange(value)}
                            onChange={(visible) => this.setModalVisible(visible)}
                        />
                    </View>
                </ScrollView>
                <View style={styles.footerContainer}>
                    <FooterMenu />
                </View>
            </View>
        );
    }
}
export default BodyFeatures;
