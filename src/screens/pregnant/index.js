
import React, {Component} from 'react';
import {View, ScrollView, Text, StatusBar, Image, TextInput, TouchableOpacity, BackHandler} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/Feather';
import LinearGradient from 'react-native-linear-gradient'
import FooterMenu from '../../components/footerMenu'
import habbitation from "../../assets/habbitation.png";
import Slider from "react-native-slider";
import SwitchButton from 'switch-button-react-native';
import {Actions} from 'react-native-router-flux'

class Pregnant extends Component {
    constructor(props){
        super(props);
        this.state = {
            value: .5,
            activeSwitch: 1,
            color: 'rgb(227, 5, 26)',
            color2: 'rgb(227, 5, 26)',
            text: '',
            text2: '',
            text3: '',
            text4: '',
            text5: '',
            activeButton: 3
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    nextStep() {
        let data = this.props.data;
        data.pregnantHistory  = this.state.text1;
        data.pregnant = this.state.text2;
        data.milk = this.state.text3;
        data.dryMilk = this.state.text4;
        data.childAllergy = this.state.text5;
        Actions.finalConfirm({data: data, titleName: this.props.titleName, price: this.props.price});
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                        <TouchableOpacity onPress={() => Actions.push('foodHabbitation')} style={styles.iconContainer}>
                            <Icon name="arrow-left" size={25} color="white" />
                        </TouchableOpacity>
                        <View style={styles.headerTitleContainer}>
                            <Text style={styles.headerTitle}>سابقه بارداری</Text>
                        </View>
                    </LinearGradient>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <Image source={habbitation} />
                        <View style={styles.questionTextContainer}>
                            <Text style={styles.questionText}>آیا سایقه بارداری دارید؟</Text>
                        </View>
                        <TextInput
                            multiline = {true}
                            numberOfLines = {2}
                            value={this.state.text1}
                            onChangeText={(text) => this.setState({text1: text})}
                            placeholderTextColor={'rgb(142, 142, 142)'}
                            underlineColorAndroid='transparent'
                            placeholder="توضیحات..."
                            style={{
                                // height: 45,
                                paddingRight: 15,
                                width: '90%',
                                fontSize: 18,
                                color: 'rgb(142, 142, 142)',
                                textAlign:'right',
                                borderWidth: 1,
                                borderColor: 'rgb(236, 236, 236)',
                                borderRadius: 5
                            }}
                        />
                        <View style={styles.questionTextContainer}>
                            <Text style={styles.questionText}>در حال حاضر باردار هستید؟</Text>
                        </View>
                        <TextInput
                            multiline = {true}
                            numberOfLines = {2}
                            value={this.state.text2}
                            onChangeText={(text) => this.setState({text2: text})}
                            placeholderTextColor={'rgb(142, 142, 142)'}
                            underlineColorAndroid='transparent'
                            placeholder="توضیحات..."
                            style={{
                                // height: 45,
                                paddingRight: 15,
                                width: '90%',
                                fontSize: 18,
                                color: 'rgb(142, 142, 142)',
                                textAlign:'right',
                                borderWidth: 1,
                                borderColor: 'rgb(236, 236, 236)',
                                borderRadius: 5
                            }}
                        />
                        <View style={styles.questionTextContainer}>
                            <Text style={styles.questionText}>در حال حاضر شیردهی دارید؟</Text>
                        </View>
                        <TextInput
                            multiline = {true}
                            numberOfLines = {2}
                            value={this.state.text3}
                            onChangeText={(text) => this.setState({text3: text})}
                            placeholderTextColor={'rgb(142, 142, 142)'}
                            underlineColorAndroid='transparent'
                            placeholder="توضیحات..."
                            style={{
                                // height: 45,
                                paddingRight: 15,
                                width: '90%',
                                fontSize: 18,
                                color: 'rgb(142, 142, 142)',
                                textAlign:'right',
                                borderWidth: 1,
                                borderColor: 'rgb(236, 236, 236)',
                                borderRadius: 5
                            }}
                        />
                        <View style={styles.questionTextContainer}>
                            <Text style={styles.questionText}>فرزند از شیر خشک استفاده میکند؟</Text>
                        </View>
                        <TextInput
                            multiline = {true}
                            numberOfLines = {2}
                            value={this.state.text4}
                            onChangeText={(text) => this.setState({text4: text})}
                            placeholderTextColor={'rgb(142, 142, 142)'}
                            underlineColorAndroid='transparent'
                            placeholder="توضیحات..."
                            style={{
                                // height: 45,
                                paddingRight: 15,
                                width: '90%',
                                fontSize: 18,
                                color: 'rgb(142, 142, 142)',
                                textAlign:'right',
                                borderWidth: 1,
                                borderColor: 'rgb(236, 236, 236)',
                                borderRadius: 5
                            }}
                        />
                        <View style={styles.questionTextContainer}>
                            <Text style={styles.questionText}>فرزند به ماده خاصی حساسیت دارد؟</Text>
                        </View>
                        <TextInput
                            multiline = {true}
                            numberOfLines = {2}
                            value={this.state.text5}
                            onChangeText={(text) => this.setState({text5: text})}
                            placeholderTextColor={'rgb(142, 142, 142)'}
                            underlineColorAndroid='transparent'
                            placeholder="توضیحات..."
                            style={{
                                // height: 45,
                                paddingRight: 15,
                                width: '90%',
                                fontSize: 18,
                                color: 'rgb(142, 142, 142)',
                                textAlign:'right',
                                borderWidth: 1,
                                borderColor: 'rgb(236, 236, 236)',
                                borderRadius: 5
                            }}
                        />
                        <TouchableOpacity style={[styles.nextContainer, {marginTop: 40}]} onPress={() => this.nextStep()}>
                            <Icon name="arrow-right" size={25} color="rgb(77, 205, 202)" />
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                <View style={styles.footerContainer}>
                    <FooterMenu />
                </View>
            </View>
        );
    }
}
export default Pregnant;

