import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(251, 251, 251)',
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999
    },
    top: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
        paddingRight: 15,
        paddingLeft: 15
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    headerTitleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerTitle: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 16,
        paddingRight: '35%'
    },
    body: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 15,
        paddingRight: 15,
        marginBottom: 20
    },
    bodyContainer: {
        // paddingBottom: 200,
        // alignItems: 'center',
        // justifyContent: 'center',
        paddingBottom: 50,
        backgroundColor: 'white',
        paddingTop: 20,
        paddingRight: 10,
        borderColor: 'rgb(237, 237, 237)',
        borderWidth: 1
    },
    slideContainers: {
        flexDirection: 'row',
        transform: [
            {rotateY: '180deg'},
        ],
        borderBottomColor: 'rgb(237, 237, 237)',
        borderBottomWidth: 1,
        paddingBottom: 30
    },
    slideContainersBottom: {
        flexDirection: 'row',
        transform: [
            {rotateY: '180deg'},
        ],
        paddingBottom: 30
    },
    slideText: {
        paddingLeft: 30,
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    timeContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 30,
        paddingRight: 15,
        paddingLeft: 20,
        paddingBottom: 40
    },
    timeIconContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 35,
        height: 35,
        borderRadius: 35,
        backgroundColor: 'rgb(227, 5, 26)'
    },
    label: {
        paddingRight: 10,
        paddingBottom: 20,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12
    },
    scroll: {
        paddingTop: 75,
        paddingRight: 15,
        marginBottom: 50

    },
    footerContainer: {
        position: 'absolute',
        flex: 1,
        bottom: 0,
        right: 0,
        left: 0
    },
    buttonTitle: {
        fontSize: 15,
        color: 'lightgray'
    }
});
