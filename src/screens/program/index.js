import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Linking, BackHandler} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/Feather';
import FIcon from 'react-native-vector-icons/dist/FontAwesome';
import MIcon from 'react-native-vector-icons/dist/MaterialIcons';
import LinearGradient from 'react-native-linear-gradient'
import FooterMenu from '../../components/footerMenu'
import SwitchButton from 'switch-button-react-native';
import {Actions} from 'react-native-router-flux'

// import ProgramModal from './programModal'
import ProgramItemContainer from "../../components/programItemContainer/index";

class Program extends Component {
    constructor(props){
        super(props);
        this.state = {
            activeText: 1,
            activeSwitch: 1,
            modalVisible: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    // setModalVisible() {
    //     this.setState({modalVisible: !this.state.modalVisible});
    // }
    // closeModal() {
    //     this.setState({modalVisible: false});
    //
    // }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    openPdf() {
        Linking.openURL('http://www.fitclub.ws.info/panel/'+this.props.pName+'Single/'+this.props.item.id+'/print/pdf');
    }
    render() {
        let arr = [];
        for(let i=1 ; i<= this.props.item.days_count; i++){
            arr.push(i)
        }
        let setData = null;
        const myInfo = JSON.parse(this.props.item.days[this.state.activeText-1].data);
        for(const k in myInfo){
            setData = myInfo[k].map((item)=> <ProgramItemContainer item={item} key={item.move_id} /> )
        }
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                        <TouchableOpacity onPress={() => Actions.push('UserPrograms')} style={styles.iconContainer}>
                            <Icon name="arrow-left" size={25} color="white" />
                        </TouchableOpacity>
                        <View style={styles.headerTitleContainer}>
                            <TouchableOpacity  onPress={() => this.setState({modalVisible: !this.state.modalVisible })}>
                                <MIcon name="keyboard-arrow-down" size={22} color="white" />
                            </TouchableOpacity>
                            <Text style={styles.headerTitle}>برنامه شماره 1</Text>
                        </View>
                    </LinearGradient>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ transform: [{ scaleX: -1}]}}>
                            <View style={styles.slideContainers}>
                                {
                                    arr.map((item) =>
                                        <TouchableOpacity onPress={() => this.setState({activeText: item})} key={item}>
                                            <Text style={[styles.slideText, {color: this.state.activeText === item ? 'black' : 'gray', fontSize:  this.state.activeText === item ? 14 : 12 }]}>برنامه تمرینی روز {item} </Text>
                                        </TouchableOpacity>
                                    )
                                }
                            </View>
                        </ScrollView>
                        <View style={styles.timeContainer}>
                            <SwitchButton
                                onValueChange={(val) => this.setState({ activeSwitch: val })}
                                text1 = 'پایان'
                                text2 = '5:27'
                                switchWidth = {135}
                                switchHeight = {35}
                                switchdirection = 'ltr'
                                switchBorderRadius = {100}
                                switchSpeedChange = {500}
                                switchBorderColor = '#d4d4d4'
                                switchBackgroundColor = '#fff'
                                btnBorderColor = 'transparent'
                                btnBackgroundColor = 'rgb(227, 5, 26)'
                                fontColor = '#b1b1b1'
                                activeFontColor = '#fff'
                            />
                            <TouchableOpacity onPress={() => this.openPdf()} style={styles.timeIconContainer}>
                                <FIcon name="file-pdf-o" size={20} color="white" />
                            </TouchableOpacity>
                        </View>
                        {
                            setData
                        }
                        {/*<ProgramModal*/}
                            {/*closeModal={() => this.closeModal()}*/}
                            {/*modalVisible={this.state.modalVisible}*/}
                            {/*onChange={(visible) => this.setModalVisible(visible)}*/}
                        {/*/>*/}
                    </View>
                </ScrollView>
                <View style={styles.footerContainer}>
                    <FooterMenu />
                </View>
            </View>
        );
    }
}
export default Program;
