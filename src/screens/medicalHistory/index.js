
import React, {Component} from 'react';
import {View, ScrollView, Text, StatusBar, Image, TextInput, TouchableOpacity, BackHandler} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/Feather';
import LinearGradient from 'react-native-linear-gradient'
import FooterMenu from '../../components/footerMenu'
import medical from "../../assets/medical.png";
import Slider from "react-native-slider";
import SwitchButton from 'switch-button-react-native';
import {Actions} from 'react-native-router-flux'

class MedicalHistory extends Component {
    constructor(props){
        super(props);
        this.state = {
            value: .5,
            activeSwitch: 1,
            color: 'rgb(227, 5, 26)',
            color2: 'rgb(227, 5, 26)',
            color3: 'rgb(227, 5, 26)',
            color4: 'rgb(227, 5, 26)',
            text: '',
            text2: '',
            text3: '',
            text4: ''
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    nextStep() {
        let data = this.props.data;
        data.disease = this.state.text;
        data.medicine = this.state.text2;
        data.injury = this.state.text3;
        data.bloodExperiment  = this.state.text4;
        Actions.foodHabbitation({data: data, titleName: this.props.titleName, price: this.props.price});
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                        <TouchableOpacity onPress={() => Actions.push('thirdQuestion')} style={styles.iconContainer}>
                            <Icon name="arrow-left" size={25} color="white" />
                        </TouchableOpacity>
                        <View style={styles.headerTitleContainer}>
                            <Text style={styles.headerTitle}>سوابق پزشکی</Text>
                        </View>
                    </LinearGradient>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <Image source={medical} />
                        <View style={styles.question}>
                            <SwitchButton
                                onValueChange={(val) => this.setState({ activeSwitch: val, color: this.state.color === 'rgb(227, 5, 26)' ? 'rgb(32, 193, 189)': 'rgb(227, 5, 26)' })}
                                text1 = 'خیر'
                                text2 = 'بله'
                                switchWidth = {78}
                                switchHeight = {38}
                                switchdirection = 'ltr'
                                switchBorderRadius = {100}
                                switchSpeedChange = {500}
                                switchBorderColor = '#d4d4d4'
                                switchBackgroundColor = '#fff'
                                btnBorderColor = 'transparent'
                                btnBackgroundColor = {this.state.color}
                                fontColor = '#b1b1b1'
                                activeFontColor = '#fff'
                            />
                            <Text style={styles.questionText}>آیا سابقه بیماری دارید؟</Text>
                        </View>
                        <TextInput
                            multiline = {true}
                            numberOfLines = {2}
                            value={this.state.text}
                            onChangeText={(text) => this.setState({text: text})}
                            placeholderTextColor={'lightgray'}
                            underlineColorAndroid='transparent'
                            placeholder="توضیحات..."
                            style={{
                                // height: 45,
                                paddingRight: 15,
                                width: '90%',
                                fontSize: 18,
                                color: 'rgb(142, 142, 142)',
                                textAlign:'right',
                                borderRadius: 5,
                                backgroundColor: 'rgb(233, 234, 237)'
                            }}
                        />
                        <View style={styles.question}>
                            <SwitchButton
                                onValueChange={(val) => this.setState({ activeSwitch: val, color2: this.state.color2 === 'rgb(227, 5, 26)' ? 'rgb(32, 193, 189)': 'rgb(227, 5, 26)' })}
                                text1 = 'خیر'
                                text2 = 'بله'
                                switchWidth = {78}
                                switchHeight = {38}
                                switchdirection = 'ltr'
                                switchBorderRadius = {100}
                                switchSpeedChange = {500}
                                switchBorderColor = '#d4d4d4'
                                switchBackgroundColor = '#fff'
                                btnBorderColor = 'transparent'
                                btnBackgroundColor = {this.state.color2}
                                fontColor = '#b1b1b1'
                                activeFontColor = '#fff'
                            />
                            <Text style={styles.questionText}>آیا داروی خاصی مصرف می کنید؟</Text>
                        </View>
                        <TextInput
                            multiline = {true}
                            numberOfLines = {2}
                            value={this.state.text2}
                            onChangeText={(text) => this.setState({text2: text})}
                            placeholderTextColor={'rgb(142, 142, 142)'}
                            underlineColorAndroid='transparent'
                            placeholder="توضیحات..."
                            style={{
                                // height: 45,
                                paddingRight: 15,
                                width: '90%',
                                fontSize: 18,
                                color: 'rgb(142, 142, 142)',
                                textAlign:'right',
                                borderWidth: 1,
                                borderColor: 'rgb(236, 236, 236)',
                                borderRadius: 5
                            }}
                        />
                        <View style={styles.question}>
                            <SwitchButton
                                onValueChange={(val) => this.setState({ activeSwitch: val, color3: this.state.color3 === 'rgb(227, 5, 26)' ? 'rgb(32, 193, 189)': 'rgb(227, 5, 26)' })}
                                text1 = 'خیر'
                                text2 = 'بله'
                                switchWidth = {78}
                                switchHeight = {38}
                                switchdirection = 'ltr'
                                switchBorderRadius = {100}
                                switchSpeedChange = {500}
                                switchBorderColor = '#d4d4d4'
                                switchBackgroundColor = '#fff'
                                btnBorderColor = 'transparent'
                                btnBackgroundColor = {this.state.color3}
                                fontColor = '#b1b1b1'
                                activeFontColor = '#fff'
                            />
                            <Text style={styles.questionText}>آیا سابقه آسیب دیدگی دارید؟</Text>
                        </View>
                        <TextInput
                            multiline = {true}
                            numberOfLines = {2}
                            value={this.state.text3}
                            onChangeText={(text) => this.setState({text3: text})}
                            placeholderTextColor={'rgb(142, 142, 142)'}
                            underlineColorAndroid='transparent'
                            placeholder="توضیحات..."
                            style={{
                                // height: 45,
                                paddingRight: 15,
                                width: '90%',
                                fontSize: 18,
                                color: 'rgb(142, 142, 142)',
                                textAlign:'right',
                                borderWidth: 1,
                                borderColor: 'rgb(236, 236, 236)',
                                borderRadius: 5
                            }}
                        />
                        <View style={styles.question}>
                            <Text style={styles.questionText}>آیا موارد غیر عادی در آزمایش خون مشاهده شده است؟</Text>
                        </View>
                        <TextInput
                            multiline = {true}
                            numberOfLines = {2}
                            value={this.state.text4}
                            onChangeText={(text) => this.setState({text4: text})}
                            placeholderTextColor={'rgb(142, 142, 142)'}
                            underlineColorAndroid='transparent'
                            placeholder="توضیحات..."
                            style={{
                                // height: 45,
                                paddingRight: 15,
                                width: '90%',
                                fontSize: 18,
                                color: 'rgb(142, 142, 142)',
                                textAlign:'right',
                                borderWidth: 1,
                                borderColor: 'rgb(236, 236, 236)',
                                borderRadius: 5
                            }}
                        />
                        <TouchableOpacity style={[styles.nextContainer, {marginTop: 40}]} onPress={() => this.nextStep()}>
                            <Icon name="arrow-right" size={25} color="rgb(77, 205, 202)" />
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                <View style={styles.footerContainer}>
                    <FooterMenu />
                </View>
            </View>
        );
    }
}
export default MedicalHistory;

