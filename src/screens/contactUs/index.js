
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Image, Dimensions, Linking, BackHandler} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import EIcon from 'react-native-vector-icons/dist/EvilIcons';
import LinearGradient from 'react-native-linear-gradient'
import FooterMenu from '../../components/footerMenu'
import Logo from '../../assets/fitclubLogo.png'
import marker from '../../assets/marker.png'
import MapView from 'react-native-maps';
import Axios from 'axios'
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://fitclub.ws';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'

class ContactUs extends Component {
    constructor(props){
        super(props);
        this.state = {
            posts: [],
            loading: false,
            active: '',
            windowHeight: 0
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    openLink(num) {
        if(num === 1 ) {
            Linking.openURL('https://plus.google.com/u/1/111659695678489068684')
        }
        else if (num === 2) {
            Linking.openURL('http://t.me/fitclub_ir')
        }
        else if (num === 3) {
            Linking.openURL('https://twitter.com/FitclubIr')
        }
        else if (num === 4) {
            Linking.openURL('https://www.instagram.com/fitclub_ir/')
        }

    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        if(this.state.loading)
            return (<Loader txtColor="red" color='red' />);
        else return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                        <View style={styles.iconContainer}>
                            <TouchableOpacity onPress={() => Actions.push('home')}>
                                <FIcon name="arrow-left" size={25} color="white" style={{paddingRight: 20}} />
                            </TouchableOpacity>
                            <Icon name="bell-o" size={23} color="white" />
                        </View>
                        <Text style={styles.headerTitle}>تماس با ما</Text>
                        <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                            <Icon name="bars" size={20} color="white" />
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <MapView
                            style={{width: '100%', height: Dimensions.get('window').height}}
                            region={{
                                latitude: 37.78825,
                                longitude: -122.4324,
                                latitudeDelta: 0.015,
                                longitudeDelta: 0.0121,
                            }}
                        >
                        </MapView>
                        <Image source={marker} style={styles.marker} />

                    </View>
                </ScrollView>
                <View style={styles.info}>
                    <Image source={Logo} style={styles.image} />
                    <View style={styles.infoContainer}>
                        <Text style={styles.label}>آدرس</Text>
                        <Text style={styles.value}>تهران خیابان شریعتی بازار نگین ظفر</Text>
                        <Text style={[styles.label, {paddingTop: 15}]}>شماره تماس</Text>
                        <Text style={styles.value}>021 4722063</Text>
                    </View>
                    <View style={styles.socialContainer}>
                        <TouchableOpacity onPress={() => this.openLink(1)}>
                            <View style={styles.circle}>
                                <Icon name="google-plus" size={15} color="black" />
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.openLink(2)}>
                            <View style={styles.circle}>
                                <EIcon name="sc-telegram" size={27} color="black" />
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.openLink(3)}>
                            <View style={styles.circle}>
                                <EIcon name="sc-twitter" size={30} color="black" />
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.openLink(4)}>
                            <View style={styles.circle}>
                                <Icon name="instagram" size={19} color="black" />
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.footerContainer}>
                    <FooterMenu active="contact" />
                </View>
            </View>
        );
    }
}
export default ContactUs;
