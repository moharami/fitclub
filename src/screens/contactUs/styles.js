import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(251, 251, 251)',
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999
    },
    top: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
        paddingRight: 15,
        paddingLeft: 15
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    headerTitle: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 20,
        paddingRight: '15%',
    },
    body: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 15,
        paddingRight: 15,
        marginBottom: 20
    },
    bodyContainer: {
        // paddingBottom: 200,
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 150,
        backgroundColor: 'white',
        position: 'relative'
    },
    marker: {
        position: 'absolute',
        top: 90,
        right: '40%',
        zIndex: 999
    },
    scroll: {
        paddingTop: 70
    },
    footerContainer: {
        position: 'absolute',
        flex: 1,
        bottom: 0,
        right: 0,
        left: 0
    },
    buttonTitle: {
        fontSize: 15,
        color: 'lightgray'
    },
    image: {
        width: '45%',
        resizeMode: 'contain'
    },
    info: {
        width: '90%',
        alignItems: 'center',
        justifyContent: 'center',
        // padding: 10,
        backgroundColor: 'white',
        borderColor: 'rgb(237, 237, 237)',
        borderWidth: 1,
        position: 'absolute',
        bottom: 70,
        // right: 0,
        left: '4%',
        zIndex: 9999
    },
    infoContainer: {
        width: '95%',
        alignItems: 'center',
        justifyContent: 'center',
        borderTopColor: 'rgb(237, 237, 237)',
        borderTopWidth: 1,
        padding: 10
    },
    label: {
        color: 'rgb(146, 146, 146)',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14
    },
    value: {
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 16
    },
    circle: {
        width: 35,
        height: 35,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 60,
        borderColor: 'rgb(237, 237, 237)',
        borderWidth: 2

    },
    questionText: {
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14

    },
    socialContainer: {
        flexDirection: 'row',
        width: '60%',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingBottom: 15

    }

});
