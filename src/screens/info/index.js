import React, {Component} from 'react';
import {ImageBackground, View, TouchableOpacity, Text, AsyncStorage, BackHandler} from 'react-native';
import background from '../../assets/background.jpg'
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import SlideShow from '../../components/slideShow'
import Axios from 'axios'
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://fitclub.ws';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import {store} from '../../config/store';

class Info extends Component {

    constructor(props){
        super(props);
        this.state = {
            info: false,
            // loading: false

        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

        try {
            AsyncStorage.getItem('token').then((info) => {
                if(info !== null) {
                    const newInfo = JSON.parse(info);
                    Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
                    console.log('comment token', newInfo.token);

                    Axios.get('/is_auth').then(response => {
                        console.log('user INFO FOR LOGIN', response);
                        if(response.data.data) {
                            // this.setState({info: false});
                            Actions.push('home');
                        }
                        else  {
                            Actions.push('Login');
                            // this.setState({info: true});
                        }
                    })
                        .catch( (error) =>{
                                this.setState({info: true});
                            }
                        );
                }
                else{
                    this.setState({info: true});
                }
            });
        } catch (error) {
            console.log(error)
        }
    }
    render() {
        if(!this.state.info){
            return (<Loader txtColor="red" color='red' />)
        }
        else
            return (
                <ImageBackground style={styles.container} source={background}>
                    <View style={styles.slideshowContainer}>
                        <SlideShow activeSlide={1}/>
                    </View>
                    <TouchableOpacity onPress={() => Actions.Login()} style={styles.advertise}>
                        <View>
                            <Text style={styles.buttonTitle}>شروع</Text>
                        </View>
                    </TouchableOpacity>
                </ImageBackground>
            );
    }
}
export default Info;