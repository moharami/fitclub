import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        alignItems: 'center',
        // justifyContent: 'space-between',
        // paddingTop: 100

    },
    slideshowContainer: {
        position: 'absolute',
        bottom: '7%'
    },
    advertise: {
        width: '40%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: 'rgb(277, 5, 26)',
        padding: 10,
        elevation: 3,
        position: 'absolute',
        bottom: '3%'
    },
    buttonTitle: {
        fontSize: 17,
        color: 'white',
        fontWeight: 'bold'
    },
});
