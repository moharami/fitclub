
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(251, 251, 251)',
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 100,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999
    },
    top: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 90,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999
    },
    topHeader: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
        paddingRight: 15,
        paddingLeft: 15
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    headerTitle: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 20,
        paddingRight: '15%',
    },
    // navbar: {
    //     flexDirection: 'row',
    //     position: 'absolute',
    //     top: 70,
    //     right: 0,
    //     left: 0,
    //     zIndex: 9999,
    // },
    body: {
        // flexDirection: 'row',
        flexDirection: 'row-reverse',
        flexWrap: 'wrap',
        // paddingLeft: 15,
        padding: 10,
        marginBottom: 20,
        // direction: 'rtl'
    },
    bodyContainer: {
        paddingBottom: 200,

    },
    filtering: {
        position: 'absolute',
        bottom: 25,
        width: '60%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 15,
        paddingLeft: 15,
        backgroundColor: 'rgb(231, 21, 56)',
        borderRadius: 30
    },
    scroll: {
        paddingTop: 100
    },
    filterContainer: {
        // flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonTitle: {
        fontSize: 15,
        color: 'lightgray'
    }
});
