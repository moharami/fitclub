import React, {Component} from 'react';
import {ScrollView, View, BackHandler, TouchableOpacity, Text} from 'react-native';
import styles from './styles'
import AppSearchBar from '../../components/searchBar'
import SearchItem from '../../components/searchItem'
import Axios from 'axios'
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://fitclub.ws';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import {List, SearchBar} from "react-native-elements";
import {Actions} from 'react-native-router-flux'
import LinearGradient from 'react-native-linear-gradient'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';

class Search extends Component {
    constructor(props){
        super(props);
        this.state = {
            posts: [],
            loading: false,
            text: '',
            data: []

        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    _doSearch(text) {
        this.setState({loading: true, text: text});
        console.log('search text ', text);
        Axios.get('/blog/search?q='+this.state.text).then(response => {
            const data = response.data.data;
            console.log('search response',response.data);
            this.setState({data: data, loading: false})
        })
        .catch(function (error) {
                console.log(error.response);
            }
        );
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        if(this.state.loading)
            return (<Loader txtColor="red" color='red' />)
        else
        return (
            <View style={styles.container}>
                {/*<LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.topHeader}>*/}
                    {/*<View style={styles.iconContainer}>*/}
                        {/*<TouchableOpacity onPress={() => Actions.push('home')}>*/}
                            {/*<FIcon name="arrow-left" size={25} color="white" style={{paddingRight: 20}} />*/}
                        {/*</TouchableOpacity>*/}
                        {/*<Icon name="bell-o" size={23} color="white" />*/}
                    {/*</View>*/}
                    {/*<Text style={styles.headerTitle}>تماس با ما</Text>*/}
                    {/*<TouchableOpacity onPress={() => Actions.drawerOpen()}>*/}
                        {/*<Icon name="bars" size={20} color="white" />*/}
                    {/*</TouchableOpacity>*/}
                {/*</LinearGradient>*/}
                <View style={styles.header}>
                    <View style={styles.top}>
                        <View style={{width: '95%'}}>
                            <SearchBar
                                containerStyle={{
                                    backgroundColor: 'transparent',
                                    borderWidth: 0,
                                    shadowColor: 'white',
                                    borderBottomColor: 'transparent',
                                    borderTopColor: 'transparent'
                                }}
                                value={this.state.text}
                                onChangeText={(text) => this._doSearch(text)}
                                onClearText={(text) => this._doSearch(text)}
                                placeholder="جستجو ..."
                                round
                                inputStyle={{textAlign: 'right',  paddingRight: '40%', backgroundColor: 'rgb(243, 243, 243)'}}
                            />
                        </View>
                    </View>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <View style={styles.body}>
                            {
                                this.state.data.blog ? this.state.data.blog.map((item) =>  <SearchItem title={item.title} key={item.id} item={item} relatedItems={this.state.data.blog} />) : null

                            }
                            {
                                this.state.data.category ? this.state.data.category.map((item) =>  <SearchItem title={item.title} key={item.id} item={item} relatedItems={this.state.data.category} />) : null

                            }
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
export default Search;
