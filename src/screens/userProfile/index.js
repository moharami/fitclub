import React, {Component} from 'react';
import {ScrollView, View, TouchableOpacity, Text, Image, AsyncStorage, BackHandler, Alert} from 'react-native';
import styles from './styles'
import LinearGradient from 'react-native-linear-gradient'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {Actions} from 'react-native-router-flux'
import FooterMenu from '../../components/footerMenu'
import History from '../../components/history'
import layer from '../../assets/layer.png'
import ProfileOptions from "../../components/profileOptions/index";
import ProfileModal from './profileModal'
import { AreaChart, Grid } from 'react-native-svg-charts'
import * as shape from 'd3-shape'
import {connect} from 'react-redux';
import Axios from 'axios'
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://fitclub.ws';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import {store} from '../../config/store';

class UserProfile extends Component {
    constructor(props){
        super(props);
        this.state = {
            posts: [],
            loading: false,
            active: '',
            friTab: true,
            depTab: false,
            activeSwitch: 1,
            text: '',
            activeButton: 'center',
            modalVisible: false,
            userData: [],
            weight: null,
            pointWeight: null,
            weightArray: [],
            heightArray: [],
            // stomachSizeArray: [],
            // armSizeArray: [],
            // legSizeArray: [],
            // footSizeArray: [],
            // chestSizeArray: [],
            data: [],
            historyData: {}
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        try {
            if(this.props.history.data && this.props.training[0] ){
                console.log('complete')
                this.setState({loading: false})
                const info = JSON.parse(this.props.training[this.props.training.length-1].data);
                console.log('this is info',info);
                const weightArray = [], heightArray=[];
                this.setState({ weight: info.weight, pointWeight: info.pointWeight });
                for(let i=0 ; i< this.props.training.length; i++) {
                    const detail = JSON.parse(this.props.training[i].data);
                    weightArray.push(detail.weight);
                    heightArray.push(detail.height);
                }
                this.setState({
                    weightArray: weightArray,
                    heightArray: heightArray,
                    data: heightArray
                });
            }
            else{
                this.setState({loading: true})
                AsyncStorage.getItem('token').then((info) => {
                    const newInfo = JSON.parse(info);
                    Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
                    Axios.get('/training/details/show').then(response => {
                        store.dispatch({type: 'USER_TRAINING_FETCHED', payload: response.data.data});
                        // this.setState({ loading: false });
                        const info = JSON.parse(response.data.data[response.data.data.length-1].data);
                        console.log(info);
                        this.setState({ weight: info.weight, pointWeight: info.pointWeight });
                        const weightArray = [], heightArray = [], stomachSizeArray = [], armSizeArray = [], legSizeArray = [], footSizeArray = [], chestSizeArray = [], fatArray = [];

                        for(let i=0 ; i< response.data.data.length; i++) {
                            const detail = JSON.parse(response.data.data[i].data);
                            weightArray.push(detail.weight);
                            heightArray.push(detail.height);
                            // stomachSizeArray.push(detail.stomachSize);
                            // armSizeArray.push(detail.armSize);
                            // legSizeArray.push(detail.legSize);
                            // footSizeArray.push(detail.footSize);
                            // chestSizeArray.push(detail.chestSize);
                        }
                        this.setState({
                            weightArray: weightArray,
                            heightArray: heightArray,
                            data: heightArray
                            // stomachSizeArray: stomachSizeArray,
                            // armSizeArray: armSizeArray,
                            // legSizeArray: legSizeArray,
                            // footSizeArray: footSizeArray,
                            // chestSizeArray: chestSizeArray
                        });
                        // this.setState({data: heightArray});


                    })
                        .catch( (error) => {
                            console.log(error.response);
                        });
                    Axios.get('/app_invoices').then(response => {
                        store.dispatch({type: 'USER_HISTORY_FETCHED', payload: response.data});
                        this.setState({loading: false, historyData: response.data});
                    })
                        .catch((error) => {
                            this.setState({loading: false});
                            console.log(error);
                            Alert.alert('','خطا')

                        });
                });

            }

        }
        catch (error) {
            console.log(error)
            this.setState({loading: false});
            alert('خطایی رخ داده مجددا تلاش نمایید');
        }
    }
    setModalVisible() {
        this.setState({modalVisible: !this.state.modalVisible});
    }
    closeModal() {
        this.setState({modalVisible: false});

    }
    showChart(dir) {
        if(dir === 'left')
            this.setState({activeButton: dir});
        else if(dir === 'center')
            this.setState({activeButton: dir, data: this.state.heightArray});
        else if(dir === 'right')
            this.setState({activeButton: dir,  data: this.state.weightArray});
    }
    render() {
        const {user, history, training} = this.props;
        // const data = [ 50, 10, 40, 95, -4, -24, 85, 91, 35, 53, -53, 24, 50, -20, -80 ];

        if(this.state.loading){
            return (<Loader txtColor="red" color='red' />)
        }
        else return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.top}>
                        <View style={styles.iconContainer}>
                            <TouchableOpacity onPress={() => Actions.push('editProfile')}>
                                <FIcon name="edit" size={23} color="white" style={{paddingRight: 20}} />
                            </TouchableOpacity>
                            <Icon name="bell-o" size={23} color="white" />
                        </View>
                        <Text style={styles.headerTitle}>پروفایل کاربری</Text>
                        <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                            <Icon name="bars" size={20} color="white" />
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.scrollContainer}>
                        <View style={styles.contentTop}>
                            <View style={styles.contentContainer}>
                                <View style={styles.userTopContainer}>
                                    <View style={styles.userContainer}>
                                        <Text style={styles.name}>{this.props.user.fname} {this.props.user.lname}</Text>
                                        <View style={styles.status}>
                                            <View style={styles.statusTextContainer}>
                                                <Text style={styles.statusText}>کاربر فعال</Text>
                                            </View>
                                            <Icon name="mars" size={14} color="white" style={{paddingLeft: 10}}/>
                                        </View>
                                    </View>
                                    <View style={styles.imageContainer}>
                                        <Image style={styles.layer} source={layer} />
                                        {
                                            this.props.user.attachments ?
                                                <Image source={{uri: "http://fitclub.ws/files?uid="+this.props.user.attachments.uid+"&width=100&height=110"}} style={styles.image} />
                                                : <Image source={{uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTQzoL7LYphmQJUakxuGQMoTicLEMGTdb9fhBXoabCzfW44aNaZEA'}} style={styles.image}/>
                                        }
                                    </View>
                                </View>
                                <View style={styles.programInfo}>
                                    <View style={[styles.infoRowContainer, {borderRightColor: 'lightgray', borderRightWidth: 1}]}>
                                        <View style={styles.infoRowTop}>
                                            <Text style={styles.labelWeight}> کیلوگرم</Text>
                                            <Text style={styles.weight}>{this.state.pointWeight}</Text>
                                        </View>
                                        <View style={styles.infoRowBottom}>
                                            <Text style={styles.label}>وزن هدف</Text>
                                            <View style={styles.redCircle} />
                                        </View>
                                    </View>
                                    <View style={styles.infoRowContainer}>
                                        <View style={styles.infoRowTop}>
                                            <Text style={styles.labelWeight}> کیلوگرم</Text>
                                            <Text style={styles.weight}>{this.state.weight}</Text>
                                        </View>
                                        <View style={styles.infoRowBottom}>
                                            <Text style={styles.label}>وزن فعلی</Text>
                                            <View style={styles.blueCircle} />
                                        </View>
                                    </View>
                                    <View style={styles.TimeContainer} />
                                    <View style={styles.relativeContainer}>
                                        <TouchableOpacity onPress={() => this.showChart('left')}>
                                            <View style={[styles.programTimeContainer, {backgroundColor: this.state.activeButton === 'left' ? 'rgb(277, 5, 26)' :  'transparent'}]}>
                                                <Text style={[styles.timeLabel, {color: this.state.activeButton === 'left' ? 'white' : 'gray'}]}>چربی</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.showChart('center')}>
                                            <View style={[styles.programTimeContainer, {backgroundColor: this.state.activeButton === 'center' ? 'rgb(277, 5, 26)' :  'transparent'}]}>
                                                <Text style={[styles.timeLabel, {color: this.state.activeButton === 'center' ? 'white' : 'gray'}]}>سایز</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.showChart('right')}>
                                            <View style={[styles.programTimeContainer, {backgroundColor: this.state.activeButton === 'right' ? 'rgb(277, 5, 26)' :  'transparent'}]}>
                                                <Text style={[styles.timeLabel, {color: this.state.activeButton === 'right' ? 'white' : 'gray'}]}>وزن</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                {
                                    this.state.activeButton === 'left' ? <Text style={{position: 'absolute', zIndex: 9999, bottom: 140,fontFamily: 'IRANSansMobile(FaNum)', fontSize: 12}}> برای تعیین میزان چربی به صورت حضوری مراجعه شود</Text> :
                                        <AreaChart
                                            style={{ height: 200, width: '80%', position: 'absolute',
                                                bottom: 70,
                                                zIndex: 9999 }}
                                            data={ this.state.data }
                                            contentInset={{ top: 30, bottom: 30 }}
                                            curve={ shape.curveNatural }
                                            svg={{ fill: 'rgba(134, 65, 244, 0.8)' }}
                                        >
                                            <Grid/>
                                        </AreaChart>
                                }

                                <TouchableOpacity  onPress={() => this.setState({modalVisible: !this.state.modalVisible })} style={styles.updateBtn}>
                                    <View>
                                        <Text style={styles.buttonText}>بروزرسانی داده ها</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.contentBottom}>
                            <View style={styles.contentBottomRow}>
                                <ProfileOptions src="programs" title="برنامه ها" />
                                <ProfileOptions src="wallet" title="کیف پول" inventoryTitle="موجودی فعلی" data={this.props.history} />
                            </View>
                            <View style={styles.contentBottomRow}>
                                <ProfileOptions src="commonQuestion" title="سوالات متداول" />
                                <ProfileOptions src="tickets" title="تیکت ها" />
                            </View>
                        </View>
                        <ProfileModal
                            closeModal={() => this.closeModal()}
                            modalVisible={this.state.modalVisible}
                            onChange={(visible) => this.setModalVisible(visible)}
                        />
                    </View>
                </ScrollView>
                <View style={styles.footerContainer}>
                    <FooterMenu active="user" />
                </View>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.auth.user,
        history: state.posts.history,
        training: state.posts.training,
    }
}
export default connect(mapStateToProps)(UserProfile);