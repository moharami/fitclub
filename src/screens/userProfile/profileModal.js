import React, { Component } from 'react';
import {
    Modal,
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    TextInput,
    Alert
}
    from 'react-native'
import ProfileUpdatedItem from '../../components/profileUpdatedItem'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import LineGauge from 'react-native-line-gauge'
import Axios from 'axios'
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://fitclub.ws';
Axios.defaults.baseURL = url;

class ProfileModal extends Component {
    state = {
        modalVisible: false,
        value: '',
        lineAmount: 1,
        text: ''
    };
    handleUpdate() {
        let midterm = {"weight": 0, "description": ""};
        midterm.weight = this.state.value;
        midterm.description = this.state.text;
        console.log('midterm', midterm);

        try {
            Axios.post('/training/details/midterm', {
                midterm: JSON.stringify(midterm),
            }).then(response=> {
                Alert.alert('','پروفایل شما بروزرسانی شد');

            })
            .catch((error) => {
                    Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    this.setState({loading: false});

                }
            );
        }
        catch (error) {
            console.log(error)
            Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
            this.setState({loading: false});

        }
    }
    render() {
        return (
            <View style = {styles.container}>
                <Modal animationType = {"fade"} transparent = {true}
                       visible = {this.props.modalVisible}
                       onRequestClose = {() => this.props.onChange(false)}
                       onBackdropPress= {() => this.props.onChange(!this.state.modalVisible)}
                >
                    <View style = {styles.modal} >

                        <View style={styles.box}>
                            {/*<ScrollView horizontal={true} showsHorizontalScrollIndicator={false} >*/}
                               {/*<View style={styles.innerr}>*/}
                                   {/*<ProfileUpdatedItem />*/}
                                   {/*<ProfileUpdatedItem />*/}
                                   {/*<ProfileUpdatedItem />*/}
                                   {/*<ProfileUpdatedItem />*/}
                                   {/*<ProfileUpdatedItem />*/}
                                   {/*<ProfileUpdatedItem />*/}
                                   {/*<ProfileUpdatedItem />*/}
                                   {/*<ProfileUpdatedItem />*/}
                                   {/*<ProfileUpdatedItem />*/}
                                   {/*<ProfileUpdatedItem />*/}
                                   {/*<ProfileUpdatedItem />*/}
                               {/*</View>*/}
                            {/*</ScrollView>*/}
                            {/*<View style={styles.programGuidContainer}>*/}
                                {/*<View style={styles.programGuid}>*/}
                                    {/*<Text style={styles.programGuidText}>قبلی</Text>*/}
                                    {/*<Text style={{color: 'rgb(100,100,100)', fontWeight: 'bold' }}>7.2%</Text>*/}
                                {/*</View>*/}
                                {/*<View style={styles.programGuid}>*/}
                                    {/*<Text style={[styles.programGuidText, {fontSize: 18}]}>امروز</Text>*/}
                                    {/*<Text style={{color: 'red', fontSize: 20, fontWeight: 'bold'}}>2.6%</Text>*/}
                                {/*</View>*/}
                                {/*<View style={styles.programGuid}>*/}
                                    {/*<Text style={styles.programGuidText}>مانده به هدف</Text>*/}
                                    {/*<Text style={{color: 'rgb(32, 193, 188)', fontWeight: 'bold'}}>4.1%</Text>*/}
                                {/*</View>*/}
                            {/*</View>*/}
                            <View style={styles.weightContainer}>
                                <Text style={styles.weightText}>(کیلوگرم)</Text>
                                <Text style={styles.weight}>وزن </Text>
                            </View>
                            <Text style={styles.weightAmount}>{this.state.lineAmount}</Text>
                            <LineGauge min={0} max={100} value={42} onChange={(value) => this.setState({lineAmount: value})} />
                            <TextInput
                                multiline = {true}
                                numberOfLines = {3}
                                value={this.state.text}
                                onChangeText={(text) => this.setState({text: text})}
                                placeholderTextColor={'rgb(142, 142, 142)'}
                                underlineColorAndroid='transparent'
                                placeholder="توضیحات..."
                                style={{
                                    // height: 45,
                                    paddingRight: 15,
                                    width: '90%',
                                    fontSize: 18,
                                    color: 'rgb(142, 142, 142)',
                                    textAlign:'right',
                                    borderWidth: 1,
                                    borderColor: 'rgb(236, 236, 236)',
                                    borderRadius: 5,
                                    marginTop: 30
                                }}
                            />
                            <TouchableOpacity  onPress={() => this.handleUpdate()} style={styles.updateBtn}>
                                <View>
                                    <Text style={styles.buttonText}>بروزرسانی داده ها</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity onPress={() => this.props.closeModal()} style={styles.closeContainer}>
                            <Icon name="close" size={20} color={ "white"} />
                        </TouchableOpacity>
                    </View>
                </Modal>
            </View>
        )
    }
}
export default ProfileModal

const styles = StyleSheet.create ({
    container: {
        alignItems: 'center',
        backgroundColor: 'white',
        padding: 100,
        position: 'absolute',
        bottom: -200
    },
    modal: {
        flexGrow: 1,
        justifyContent: 'flex-start',
        // flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,.5)',
        paddingRight: 15,
        paddingLeft: 15
    },
    text: {
        color: '#3f2949',
        marginTop: 10
    },
    box: {
        width: '100%',
        // height: 120,
        // flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,

    },
    innerr: {
        marginTop: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    programGuidContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        // paddingLeft: 35,
        paddingTop: 10
    },
    programGuid: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    programGuidText: {
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    closeContainer: {
        width: 60,
        height: 60,
        borderRadius: 60,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0, .3)',
        padding: 20,
        marginTop: 30
    },
    weightContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 20
    },
    weight: {
        fontSize: 16,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    weightText: {
        fontSize: 12,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    weightAmount: {
        fontSize: 35,
        color: 'gray',
        // paddingTop: 10,
        fontWeight: 'bold'
    },
    updateBtn: {
        width: '57%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgb(32, 193, 188)',
        padding: 15,
        borderRadius: 30,

        marginTop: 30,
        marginBottom: 30,
    },
    buttonText: {
        fontSize: 15,
        color: 'white',
        fontWeight: 'bold',
        fontFamily: 'IRANSansMobile(FaNum)',
    },
});