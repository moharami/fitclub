
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({

    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(251, 251, 251)',
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999
    },
    top: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'rgb(252, 86, 86)',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        paddingRight: 15,
        paddingLeft: 15
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    headerTitle: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 18,
        paddingRight: 50
    },
    scroll: {
        paddingTop: 50,
        flex: 1
        // paddingBottom: 200,
    },
    scrollContainer: {
        // paddingTop: 10,
        paddingBottom: 100,
    },
    absolutedHeader: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 10
    },
    absolutedHeaderText: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14,
    },
    contentTop: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 10,
    },
    contentContainer: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
        paddingBottom: 320,
        zIndex: 0
    },
    userTopContainer: {
        flexDirection: 'row',
        width: '100%',
        paddingBottom: 120,
        paddingTop: 20,
        alignItems: 'center',
        justifyContent: 'flex-end',
        backgroundColor: 'rgb(252, 86, 86)'
    },
    walletRow: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    walletAmount: {
        color: 'white',
        fontSize: 40,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    walletText: {
        color: 'white',
        // fontSize: 40,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingBottom: 20
    },
    imageContainer: {
        position: 'relative',
        width: 100,
        height: 110,
        zIndex: 0,
        marginRight: 20,
        marginLeft: 15
    },
    layer: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: 100,
        height: 110,
        zIndex: 9999
    },
    image: {
        position: 'absolute',
        top: 0,
        left: 0,
        // width: 120,
        // height: 130,
        width: 100,
        height: 110,
        zIndex: 1,
        resizeMode: 'contain'

    },
    rialText: {
        color: 'white',
        // fontSize: 25,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingTop: 10,
        paddingRight: 5
    },
    walletInfoContainer: {
        position: 'relative',
        paddingBottom: 50,
        zIndex: 9999
    },
    programInfo: {
        flexDirection: 'row',
        width: '90%',
        backgroundColor: 'white',
        position: 'absolute',
        bottom: 20,
        zIndex: 999,
        paddingBottom: 300,
        borderColor: 'lightgray',
        borderWidth: 1,
        paddingTop: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    infoRowTop: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'center',
        paddingBottom: 50
    },
    infoRowBottom: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'center',
        // backgroundColor: 'red',
        height: 30,
        position: 'absolute',
        top: 23,
        // left: '22%',
        zIndex: 9999
    },
    label: {
        color: 'rgb(150, 150, 150)',
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingRight: 10,
        paddingBottom: 40

    },
    infoRowContainer: {
        width: '50%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRightColor: 'white',
        borderRightWidth: 1,
        // backgroundColor: 'red',
        height: 70
    },
    blueCircle: {
        width: 10,
        height: 10,
        borderRadius: 10,
        backgroundColor: 'rgb(0, 168, 228)',
        marginTop: 8
    },
    redCircle: {
        width: 10,
        height: 10,
        borderRadius: 10,
        backgroundColor: 'rgb(250, 113, 67)',
        marginTop: 8

    },
    weight: {
        fontSize: 25,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    labelWeight: {
        fontSize: 10,
        color: 'rgb(150, 150, 150)',
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingBottom: 5
    },
    TimeContainer: {
        width: '92%',
        height: 25,
        borderRadius: 20,
        backgroundColor: 'rgb(233, 234, 238)',
        position: 'absolute',
        bottom: 180,
        zIndex: 999,
        marginBottom: 80,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    relativeContainer: {
        width: '80%',
        height: 40,
        borderRadius: 20,
        backgroundColor: 'transparent',
        position: 'absolute',
        bottom: 180,
        zIndex: 1000,
        marginBottom: 73,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    programTimeContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        // padding: 20,
        // paddingRight: 30,
        // paddingLeft: 30,
        borderRadius: 30,
        // position: 'absolute',
        // bottom: 0,
        // zIndex: 1230,
        // marginBottom: 80,
    },
    timeLabel: {
        paddingTop:10,
        paddingRight:40,
        paddingLeft:40,
        paddingBottom:10,
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    updateBtn: {
        width: '56%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgb(32, 193, 188)',
        padding: 15,
        borderRadius: 30,
        position: 'absolute',
        bottom: 0,
        left: '22%',
        zIndex: 10000
    },
    buttonText: {
        fontSize: 15,
        color: 'white',
        fontWeight: 'bold',
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    footerContainer: {
        position: 'absolute',
        flex: 1,
        bottom: 0,
        right: 0,
        left: 0
    },
    historyText: {
        color: 'black',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    name: {
        fontSize: 17,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    status: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    statusTextContainer: {
        borderRightColor: 'white',
        borderRightWidth: 1,
        paddingRight: 10
    },
    statusText: {
        fontSize: 12,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    contentBottom: {
        padding: 20
    },
    contentBottomRow: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
    }
});
