
import React, {Component} from 'react';
import {View, ScrollView, Text, StatusBar, Image, TouchableOpacity, BackHandler} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/Feather';
import LinearGradient from 'react-native-linear-gradient'
import FooterMenu from '../../components/footerMenu'
import practice from "../../assets/practice.png";
import Slider from "react-native-slider";
import final from '../../assets/finalConfirm.png';
import {Actions} from 'react-native-router-flux';

class FinalConfirm extends Component {
    constructor(props){
        super(props);
        this.state = {
            value: .5,
            activeSwitch: 1,
            color: 'rgb(227, 5, 26)',
            color2: 'rgb(227, 5, 26)',
            text: '',
            text2: '',
            activeImage: '',
            activeButton: ''
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    paymentMethod() {
        this.setState({activeButton: 'right'})
        Actions.payment({data: this.props.data, titleName: this.props.titleName, price: this.props.price});
    }
    programShow() {
        this.setState({activeButton: 'left'})
        Actions.push('UserPrograms')
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                        <TouchableOpacity onPress={() => Actions.push('purpose')} style={styles.iconContainer}>
                            <Icon name="arrow-left" size={25} color="white" />
                        </TouchableOpacity>
                        <View style={styles.headerTitleContainer}>
                            <Text style={styles.headerTitle}>تایید نهایی</Text>
                        </View>
                    </LinearGradient>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <Image source={final} />
                        <View style={styles.content}>
                            <Text style={styles.recordText}>برنامه شما با موفقیت ثبت شد</Text>
                            <Text style={styles.describText}>شما می توانید با مراجعه به صفحه برنامه های من از هزینه برنامه و جزئیات سفارش خود مطلع شوید.</Text>
                        </View>
                        <View style={styles.paymentButtonContainer}>
                            <TouchableOpacity onPress={() => this.programShow()} style={[styles.paymentButton, {backgroundColor: this.state.activeButton === 'left' ? 'rgb(32, 193, 189)' : 'rgb(233, 234, 237)', marginRight: 5 }]}>
                                <View>
                                    <Text style={[styles.paymentText, {color: this.state.activeButton === 'left' ? 'white' : 'gray', fontFamily: this.state.activeButton === 'left' ? 'IRANSansMobile(FaNum)' : 'IRANSansMobile(FaNum)' }]}>برنامه های من</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.paymentMethod()} style={[styles.paymentButton, {backgroundColor: this.state.activeButton === 'right' ? 'rgb(32, 193, 189)' : 'rgb(233, 234, 237)', marginLeft: 5 }]}>
                                <View>
                                    <Text style={[styles.paymentText, {color: this.state.activeButton === 'right' ? 'white' : 'gray', fontFamily: this.state.activeButton === 'right' ? 'IRANSansMobile(FaNum)' : 'IRANSansMobile(FaNum)' }]}>پرداخت</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
                <View style={styles.footerContainer}>
                    <FooterMenu />
                </View>
            </View>
        );
    }
}
export default FinalConfirm;

