
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({

    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(251, 251, 251)',
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999
    },
    top: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        paddingRight: 15,
        paddingLeft: 15
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    headerTitle: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 18,
        paddingRight: 50
    },
    scroll: {
        paddingTop: 50,
        flex: 1
        // paddingBottom: 200,
    },
    scrollContainer: {
        // paddingTop: 10,
        paddingBottom: 100,
    },
    absolutedHeader: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 10
    },
    absolutedHeaderText: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14,
    },
    contentTop: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 10,
    },
    contentContainer: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
        paddingBottom: 50,
        zIndex: 0,
    },
    walletAmountContainer: {
        width: '100%',
        paddingBottom: 100,
        paddingTop: 40,
        alignItems: 'center',
        justifyContent: 'center',
    },
    walletRow: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    walletAmount: {
        color: 'white',
        fontSize: 40,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    walletText: {
        color: 'white',
        // fontSize: 40,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingBottom: 20
    },
    rialText: {
        color: 'white',
        // fontSize: 25,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingTop: 10,
        paddingRight: 5
    },
    walletInfoContainer: {
        position: 'relative',
        paddingBottom: 50,
        zIndex: 9999
    },
    walletInfo: {
       width: '90%',
        backgroundColor: 'white',
        position: 'absolute',
        bottom: 20,
        zIndex: 999,
        paddingBottom: 25,
        borderColor: 'lightgray',
        borderWidth: 1
    },
    infoRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 10

    },
    label: {
        color: 'black',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    value: {
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    sharjBtn: {
        width: '56%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgb(32, 193, 188)',
        padding: 15,
        borderRadius: 30,
        position: 'absolute',
        bottom: 0,
        left: '22%',
        zIndex: 9999
    },
    buttonText: {
        fontSize: 15,
        color: 'white',
        fontWeight: 'bold',
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    footerContainer: {
        position: 'absolute',
        flex: 1,
        bottom: 0,
        right: 0,
        left: 0
    },
    historyText: {
        color: 'black',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    contentBottom: {
        backgroundColor: 'white',
        // alignItems: 'center',
        // justifyContent: 'center',
        padding: 10,
        borderColor: 'lightgray',
        borderWidth: 1,
        margin: 15

    }
});
