import React, {Component} from 'react';
import {ScrollView, View, TouchableOpacity, Text, Dimensions, AsyncStorage, BackHandler} from 'react-native';
import styles from './styles'
import LinearGradient from 'react-native-linear-gradient'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {Actions} from 'react-native-router-flux'
import FooterMenu from '../../components/footerMenu'
import History from '../../components/history'
import InputModal from './inputModal'
import Axios from 'axios';
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://fitclub.ws';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import {store} from '../../config/store';
import {connect} from 'react-redux';

class Wallet extends Component {
    constructor(props){
        super(props);
        this.state = {
            data: {},
            loading: false,
            active: '',
            friTab: true,
            depTab: false,
            activeSwitch: 1,
            text: '',
            activeButton: 'right',
            absoluteHeader: false,
            windowHeight: 0,
            modalVisible: false,
            invoices: []
        };
        this.onBackPress = this.onBackPress.bind(this);

    }
    scrollEnabled(e) {
        const windowHeight = Dimensions.get('window').height,
            height = e.nativeEvent.contentSize.height,
            offset = e.nativeEvent.contentOffset.y;
        this.setState({ windowHeight: offset});

        // alert(offset)
        if(offset >= 150){
            this.setState({absoluteHeader: true});
        }
        else {
            this.setState({absoluteHeader: false});
        }
    }
    setModalVisible() {
        this.setState({modalVisible: !this.state.modalVisible});
    }
    closeModal() {
        this.setState({modalVisible: false});

    }
    sharjMethod() {
        // this.setState({loading: true});
        this.setState({
            modalVisible: !this.state.modalVisible
        })
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        // const {history} = this.props;
        if(this.state.loading)
            return (<Loader txtColor="red" color='red' />);
        else return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.top}>
                        <View style={styles.iconContainer}>
                            <TouchableOpacity onPress={() => Actions.push('UserProfile')}>
                                <FIcon name="arrow-left" size={27} color="white" style={{paddingRight: 20}} />
                            </TouchableOpacity>
                            <Icon name="bell-o" size={23} color="white" />
                        </View>
                        <Text style={styles.headerTitle}>کیف پول من</Text>
                        <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                            <Icon name="bars" size={20} color="white" />
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
                <ScrollView style={styles.scroll} onScroll={ (e) => this.scrollEnabled(e)}>
                    <View style={styles.scrollContainer}>
                        {this.state.absoluteHeader ?
                            <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={[styles.absolutedHeader, {position: 'absolute', top: this.state.windowHeight, zIndex: 9999 }]}>
                                <Text style={styles.absolutedHeaderText}> {this.props.data.credit} ریال</Text>
                                <Text style={styles.absolutedHeaderText}>موجودی کیف پول</Text>
                            </LinearGradient>
                            :
                            <View style={styles.contentTop}>
                                <View style={styles.contentContainer}>
                                    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(233, 50, 52)', 'rgb(254, 88, 88)']} style={styles.walletAmountContainer}>
                                        <View style={styles.walletRow}>
                                            <Text style={styles.rialText}>ریال</Text>
                                            <Text style={styles.walletAmount}>{this.props.data.credit}</Text>
                                        </View>
                                        <Text style={styles.walletText}>موجودی کیف پول</Text>
                                    </LinearGradient>
                                    <View style={styles.walletInfo}>
                                        <View style={[styles.infoRow, {borderBottomColor: 'lightgray', borderBottomWidth: 1}]}>
                                            <Text style={[styles.value,{color: 'rgb(32, 193, 188)'}]}>{this.props.data.charged} ریال</Text>
                                            <Text style={styles.label}>مجموع شارژ کیف پول </Text>
                                        </View>
                                        <View style={styles.infoRow}>
                                            <Text style={[styles.value,{color: 'red'}]}>  {this.props.data.discharged} ریال</Text>
                                            <Text style={styles.label}>مجموع پرداختی های آنلاین</Text>
                                        </View>
                                    </View>
                                    <TouchableOpacity onPress={() => this.sharjMethod()} style={styles.sharjBtn}>
                                        <View>
                                            <Text style={styles.buttonText}>شارژ کیف پول</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        }
                        <View style={styles.contentBottom}>
                            <Text style={styles.historyText}>تاریخچه</Text>
                            {/*{*/}
                                {/*this.state.data.invoices ? this.state.data.invoices.map((item)=> <History border icon="arrow-up" iconColor="rgb(32, 193, 188)" item={item}/>) : null*/}
                            {/*}*/}
                            {
                                this.props.data.invoices.length !== 0 ? this.props.data.invoices.map((item)=> <History border iconColor="rgb(32, 193, 188)" item={item} key={item.id} />) :
                                    <Text style={{color: 'red', textAlign: 'center', padding: 25}}>تاریخچه ای برای نمایش وجود ندارد</Text>

                            }
                            <InputModal
                                closeModal={() => this.closeModal()}
                                modalVisible={this.state.modalVisible}
                                onChange={(visible) => this.setModalVisible(visible)}
                            />
                        </View>
                    </View>
                </ScrollView>
                <View style={styles.footerContainer}>
                    <FooterMenu />
                </View>
            </View>
        );
    }
}
export default Wallet;