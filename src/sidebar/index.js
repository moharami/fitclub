import React, {Component} from 'react';
import {Text, View,Image, TouchableOpacity, ScrollView, AsyncStorage} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import MIcon from 'react-native-vector-icons/dist/MaterialIcons';
import {Actions} from 'react-native-router-flux'
import profile from '../assets/profile.png'
import MenuItem from '../components/sidebarMenuItem'
import Axios from 'axios'
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
export const url = 'http://fitclub.ws';
Axios.defaults.baseURL = url;
import Loader from '../components/loader'

export default class Sidebar extends React.Component {
    constructor(){
        super();
        this.state = {
            loading: true,
            user: {},
            credit: {}
        };
    }
    logoutMethod() {
        AsyncStorage.removeItem('token');
        Actions.push('Login')
    }
    componentWillMount() {
        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);
                Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
                console.log('user token', newInfo.token);
                Axios.get('/core/show').then(response => {
                    console.log(response);
                    this.setState({user: response.data})
                    console.log('user', response.data)

                    Axios.get('/financial/wallet').then(response => {
                        this.setState({credit: response.data, loading: false})
                        console.log('credit iiiiinfo', response.data)
                    })
                    .catch((error) =>{
                            console.log(error.response);
                        });
                })
                .catch( (error) =>{
                    console.log(error.response);
                });
            }
        });
    }
    render(){
        if(this.state.loading){
            return (<Loader txtColor="red" color='red' />)
        }
        else return (
            <ScrollView style={styles.scroll}>
                <View style={styles.container}>
                    <View style={styles.bottom}>
                        <View style={styles.header}>
                            <View style={styles.top}>
                                <TouchableOpacity onPress={() => Actions.drawerClose()} style={styles.closeIcon}>
                                    <MIcon name="close" size={28} color="red" />
                                </TouchableOpacity>
                                <View style={styles.imageContainer}>
                                    <Image style={styles.layer} source={profile} />
                                    {
                                        this.state.user.data.attachments ?
                                          <Image source={{uri: "http://fitclub.ws/files?uid="+this.state.user.data.attachments.uid+"&width=100&height=110"}} style={styles.image} />
                                        : <Image source={{uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTQzoL7LYphmQJUakxuGQMoTicLEMGTdb9fhBXoabCzfW44aNaZEA'}} style={styles.image}/>
                                    }
                                </View>
                            </View>
                            <View style={styles.iconContainer}>
                                <TouchableOpacity onPress={() => this.logoutMethod()}>
                                    <Icon name="power-off" size={30} color="rgb(181, 181, 181)" />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => Actions.push('editProfile')}>
                                    <Icon name="cog" size={30} color="rgb(181, 181, 181)" />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.userContainer}>
                                <Text style={styles.userText}>کاربر عادی</Text>
                            </View>
                            <Text style={styles.name}>{this.state.user.data.fname} {this.state.user.data.lname}</Text>
                            <View style={styles.cash}>
                                <Icon name="plus-circle" size={30} color="rgb(36, 191, 183)" style={{paddingLeft: 2 }} />
                                <View style={styles.right}>
                                    <Text style={styles.cashColorText}>{this.state.credit.data.credit} ریال</Text>
                                    <Text style={styles.cashText}>موجودی کیف پول: </Text>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={styles.categories}>
                        <TouchableOpacity onPress={() => Actions.push('home')}>
                            <MenuItem title="خانه" icon="home" border />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => Actions.push('UserPrograms')}>
                            <MenuItem title="برنامه های من" icon="regim" border />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => Actions.push('commonQuestion')}>
                            <MenuItem title="سوالات متداول" icon="question" border />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => Actions.push('tickets')}>
                            <MenuItem title="تیکت ها" icon="ticket" border />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => Actions.push('ContactUs')}>
                            <MenuItem title="تماس با ما" icon="contact" />
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

