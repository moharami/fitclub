import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    scroll: {

    },
    container: {
        backgroundColor: 'white',
        flex: 1
    },
    bodyContainer: {
        alignItems: 'flex-start',
        justifyContent: 'space-between'
    },
    imageContainer: {
        position: 'relative',
        width: 120,
        height: 130,
        zIndex: 0
    },
    layer: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: 120,
        height: 130,
        zIndex: 9999
    },
    image: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: 120,
        height: 130,
        resizeMode: 'contain',
        borderRadius: 70,
        zIndex: 1
    },
    header: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerTitle: {
        color: 'white',
        fontSize: 27
    },
    headerBody: {
        color: 'white',
        opacity: .7,
        fontSize: 18
    },
    top: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        paddingTop: 40,
        paddingLeft: 10
    },
    closeIcon: {
        paddingRight: '22%'
    },
    userContainer: {
        backgroundColor: 'rgb(233, 233, 233)',
        borderRadius: 15,
        paddingRight: 15,
        paddingLeft: 15,
        paddingTop: 3,
        paddingBottom: 3
    },
    userText: {
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    name: {
        fontSize: 18,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingTop: 10,
        paddingBottom: 10
    },
    cash: {
        backgroundColor: 'rgb(233, 249, 248)',
        borderRadius: 20,
        paddingRight: 10,
        paddingTop: 1,
        paddingBottom: 1,
        borderColor: 'rgb(36, 191, 183)',
        borderWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '75%'
    },
    cashText: {
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12
    },
    cashColorText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'rgb(36, 191, 183)',
        paddingRight: 5
    },
    right: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '33%',
        paddingTop: 30,
        paddingBottom: 30
    },
    left: {
        padding: 30
    },
    body: {
        paddingRight: 50,
        paddingTop: 50
    },
    categories: {
        paddingTop: 15,
        paddingRight: 30,
        paddingLeft: 30,
    }

});
