
const INITIAL_STATE = {
    posts: [],
    categories: [],
    categoryDetail: [],
    programs: [],
    history: [],
    training: [],
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'USER_POSTS_FETCHED':
            return { ...state, posts: action.payload };
        case 'USER_CATEGORIES_FETCHED':
            return { ...state, categories: action.payload };
        case 'USER_HISTORY_FETCHED':
            return { ...state, history: action.payload };
        case 'NEW_POSTS_FETCHED':
            const newArray = [...state.posts.blog.data, ...action.payload];
            // const newArray = state.posts.data.concat(action.payload.data);
            let newPosts = state.posts;
            newPosts.blog.data = newArray;
            return {
                ...state,
                posts: newPosts,
            };
        case 'DETAIL_POST_FETCHED':
            return { ...state, categoryDetail: action.payload };
        case 'NEW_PROGRAMS_FETCHED':
            return { ...state, programs: action.payload };
        case 'NEW_CATEGORIES_FETCHED':
            const newArray2 = [...state.categories, ...action.payload];
            console.log('newArray2', newArray2)
            console.log('action.payload', action.payload)
            console.log('cate', state.categories)
            let newCats = state.categories;
            newCats = newArray2;
            // const newArray = state.posts.data.concat(action.payload.data);
            return {
                ...state,
                categories: newCats,
            };
        case 'USER_TRAINING_FETCHED':
            return { ...state, training: action.payload };
        default:
            return state;
    }
};
