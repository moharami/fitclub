import React, {Component} from 'react';
import {Text, View, Image, ScrollView, TouchableOpacity} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/MaterialIcons';
import ProgramStep from '../../components/programStep'
import RecordedInfo from '../../components/recordedInfo'
import Slider from "react-native-slider";
import {Actions} from 'react-native-router-flux'
import moment from 'moment';
import 'moment/locale/fa';

class ProgramType extends Component {
    constructor(props){
        super(props);
        this.state = {
            value: .5,
            showDetail: false

        };
    }
    render() {
        console.log('itttem',this.props.item)
        let statusText = '', statusColor='';
        if(this.props.item.detail.active === 0){
            statusText = 'منقضی شده';
            statusColor = 'red'

        }
        else if(this.props.item.detail.active === 1){
            statusText = 'در حال دریافت برنامه';
            statusColor = 'yellow'

        }
        else if(this.props.item.detail.active === 2){
            statusText = 'آماده';
            statusColor = 'green'
        }
        let programType = '';
        if(this.props.item.type === 'diet'){
            programType = 'رژیم';
        }
        else if(this.props.item.type === 'exercise'){
            programType = 'رژیم و تمرین';
        }
        else if(this.props.item.type === 'complementary'){
            programType = 'رژیم و تمرین و مکمل';
        }
        const info = JSON.parse(this.props.item.detail.data);
        let infoarr = [];
        infoarr['armSize'] = info.armSize;
        infoarr['chestSize'] = info.chestSize;
        infoarr['footSize'] = info.footSize;
        infoarr['height'] = info.height;
        infoarr['stomachSize'] = info.stomachSize;
        infoarr['weight'] = info.weight;

        return (
            <View>
            {/*<TouchableOpacity onPress={() => Actions.program({item: this.props.item})}>*/}
                <View style={styles.container}>
                    <View style={styles.topLeft}>
                        {/*{this.props.iconTop ?*/}
                            {/*<Icon name="keyboard-arrow-up" size={20} color="gray" />*/}
                        {/*:  <Icon name="keyboard-arrow-down" size={20} color="gray" />*/}
                        {/*}*/}
                        <TouchableOpacity onPress={() => this.setState({showDetail: !this.state.showDetail})}>
                            <Icon name={this.state.showDetail ? 'keyboard-arrow-up' : 'keyboard-arrow-down'} size={20} color="gray" />
                        </TouchableOpacity>
                        <Text style={[styles.payment, {color: statusColor}]}>{statusText}</Text>
                    </View>
                    <View style={styles.topRight}>
                        <Text style={styles.type}>{this.props.item.title}</Text>
                        <Text style={styles.lastUpdate}>آخرین بروزرسانی : {moment(this.props.item.updated_at).format('L')}</Text>
                    </View>
                </View>
                {this.state.showDetail ?
                    <View>
                        <View style={styles.statusContainer}>
                            <Text style={styles.statusText}>وضعیت برنامه</Text>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{marginBottom: 20, transform: [{ scaleX: -1}]}}>
                                <View style={styles.slideContainers}>
                                    <ProgramStep active={this.props.item.detail.active === 0} label="منقضی شده" />
                                    <ProgramStep active={this.props.item.detail.active === 1} label="بررسی توسط مربی"/>
                                    <ProgramStep active={this.props.item.detail.active === 2} label="برنامه آماده" />
                                </View>
                            </ScrollView>
                            <Slider
                                thumbTintColor="rgb(32, 193, 188)"
                                minimumTrackTintColor="rgb(32, 193, 188)"
                                value={this.state.value}
                                thumbStyle={{borderWidth: 1, borderColor: 'rgb(32, 193, 188)', backgroundColor: 'white', width: 15, height: 15}}
                                maximumTrackTintColor="red"
                                trackStyle={{width: '100%', height: 15, borderRadius: 15, backgroundColor: 'lightgray', transform: [{ scaleX: -1}]}}
                                style={{width: '100%', height: 15, borderRadius: 15, backgroundColor: 'rgb(32, 193, 188)', transform: [{ scaleX: -1}] }}
                                onValueChange={value => this.setState({value: value })}
                            />
                        </View>
                        <View style={styles.infoContainer}>
                            <Text style={styles.statusText}>اطلاعات برنامه</Text>
                            {/*<View style={styles.regimContainer}>*/}
                                {/*<View style={styles.left}>*/}
                                    {/*<Text style={styles.value}>دو ماه</Text>*/}
                                    {/*<Text style={styles.label}>طول برنامه: </Text>*/}
                                {/*</View>*/}
                                {/*<View style={styles.right}>*/}
                                    {/*<Text style={styles.value}>FC-4536998</Text>*/}
                                    {/*<Text style={styles.label}>کد برنامه: </Text>*/}
                                {/*</View>*/}
                            {/*</View>*/}
                            <View style={styles.regimContainer}>
                                {/*<View style={styles.left}>*/}
                                    {/*<Text style={styles.redValue}>750000 ریال</Text>*/}
                                    {/*<Text style={styles.label}>قیمت دوره: </Text>*/}
                                {/*</View>*/}
                                <View style={styles.left}>
                                    <Text style={styles.value}>{programType}</Text>
                                    <Text style={styles.label}>نوع برنامه: </Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.recordedContainer}>
                            <Text style={styles.statusText}>اطلاعات ثبت شده</Text>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ transform: [{ scaleX: -1}]}}>
                                <View style={styles.slideContainers}>
                                    {/*{*/}
                                        {/*infoarr.map((item, index) => console.log('atrrrrrrrrrr', item)*/}
                                        {/*)*/}
                                    {/*}*/}
                                    <RecordedInfo backgroundColor="rgb(89, 218, 164)" borderColor="rgb(69, 198, 144)"  item={infoarr['armSize']} name= 'سایز بازو' />
                                    <RecordedInfo backgroundColor="rgb(89, 218, 164)" borderColor="rgb(69, 198, 144)" item={infoarr['chestSize']} name= 'دور سینه' />
                                    <RecordedInfo backgroundColor="rgb(89, 218, 164)" borderColor="rgb(69, 198, 144)" item={infoarr['footSize']} name= 'دور ساق پا' />
                                    <RecordedInfo backgroundColor="rgb(89, 218, 164)" borderColor="rgb(69, 198, 144)" item={infoarr['height']} name= 'قد' />
                                    <RecordedInfo backgroundColor="rgb(89, 218, 164)" borderColor="rgb(69, 198, 144)" item={infoarr['stomachSize']} name= 'دور شکم' />
                                    <RecordedInfo backgroundColor="rgb(89, 218, 164)" borderColor="rgb(69, 198, 144)" item={infoarr['weight']} name= 'وزن' />
                                </View>
                            </ScrollView>

                            {
                                this.props.item.detail.active === 2 ?
                                    (this.props.item.type === 'exercise' ?
                                    <TouchableOpacity onPress={() => Actions.program({item: this.props.item, pName: this.props.pName})} style={styles.payButton}>
                                        <Text style={styles.buttonText}>مشاهده برنامه</Text>
                                    </TouchableOpacity> :
                                    <TouchableOpacity onPress={() => Actions.foodProgram({item: this.props.item, pName: this.props.pName})} style={styles.payButton}>
                                        <Text style={styles.buttonText}>مشاهده برنامه</Text>
                                    </TouchableOpacity> )
                                : null
                            }

                        </View>
                    </View>: null
                }
            </View>

        );
    }
}
export default ProgramType;