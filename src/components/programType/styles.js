import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    payment: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 16,
        paddingLeft: 15
    },
    type: {
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 18,
        textAlign: 'right'
    },
    lastUpdate: {
        color: 'lightgray',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 13,
    },
    topLeft: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    topRight: {

    },
    statusContainer: {
        margin: 10,
        borderBottomColor: 'rgb(237, 237, 237)',
        borderBottomWidth: 1,
        borderTopColor: 'rgb(237, 237, 237)',
        borderTopWidth: 1,
        paddingBottom: 20
    },
    infoContainer: {
        margin: 10,
        borderBottomColor: 'rgb(237, 237, 237)',
        borderBottomWidth: 1,
        paddingBottom: 20,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    statusText: {
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 18,
        // paddingRight: 10,
        paddingBottom: 10,
        alignSelf: 'flex-end'
    },
    regimContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginBottom: 8,
    },
    left: {
        flexDirection: 'row',
        alignSelf: 'flex-end'
    },
    right: {
        flexDirection: 'row'
    },
    label: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14
    },
    value: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black',
        fontSize: 14
    },
    redValue: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'red',
        fontSize: 15
    },
    recordedContainer: {
        margin: 10,
        paddingBottom: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    slideContainers: {
        flexDirection: 'row',
        transform: [
            {rotateY: '180deg'},
        ]
    },
    payButton: {
        width: '60%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgb(32, 193, 189)',
        padding: 15,
        borderRadius: 25,
        // paddingRight: 80,
        // paddingLeft: 80,
        marginTop: 30
    },
    buttonText: {
        fontSize: 15,
        color: 'white',
        fontWeight: 'bold',
        fontFamily: 'IRANSansMobile(FaNum)',
    },
});
