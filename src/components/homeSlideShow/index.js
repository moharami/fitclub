import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ImageBackground
} from 'react-native';

import Swiper from 'react-native-swiper';

const styles = StyleSheet.create({
    wrapper: {
        width: '100%',
        height: 170,
        // marginLeft: '5%'
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },
    mainImage:{
        width: '100%',
        height: 170,
        // marginRight: 10,
        overflow: 'hidden',
        alignItems: "flex-end",
        justifyContent: "flex-end",
        marginRight: 30,

    },
    imageText: {
        backgroundColor: "transparent",
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: "white",
        textAlign: 'center'
    }
});

export default class HomeSlideShow extends Component {

    render(){
        return (
            <Swiper style={styles.wrapper} showsButtons={false} showsPagination={false} removeClippedSubviews={false} autoplay={true} loop={true}>
                {
                    this.props.slider[0].subs.map((item) => {return <ImageBackground source={{uri: "http://fitclub.ws/files?uid="+ item.attachments[0].uid +"&width=300&height=100"}} style={styles.mainImage} key={item.id}>
                         <View style={{backgroundColor:'rgba(0,0,0,.4)',
                                height: 30,
                                // width: "70%",
                                alignItems: "center",
                                justifyContent: "center",
                                paddingRight: 20,
                                paddingLeft: 20,
                                marginBottom: 10
                            }}>
                                <Text style={styles.imageText}>{item.title}</Text>
                            </View>
                        </ImageBackground>
                    })
                }
            </Swiper>
        );
    }
}

