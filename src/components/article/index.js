import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import NumberedSlideshow from '../../components/numberedSlideshow'
import VideoPlayer from 'react-native-video-controls';
import moment from 'moment';
import 'moment/locale/fa';

class Article extends Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <TouchableOpacity style={styles.container} onPress={() => Actions.articledetail({ item: this.props.item, relatedItems: this.props.relatedItems })}>
                <View style={{width: '100%'}}>
                    {this.props.item.attachments ? <NumberedSlideshow attachments={this.props.item.attachments} /> : null}
                    {/*<VideoPlayer source={{ uri: 'https://vjs.zencdn.net/v/oceans.mp4' }} />*/}
                    <View style={styles.content}>
                        <Text style={styles.title}>{this.props.item.title}</Text>
                        <Text style={styles.contentText}>{this.props.item.description}</Text>
                        <View style={styles.advRow}>
                            <View style={styles.iconContainer}>
                                <Icon name="bookmark" size={20} color="rgb(255, 200, 0)" style={{paddingLeft: 10}} />
                                <Icon name="heart" size={20} color="red" style={{paddingLeft: 25}} />
                            </View>
                            <Text style={styles.bodyText}>{moment(this.props.item.created_at, "YYYYMMDD").fromNow()} </Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}
export default Article;