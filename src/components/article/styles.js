import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        width: '90%',
        backgroundColor: 'white',
        // position: 'relative',
        // height: 200,
        borderColor: 'lightgray',
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 45
    },
    image: {
        width: '100%',
        // resizeMode: 'contain',
        height: 100,
        // position: 'absolute',
        // top: -60,
        // left: 0,
        // right: 0,
        // bottom: 0,
    },
    title: {
        color: 'rgb(70, 70, 70)',
        paddingBottom: 5,
        paddingTop: 10,
        lineHeight: 30,
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 16
    },
    advRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 5
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 5
    },
    cameraContainer: {
        flexDirection: 'row',
    },
    content: {
        backgroundColor: 'white',
        paddingRight: 15,
        paddingLeft: 10,
        paddingBottom: 10,
        width: '100%'

    },
    contentText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        lineHeight: 25,
        paddingBottom: 20
    },
    bodyText: {
        color: 'lightgray',
        fontFamily: 'IRANSansMobile(FaNum)'
    }
});