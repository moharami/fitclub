import React, {Component} from 'react';
import {
    Image,
    View,
    Text
} from 'react-native';
import styles from './styles'
import weight from '../../assets/weight.png'


class ProfileUpdatedItem extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image source={weight} />
                <Text style={styles.title}>وزن</Text>
            </View>

        )
    }
}

export default ProfileUpdatedItem;