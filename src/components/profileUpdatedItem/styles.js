
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        // width: '33%',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 30,
        marginLeft: 30,
        // marginLeft: '3%',
    },
    title: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14,
    },
    image: {
        // width: '48%' ,
        // height: 183,
        marginBottom: 15,
        paddingRight: 15,
        paddingTop: 30
    }
});
