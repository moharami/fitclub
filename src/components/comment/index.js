
import React, {Component} from 'react';
import {Text, View, Image} from 'react-native';
import styles from './styles'
import image from '../../assets/loginBackground.jpg'
import moment from 'moment';
import 'moment/locale/fa';

class Comment extends Component {
    render() {
        return (
            <View style={styles.commentContainer}>
                <View style={styles.leftItem}>
                    <View style={styles.info}>
                        <Text style={styles.date}>{moment(this.props.item.created_at, "YYYYMMDD").fromNow()}</Text>
                        <Text style={styles.owner}>{this.props.item.fname} {this.props.item.lname}</Text>
                    </View>
                    <Text style={styles.commentContentText}>{this.props.item.comment}
                    </Text>
                </View>
                <View style={styles.rightItem}>
                    <Image source={{uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTQzoL7LYphmQJUakxuGQMoTicLEMGTdb9fhBXoabCzfW44aNaZEA'}} style={styles.commentImage} />
                </View>
            </View>
        );
    }
}
export default Comment;