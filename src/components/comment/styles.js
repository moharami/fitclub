import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        flex: 1,
        width: 170,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        marginLeft: 15
    },
    commentContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    leftItem: {
        width: '80%',
        justifyContent: 'center',
        alignItems: 'flex-end',
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
        paddingBottom: 30,
        paddingTop: 30
    },
    info: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingBottom: 7
    },
    rightItem: {
        width: '20%',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    commentImage: {
        width: 55,
        height: 55,
        borderRadius: 55
    },
    owner: {
        color: 'black',
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingLeft: 15
    },
    date: {
        color: 'lightgray',
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    commentContentText: {
        fontSize: 12,
        color: 'gray',
        fontFamily: 'IRANSansMobile(FaNum)',
        lineHeight: 20
    }
});
