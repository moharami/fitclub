/**
 * Created by n.moharami on 7/25/2018.
 */

import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        flex: 1
    },
    slideContainersBottom: {
        flexDirection: 'row',
        transform: [
            {rotateY: '180deg'},
        ],
        paddingBottom: 30,
        marginBottom: 15
    },
    label: {
        paddingRight: 10,
        paddingBottom: 20,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        textAlign: 'right'
    },
});
