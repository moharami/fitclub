/**
 * Created by n.moharami on 7/25/2018.
 */
import React, {Component} from 'react';
import {Text, View, ScrollView, TouchableOpacity} from 'react-native';
import styles from './styles'
import ProgramItem from '../../components/programItem'

class ProgramItemContainer extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.label}>{this.props.item.move_title}</Text>
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ transform: [{ scaleX: -1}]}}>
                    <View style={[styles.slideContainersBottom, { borderBottomColor: this.props.border ? 'rgb(237, 237, 237)' : 'transparent', borderBottomWidth: this.props.border ? 1 : 0}]}>
                        {
                            this.props.item.rm1? <ProgramItem rm={this.props.item.rm1} set={this.props.item.set1} id={1} /> : null
                        }
                        {
                            this.props.item.rm2? <ProgramItem rm={this.props.item.rm2} set={this.props.item.set2} id={2} /> : null
                        }
                        {
                            this.props.item.rm3? <ProgramItem rm={this.props.item.rm3} set={this.props.item.set3} id={3} /> : null
                        }
                        {
                            this.props.item.rm4? <ProgramItem rm={this.props.item.rm4} set={this.props.item.set4} id={4} /> : null
                        }
                        {
                            this.props.item.rm5? <ProgramItem rm={this.props.item.rm5} set={this.props.item.set5} id={5} /> : null
                        }
                    </View>
                </ScrollView>
            </View>
        );
    }
}
export default ProgramItemContainer;