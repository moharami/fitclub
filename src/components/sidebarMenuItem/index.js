import React, {Component} from 'react';
import {Text, View, TouchableOpacity, Image, Linking} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import IIcon from 'react-native-vector-icons/dist/Ionicons';
import reginImage from '../../assets/sidebarIcons/regimIcon.png';
import contactImage from '../../assets/sidebarIcons/contactIcon.png';
import {Actions} from 'react-native-router-flux'

class MenuItem extends Component {

    render() {
        let iconType = null;
        if(this.props.icon === 'home' ) {
            iconType = <IIcon name="md-home" size={28} color="rgb(71, 76, 96)" />
        }
        else if (this.props.icon === 'ticket') {
            iconType = <Icon name={this.props.icon} size={22} color="rgb(71, 76, 96)" />
        }
        else if (this.props.icon === 'support') {
            iconType = <Icon name={this.props.icon} size={25} color="rgb(71, 76, 96)" />
        }
        else if (this.props.icon === 'regim') {
            iconType = <Image source={reginImage} />
        }
        else if (this.props.icon === 'contact') {
            iconType = <Image source={contactImage} />
        }
        else if (this.props.icon === 'question') {
            iconType = <Icon name="question" size={22} color="rgb(71, 76, 96)" />
        }
        return (
            <View style={[styles.container, { borderBottomColor: this.props.border ? 'lightgray' : 'transparent', borderBottomWidth: this.props.border ? 1 : 0}]}>
                <Text style={styles.title}>{this.props.title}</Text>
                {iconType}
                {
                    this.props.notification ?
                        <View style={styles.logoContainer}>
                            <Text style={styles.logoText}>{this.props.notificationCount}</Text>
                        </View> : null
                }
            </View>
        );
    }
}
export default MenuItem;