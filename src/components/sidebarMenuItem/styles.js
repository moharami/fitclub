import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingTop: 10,
        paddingBottom: 10,
        position: 'relative',
        flexDirection: 'row'
    },
    title: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'rgb(71, 76, 96)',
        paddingRight: 15
    },
    logoContainer: {
        height: 22,
        width: 22,
        borderRadius: 22,
        backgroundColor: "red",
        justifyContent: "center",
        alignItems: "center",
        position: 'absolute',
        top: 15,
        left: 20,
    },
    logoText: {
        backgroundColor: "transparent",
        fontWeight: "bold",
        fontSize: 12,
        color: "white",
    }
});
