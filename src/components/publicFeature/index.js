import React, {Component} from 'react';
import {Text, View, TextInput} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/Feather';

class PublicFeature extends Component {
    constructor(props){
        super(props);
        this.state = {
         text: ''
        };
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.left}>
                    <Icon name="check" size={18} color="rgb(77, 205, 202)" style={{paddingRight: 30}} />
                    <TextInput
                        placeholder={this.props.placeholder}
                        placeholderTextColor={'gray'}
                        underlineColorAndroid='transparent'
                        value={this.state.text}
                        style={{
                            height: 40,
                            backgroundColor: 'white',
                            paddingRight: 15,
                            width: '60%',
                            color: 'gray',
                            fontSize: 14,
                            textAlign: 'left',
                            fontFamily: 'IRANSansMobile(FaNum)',

                        }}
                        onChangeText={(text) => this.setState({text: text})}
                    />
                </View>
                <Text style={styles.label}>{this.props.label}</Text>
            </View>
        );
    }
}
export default PublicFeature;