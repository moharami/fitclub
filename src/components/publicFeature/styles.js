
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        height: 47,
        backgroundColor: 'white',
        paddingRight: 10,
        paddingLeft: 10,
        paddingBottom: 1,
        borderColor: 'rgb(237, 237, 237)',
        borderWidth: 1,
        borderRadius: 20,
        marginBottom: 20,
        width: '100%',

    },
    left: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-start',
    },
    label: {
        color: 'black',
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',

    }
});
