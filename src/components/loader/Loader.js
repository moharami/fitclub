import React from 'react';
import {View, Text, ActivityIndicator} from 'react-native';

import styles from './styles';

const Loader = (props) => {
    return (
        <View style={styles.container}>
            <ActivityIndicator
                size={props.size}
                animating={props.animating}
                color={props.color}
                {...props}
            />
            <Text style={[styles.text, {color: props.txtColor ? 'red' : 'white'}]}>{props.send ? 'Loading' : 'Loading'}</Text>
        </View>
    );
};
Loader.defaultProps = {
    size: 'large',
    animating: true,
    color: 'white',
    send:true
};
export default Loader;

