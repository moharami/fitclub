
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        // alignItems: 'center',
        // justifyContent: 'space-between',
        backgroundColor: 'rgb(220, 240, 247)',
        borderColor: 'rgb(118, 215, 242)',
        borderWidth: 1,
        marginTop: 15
    },
    topRow: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        padding: 15
    },
    title:{
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end'
    },
    right: {
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    bottom: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    caption: {
        fontSize: 12,
        color: 'gray',
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingRight: 5
    },
    important: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        backgroundColor: 'rgb(247, 201, 120)',
        padding: 4,
        marginRight: 15,
        marginLeft: 15,
        borderRadius: 20
    },
    importantText: {
        fontSize: 11,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    detail: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        borderBottomColor: 'rgb(118, 215, 242)',
        borderBottomWidth: 1,
        paddingBottom: 15,
        paddingTop: 15,
    },
    contentText: {
        fontSize: 12,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'right'
    },
    leftContent: {
        width: '90%'
    },
    rightContent: {
        width: '10%'
    }
});
