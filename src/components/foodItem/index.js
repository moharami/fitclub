import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import styles from './styles'
import MIcon from 'react-native-vector-icons/MaterialIcons';
import EIcon from 'react-native-vector-icons/dist/EvilIcons';
import moment from 'moment';
import 'moment/locale/fa';

class FoodItem extends Component {
    constructor(props){
        super(props);
        this.state = {
           showDetail: false
        };
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.topRow}>
                    <TouchableOpacity onPress={() => this.setState({showDetail: !this.state.showDetail})}>
                        {
                            this.state.showDetail ?  <MIcon name="keyboard-arrow-up" size={22} color="gray" /> : <MIcon name="keyboard-arrow-down" size={22} color="gray" />
                        }
                    </TouchableOpacity>
                    <View style={styles.right}>
                        <Text style={styles.title}>{this.props.item.food_name}</Text>
                        <View style={styles.bottom}>
                            <Text style={styles.caption}>{this.props.item.start_time} الی {this.props.item.end}</Text>
                            <EIcon name="clock" size={22} />
                        </View>
                    </View>
                </View>
                {this.state.showDetail ?
                    <View>
                        {/*<View style={styles.important}>*/}
                            {/*<Text style={styles.importantText}> ناشتا (یک لیوان آب ولرم + داچین + لیمو تازه + 1 قاشق مربا خوری عسل)</Text>*/}
                        {/*</View>*/}
                        {
                            this.props.type === 'diet' ? this.props.item.menu.map((info, index) =>
                            <View style={styles.detail} key={index}>
                                <View style={styles.leftContent}>
                                    <Text style={styles.contentText}> {info}</Text>
                                </View>
                                <View style={styles.rightContent}>
                                    <Text style={[styles.contentText, {alignSelf: 'flex-end', paddingRight: 15}]}>{index+1}</Text>
                                </View>
                            </View>) :  <Text style={styles.contentText}>{this.props.item.menu}</Text>


                        }


                        {/*<View style={styles.detail}>*/}
                            {/*<View style={styles.leftContent}>*/}
                                {/*<Text style={styles.contentText}> ناشتا(یک لیوان آب ولرم داچین  لیمو تازه 1 قاشق مربا خوری عسل) ناشتا(یک لیوان آب ولرم داچین لیمو تازه1 قاشق مربا خوری عسل)</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.rightContent}>*/}
                                {/*<Text style={[styles.contentText, {alignSelf: 'flex-end', paddingRight: 15}]}>1</Text>*/}
                            {/*</View>*/}
                        {/*</View>*/}
                        {/*<View style={styles.detail}>*/}
                            {/*<View style={styles.leftContent}>*/}
                                {/*<Text style={styles.contentText}> ناشتا(یک لیوان آب ولرم داچین  لیمو تازه 1 قاشق مربا خوری عسل) ناشتا(یک لیوان آب ولرم داچین لیمو تازه1 قاشق مربا خوری عسل)</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.rightContent}>*/}
                                {/*<Text style={[styles.contentText, {alignSelf: 'flex-end', paddingRight: 15}]}>1</Text>*/}
                            {/*</View>*/}
                        {/*</View>*/}
                    </View> : null

                }
            </View>
        );
    }
}
export default FoodItem;