import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import styles from './styles'
import {Actions} from 'react-native-router-flux'

class SearchItem extends Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <TouchableOpacity style={[styles.container, {marginRight: this.props.left? 15 : 0 }]} onPress={() => Actions.articledetail({item: this.props.item, relatedItems: this.props.relatedItems })}>
                <Image style={styles.image} source={{uri: "http://fitclub.ws/files?uid="+this.props.item.attachments[0].uid+"&width=300&height=100"}} />
                <View style={styles.content}>
                    <Text style={styles.title}>{this.props.title}</Text>
                    <View style={styles.advRow}>
                        <Text style={styles.time}> 3 ساعت پیش</Text>
                    </View>
                </View>
            </TouchableOpacity>

        );
    }
}
export default SearchItem;