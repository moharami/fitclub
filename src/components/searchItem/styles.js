import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        width: '46%',
        backgroundColor: 'white',
        // position: 'relative',
        // height: 200,
        marginRight: 10,
        marginLeft: 10
    },
    image: {
        width: '100%',
        // resizeMode: 'contain',
        height: 100,
        // position: 'absolute',
        // top: -60,
        // left: 0,
        // right: 0,
        // bottom: 0,
    },
    title: {
        color: 'rgb(70, 70, 70)',
        paddingBottom: 5,
        paddingTop: 10,
        lineHeight: 20,
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14
    },
    advRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginBottom: 5
    },
    cameraContainer: {
        flexDirection: 'row',
    },
    content: {
        // position: 'absolute',
        // top: 125,
        // left: 0,
        // right: 0,
        // bottom: 0,
        // padding: 5,
        backgroundColor: 'white'
    },
    time: {
        fontSize: 12,
        paddingRight: 4,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'lightgray'

    }
});