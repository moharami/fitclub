import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import styles from './styles'
import image1 from '../../assets/image1.png';
import image2 from '../../assets/image2.png';
import image3 from '../../assets/image3.png';
import image4 from '../../assets/image4.png';
import image5 from '../../assets/image5.png';
import image6 from '../../assets/image6.png';
import image7 from '../../assets/image7.png';
import image8 from '../../assets/image8.png';
import Icon from 'react-native-vector-icons/Ionicons';
import OIcon from 'react-native-vector-icons/dist/Octicons';

class BodyFeature extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            // data: [
            //     {id: 1, fillValue: ''},
            //     {id: 2, fillValue: ''},
            //     {id: 3, fillValue: ''},
            //     {id: 4, fillValue: ''},
            //     {id: 5, fillValue: ''},
            //     {id: 6, fillValue: ''},
            //     {id: 7, fillValue: ''},
            //     {id: 8, fillValue: ''}
            //     ]
        };
    }
    render() {
        let src =  null;
        switch (this.props.imgSrc) {
            case 'image1':
                src = image1;
                break;
            case 'image2':
                src = image2;
                break;
            case 'image3':
                src = image3;
                break;
            case 'image4':
                src = image4;
                break;
            case 'image5':
                src = image5;
                break;
            case 'image6':
                src = image6;
                break;
            case 'image7':
                src = image7;
                break;
            case 'image8':
                src = image8;
        }
        return (
        <TouchableOpacity style={styles.masterContainer} onPress={() => this.props.handlePress()}>
            {this.props.data[this.props.id-1] ? <Icon name="ios-checkmark-circle" size={20} color="rgb(59, 197, 197)" style={styles.icon} /> : null}
            <View style={[styles.container, {marginRight: 7, marginLeft: 7, backgroundColor: this.props.data[this.props.id-1]? 'white' : 'rgb(246, 246, 246)'}]}>
                <View style={styles.left}>
                    {this.props.data[this.props.id-1] ? <Text style={styles.value}>{this.props.data[this.props.id-1].data}</Text> : <OIcon name="dash" size={25} color="rgb(125, 125, 127)" />}
                </View>
                <View style={styles.right}>
                    <Image source={src} style={styles.image} />
                    <View style={styles.textContainer}>
                        <Text style={styles.label}>{this.props.label}</Text>
                        <Text style={styles.labelValue}>{this.props.labelValue}</Text>
                    </View>
                </View>
            </View>
        </TouchableOpacity>

        );
    }
}
export default BodyFeature;