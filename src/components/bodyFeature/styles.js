
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: "flex-end",
        justifyContent: 'space-between',
        paddingRight: 15,
        paddingLeft: 15,
        paddingBottom: 10,
        borderColor: 'rgb(237, 237, 237)',
        borderWidth: 1,
        marginBottom: 20

    },
    masterContainer: {
        position: 'relative',
        flex: 1,

    },
    icon: {
        position: 'absolute',
        top: 0,
        right: 0,
        zIndex: 9999

    },
    left: {
        // flexDirection: 'row',
        // alignItems: "center",
        // justifyContent: 'flex-start',
    },
    right: {
        alignItems: "flex-end",
        justifyContent: 'space-between',
        height: 92,
        paddingTop: 20

    },
    value: {
        color: 'black',
        fontSize: 25,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    textContainer: {
        alignItems: "flex-end",
        justifyContent: 'flex-end'
    },
    label: {
        color: '#b7b7b8',
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    labelValue: {
        color: '#c0c0c1',
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)'
    }
});
