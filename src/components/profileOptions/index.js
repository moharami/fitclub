import React, {Component} from 'react';
import {
    ImageBackground,
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import {Actions} from 'react-native-router-flux'
import styles from './styles'
import wallet from '../../assets/walllet.png'
import programs from '../../assets/programs.png'
import tickets from '../../assets/tickets.png'
import favorite from '../../assets/favorite.png'

class ProfileOptions extends Component {
    handlePress() {
        if(this.props.src === 'wallet') {
            Actions.wallet({data: this.props.data.data})
        }
        else if(this.props.src === 'programs') {
            Actions.push('UserPrograms')
        }
        else if(this.props.src === 'tickets') {
            Actions.push('tickets')
        }
        else {
            Actions.push('commonQuestion')
        }

    }
    render() {
        this.props.data ?  console.log('innnne',this.props.data.data.credit): null
        return (
        <TouchableOpacity onPress={() => this.handlePress()} style={{flex: .48}}>

            <ImageBackground
                resizeMode='stretch'
                source={this.props.src === 'wallet' ? wallet : (this.props.src === 'programs' ? programs: (this.props.src === 'tickets' ? tickets : favorite))}
                // style={[styles.image, {marginLeft: this.props.marginLeft ? 10 : 0, marginRight: this.props.marginRight ? 10 : 0, marginTop: this.props.marginTop ? 10 : 0, marginBottom: this.props.marginBottom ? 10 : 0,}]}
                style={styles.image}
            >
                <View style={styles.textContainer}>
                    <Text style={styles.title}>{this.props.title}</Text>
                    <View>
                        <Text style={styles.inventoryTitle}>
                            {
                                this.props.inventoryTitle ? this.props.inventoryTitle : ''
                            }
                        </Text>
                        <Text style={styles.inventory}>
                            {
                                this.props.data ? this.props.data.data.credit : null
                            }
                        </Text>
                    </View>


                </View>
            </ImageBackground>
        </TouchableOpacity>
        )
    }
}

export default ProfileOptions