
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    image: {
        width: '100%' ,
        height: 150,
        marginBottom: 15,
        paddingTop: 30,
    },
    title: {
        color: 'white',
        fontSize: 18,
        fontFamily: 'IRANSansMobile(FaNum)',

    },
    inventoryTitle: {
        color: 'white',
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    inventory: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    textContainer: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'space-between',
        paddingBottom: 10,
        paddingRight:15
    }
});
