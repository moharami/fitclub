
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        // flex: 1,
        width: '100%',
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-around',
        backgroundColor: 'white',
        position: 'relative',
        bottom: 25,
        zIndex: -1
    },
    navContainer: {
        alignItems: "center",
        justifyContent: 'center',
        // paddingTop: 6,
        // paddingBottom: 3,
        position: 'relative'
    },
    label: {
        fontFamily: 'IRANSansMobile(FaNum)',
        position: 'absolute',
        bottom: 0
    }
});