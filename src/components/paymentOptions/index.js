import React from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';
import styles from './styles'
import saman from '../../assets/saman.png'
import melat from '../../assets/melat.png'
import zarin from '../../assets/zarin.png'
import ap from '../../assets/ap.png'

export default class PaymentOptions extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            active: 0
        };
    }
    render(){
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => null}>
                    <View style={styles.navContainer}>
                        <Image source={ap} style={{marginTop: 10}} />
                        <Text style={[styles.label, {color: 'black'}]}>آسان پرداخت</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => null}>
                    <View style={styles.navContainer}>
                        <Image source={melat} style={{marginTop: 10}} />
                        <Text style={styles.label}>ملت</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => null}>
                    <View style={styles.navContainer}>
                        <Image source={saman} />
                        <Text style={styles.label}>سامان</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => null}>
                    <View style={styles.navContainer}>
                        <Image source={zarin} />
                        <Text style={[styles.label, {color: 'black'}]}>زرین پال</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}