import React, {Component} from 'react';
import {TextInput} from 'react-native';
import moment from 'moment'
moment.locale('fa');
class AppTextinput extends Component {

    constructor(props){
        super(props);
        this.state = {
            text: '',
            fill: 0
        };
    }
    handleChange(text){
        this.setState({text: text, fill: text.length});
        this.props.check(text);
    }
    focuse() {
        if(this.props.birthday) {
            this.props.openPicker();
        }
    }
    render() {
        return (
            <TextInput
                onFocus={() => this.focuse()}
                placeholder={this.props.placeholder}
                placeholderTextColor={'lightgray'}
                underlineColorAndroid='transparent'
                value={this.props.date? moment(this.props.date).format('Y-M-D')  :this.state.text}
                style={{
                    height: 45,
                    backgroundColor: 'rgb(225, 75, 76)',
                    paddingRight: 15,
                    width: '100%',
                    fontSize: 16,
                    color: 'white',
                    textAlign:'right',
                    fontFamily: 'IRANSansMobile(FaNum)'
                }}
                // onChangeText={(text) => this.setState({text: text, fill: ++this.state.fill})}
                onChangeText={(text) => this.handleChange(text)}
            />

        );
    }
}
export default AppTextinput;