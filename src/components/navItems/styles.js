
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        transform: [
            {rotateY: '180deg'},
        ],
        paddingTop: 25,
        paddingBottom: 30
    },
    navContainer: {
        alignItems: "center",
        justifyContent: 'center',
        width: 84,
        height: 96,
        paddingBottom: 40,
        marginLeft: 20

    },
    text: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        // paddingBottom: 55,
        position: 'absolute',
        bottom: 15

    }
});