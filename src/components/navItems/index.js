
import React from 'react';
import {Text, View, TouchableOpacity, Image, ScrollView, I18nManager} from 'react-native';
import daro from '../../assets/4.png'

import styles from './styles'
export default class NavItems extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            active: 0,
            homeActive: true
        };
    }
    handlePress(key) {

        if(key === 'home') {
            this.setState({homeActive: true });
            // this.props.handlePress(key);
            this.refs.child.handlePress(key)

        }
        else{
            this.setState({active: key, homeActive: false});
            // this.props.handlePress(key);
            this.refs.child.handlePress(key)
        }
    }
    render(){
        return (
            <ScrollView style={{ transform: [
                { scaleX: -1},
                // {  rotate: '180deg'},

            ],}}
                        horizontal={true} showsHorizontalScrollIndicator={false}
            >
            <View style={styles.container}>
                {this.props.posts.category.map((item)=>
                <TouchableOpacity onPress={() => this.handlePress(item.id)} key={item.id}>
                    <View style={[styles.navContainer, { borderColor: this.state.active === item.id ? 'rgb(240, 122, 133)': 'lightgray', borderWidth: 1}]}>
                        <Image source={{uri: "http://fitclub.ws/files?uid="+item.attachments.uid+"&width=104&height=92" }} style={{ width: 50, height: 50}} />
                        {/*<Image source={{uri: "http://fitclub.ws/files?uid="+item.attachments[0].uid+"&width=104&height=92" }} style={{ width: 50, height: 50, tintColor: this.state.active === item.id ? 'red': 'gray'}} />*/}
                        <Text style={[styles.text, {color: this.state.active === item.id ? 'rgb(240, 122, 133)': 'gray'}]}>{item.title}</Text>
                    </View>
                </TouchableOpacity>
                )}
                <TouchableOpacity onPress={() => this.handlePress('home')}>
                    <View style={[styles.navContainer, { borderColor: this.state.homeActive ? 'rgb(240, 122, 133)': 'lightgray', borderWidth: 1}]}>
                        <Image source={{uri: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAilBMVEX///8jHyAAAADv7+8fGxwOBggeGRsyLzCcm5w+OzwMAAVBPz+ko6MaFRYJAAAcGBkUDhBxb2/29vbd3d2zsrLIx8fr6+u6ubmioaHS0tJ+fX2WlZVpZ2dHREUpJSbh4OBWVFW/vr/KycmPjo44NTaKiYlcWlpZV1hsamssKSpNS0t6eHirqqtiX2DSK2VdAAAOdElEQVR4nN1d54KiMBBeRrCtgYgde1t3Le//egcKZhICUgPc9+tOXchHpidMvr5KR2s0GXePzu9u3Tt1Ntqmc+qtd7/OcT6ejFrl375UjGbHR68NAKZBKdF1XXvB/Reh1DDdb/Te4zgbVT3QLNjOzleXgEECVlHQieH+7nqebasecgqM5g8dgFofuGFYFEB7zJswmfb+piWYOfls9mHjTOyqKcShdb+4c5eF3ZulO5c/45raH3t8AZPkYBeAmPAzq99MLoYARdDzSQLcFlVTwrDvpwLpvUChd6+aV4CRA2Ye3YtC24RzHVzI4gfop7HqFqFG33PwAcy+Qclnd0JhVbUDWVzjxdOinj/Xrr9O9z6eHUYvHGbje9f5vepeTEDbcRcgcKlSIV1+0fOgUxPo2unuF9F20V7su86aghnjYgjsquI4ukTy89xazxkn1aLt+HaKcaQEHlXoY2sYJZ+uP+ucU4cmrcn5D/oRj4yAo9xBdiPsi0tvN8j6xLeDXVTQQPtqfcfiD2TDsAAe43wP2wv8TKntgZ46s2oPQaYyFNY56b3QuvekAmKBU8DVk2Asu78OpED3PHIMWRBh0H1ht4iG/SMRUAKnccG3uf9JLLUOw2JvI8FeYglcj3Uo4VYTmbel7TJuheCEJ7BEj3yQcNThWNLdPExPhnjDNlzLjDgm32GjBtfSMuRx6Inq8F2y0HztOyGxIXRSzr3CEtonBdsXKQamKDk6LEu4j301QwpYpkbgW59DwgOrwu+y3YhOEK7Twu8ShVFPlB+jV3CgehCfIjHVxond0ABooUHcTHyEsFNd8dt+C2PQoUAjPhAubkEVRaIltAWKhcVwS4Fgv1dNgWgh2gIoyJSfBYLKQvwwVuJQBkVcVSDYhlkRV82IgRDiwDz/NQU/T7VqS5iLPimYokDQ3FW9oNA60UIpHnmCcCtmmLkgJKj57LpgRaFb1ChzQZCrPIZhLFxKRZydBOKDz+z6D8KFVBRJkoEPQXTIaP22wmVKSsoy4c6Nra1nsn+21uYI1mrFUlAgss5yjTWpMUExGTAy1OAcLuGtHUGRYvr4jZeCWulgAF4X084Bb2Vq4yZ4cOGIbqWzNn84o66Jow9jiDWJXtL86Q1Xt8w6hGpyXHGMmiZC5ZSY/pQ3wtzocLKWuHLTwgTbf2WOMC+mOF+0Okn/7Io9YdaISBG4yNI8J/sjzgrXKBiVo8uNNpHL4GRUVVk7Bx7I2uiJVOqBZJRcyx5fAdCQKpoJZmTPTWFNd3pyWHAj/mw2+uiJVFpWS44jcvzWxyzjiHw9fagYXwE4tZPPyhTNuE6rrqslxQiPWov/LTYzDZFRD1hOzdjVU+w/SZ2jNREbbD3izOM3CvPMJtjRANie0phMAXuKIlYEFGKI/D5EL04jm6QnjmLrARtPYmTRhpvCOtYt4tBFxibS7f8xdSU7laMrBCh4i5pEbgqr3iKfHrh2FqGJV+vjQ6g1kBWh0mXqxednUG9gGTRl4RgKZxo5hV9fPTaJpsTX4Yi05pWLKKBJ1Gn46zNzmeRX/egKQYeZU0n1Bb322UBD+gIyp2F3N4v7sjFAyW1I05CraFw4w7BkFA0hiUJ2pmkRKQamIWTCXUS+WUkFjxVzeULtFLkSaErtQoYJm0Q+rkGFDtKU8pMcKE0k+HOkoQ22Mx5QrRDwawTMVepWZYMrBFu5mEZ83EiwUpOOxHTQj7JAzQPyCojLjtlYo8LBFQIkj8zpoypOXCWuIei8HZ/1XjlDTqThltTDkTmMt2tHiVO/ye7+BVSreKdQLKBpcFrBwOaL+uv6aFm7X8ju/orByjHt79cnWA2bmvti3Jnv8wtSKGTrVzy4QoCCbN8jsllteNQdgNX3/UwQfVDXLXrp8MOm7FlTQ3kxlP1SrxowtdM33v+xocm6KDoZ3J8YRFkqe+oj/jrBr/J5ZVQ3ffr8rsA4CzQwn4AoRV763ZM+3IH4P8unLVPB1AwFqc0AtvwfJQX+Y9R78Rfy89S8bpklEqa3u/n7HdGIBbjEYPWfqEuoZchKo9TbBRZbCU8EFBRFbZ1Ty9B5B26e/2vlj2gGeIFZnpyoZcgyeu+GC8HwZMAJbWaJCBrUMkTuoY1Na1ZTyu0NjLA1ahmOOAfI4lQr42bSG/dSpzwuUsvQ5jSPpcQZnYXtPyIrThLUMsTW84DsDk24D1yAv2hnBQG81NYoZsg8oOsfmC/LeN31a/Jg4YcOUlujmCGLvfv3r+t7Rs1Mbzf51TtXOAMLJtvIoZghi9Po8uvERDaTw/fLWJ6IW6/xyeIaxQw51dswhpnK3T4tL1rwycpsjWKGLH+izpfGGGYJaXx3+lw4DtyQxNYoZsiCGnLDDLPsovEXBPrPcoFflpTYGsUM7+84kqwwwwxbvaacdQkeXfhKihmO3wytn5wMfYEnr7cbgyg+bGsUM2R7Z/R1Tin1d5AHr9H6fihsaxQzRDvyO/kszdsF+lnJPsrWKGaIX8DA3iI9Qz9SY3sZfT8UsjXVMdzk8oeB3rEqZOBqRZ1WzHCBGf7lYDh/2U60/WgRYWuqY9jBUVvq1VE/uafopT9/jLrO/1IxQ5Tkn77W78g7dVz6nrDBYeLjcJPnUIoZ7rG32GXPLd7JvcEa5gdBvWBrKvP47R3OD1M2XLK5+owI3tZUF5f+fjmsmpuyln4X231y4G1NlbkF26aQdjvUOvZQA34vuWKGN5wfsoUZkq7r6ShWSAVbo5ghW/Q15jgMT1dNDCa/TQT4pouzNYoZrt8LF+YMuY6UFeGA4HooICiBY1ujmCHaDzzh6sNpqvpBghKOhAKpwLamyopw1pWZ3UsQJK9OB9fHux8Dht/xFw0Y5mwAy1f1bbT3O0XYNo1Mdtkbq+h6b3v2HUaPKWwQ83UkP+skD7nw60Huf3vvoCbNLv1gt7EsbQ6iOWRr3gz1dgiUWbhgr7LkV2n6pjL/8NwVxSxrGofoJ/fyJg2dkK1B+1pDsMIMZUjBcMivxSx5wskQWGB5z+uADxNhtQzZguYzTkNvPCXfIPxehpGa30BJmRlSytBGm7o95cWGJ2ktqhXWNA4/Yg6llGGIEft/4vzpGCRLEfZtH3wfDH4JMWBOksb9LDFDlBLA8wO2gTaxqWkF+PSDwNTYrTiErytD4niExd2+eLCFmhSmps5gsu4v+o7RpDZ/mze/f+Yl2Vgxm79Vn9+452sJelM/21J+vcC07p0tsahGP1U6tmLA2nu8d5fM+6FpbTDQa0HviGuUwSPWF/g1tXcEwxKo/2Av+469nsc2Sg5Rjth0f4F8BQpgUPBd+16Xn4C9O/N9iHdje2IEuMjlkcmuvLNLc4B2sXM2BUfjzbamaL8yl4wgMf3cPLLWwA1cOGnE0tvM9jsvoMVfYZECdXZpdGyK9isLXgFVifV2RaMrADbugSl894vEtDldPUUgixmqV6BdRNaHxYUaA/XdCy9RoJaCje2rgHJfK1yQQSkUSXXKQI3wjVbVwm4d7zto6AvPuIMuSL5HhrahORTq5mXIOpdvmz6J3BRKaxU/DU+E1x97d3LNL5tnTrn2qxGRJ3oIDYy/Ucwd2aibE+SmBTaDRGYEGaPs73VXAxyRxlgRrInxzdtrB/wSZFz+hxpGNas4zPVkj+ugi3/YiPNJAuBTkeKnhmve3pyKzRJpoRGfwXPH6DTm7AAcjunkQ7EQN29vzPkP+EiHz5Kn62l+XQvgg1cTpO8TLKeNsKf8WUEJcoZf3AK0CcEbPp/kg5l5gduCn/Qkswqxwmd26Z9//yUc0Fn7+HTOjTZhGyiUKNa+BM6dnWck7WuJo1jN2tR5MaplIiVMkS1wZ6/RXXkDzI0Td4ZlirTd6aM/NOt7FsSOO4c01Ts//LOpayJ1w+qUsm8nPu2itrENfx7wp3hUxH9/pnPzzuXOcBoAd5pw7c9Wz7KFxN5YNabIa1HG+Jk7TdilWKfGmLwOtvWMUQnfpqxO26UG3Mj07JHlTKBYF6exFMaVQ4G6wqXqcXyJI4wqV/pzFC5Wh6b0F2FMOV/lO/OXM3dVZxrTE+UJ5m5BLlCkm2rzxQNYBRMMUbQqNalz0AsnGKKoQWXHmNgPcSg5dTCAYJw1s1eNpC50KhAszH0NBIpWUc8uFY6ChOpF6ovg+t2n96N6TWPbE9pwFBwpH0BogkH6agOcpWBDNUIKVpXthvB30GCnruS/6Ih9VIzvwh2zvRZvQlTVb2xHlCANSnmlwBGVUTM1FRXxOQgm1FXBkg5xGIu64N5qXXZiPPsLNfohpLRUddsJvZJtwaVMjvsehF7vhlJj41u47Q6BR1n7/CbfIQUsT0IDzEC0qR7HnzLmcS/hpxmb0utFrUt4Gl2OvYJtjj3QQlrvTaCSBHUcsm1PfdSPxbngxQ1MSXsFo62oGmavwurvNfeC66wIGzAddGTP0H2IChekDxtprzYLzFVOkq37Tjp9nmNSm9EspY/Z00h43LOGc6PuGsyw9j0FlCov9E1/JVb1RdKE0/GQdipbe2cDhpyeRuFcRX1ocZVYO1+kDID1ebZNNix7NL51AGhU6xYCw6r29RzWkRxdpaQAxunWnYyiebYWs+XqzwQzkl2pEUUSuBwjZNWfTOp1/tzsfs/z+3h/GD2xmMzug66zunrHOxkkpuuOJ5+V8vOw2EXYHMzTItQwTdRNx+wblMR2lnzCgGEdNruMbtCPnYeMsACOddlXZ8878cKaARTWdVkIeuGwivJkWUDAdKpWvzBsLxopgqRrf1b1WarkMb1f4bPdiYMb3OYO/EpGaz8krgfIxM4NhjrnOq2lR2LRvbiKRNNILHHDg83vvQ6uISlGY2ftuj0a78+9d+OJFxNcjvu6OIZUGI2Pl5MXuJgGdbm+yeouL+rGAO432vdquW/S1Elgtxb7cffoBmnr3ulvo206p956Nzwf5+O4kLUw/AMWp9YTlYNIOAAAAABJRU5ErkJggg==" }} style={{ width: 30, height: 30}} />
                        {/*<Image source={{uri: "http://fitclub.ws/files?uid="+item.attachments[0].uid+"&width=104&height=92" }} style={{ width: 50, height: 50, tintColor: this.state.active === item.id ? 'red': 'gray'}} />*/}
                        <Text style={[styles.text, {color: this.state.homeActive ? 'rgb(240, 122, 133)': 'gray'}]}>همه مقالات</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </ScrollView>
        );
    }
}