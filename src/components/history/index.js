import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import moment from 'moment';
import 'moment/locale/fa';

class History extends Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <View>
                {
                    this.props.item.transaction[0] ?
                        <View style={[styles.container,{borderBottomWidth: this.props.border ? 1 : 0, borderBottomColor: this.props.border ? 'lightgray' : 'transparent' }]}>
                            <View style={styles.walletContainer}>
                                <Text style={styles.priceText}>{this.props.item.amount} ریال</Text>
                                <View style={styles.payment}>
                                    <Text style={styles.paymentType}>از کیف پول </Text>
                                    <Text style={styles.bodyText}>پرداخت </Text>
                                </View>
                            </View>
                            <View style={styles.right}>
                                <View style={styles.infoConainer}>
                                    <Text style={styles.titleText}>{this.props.item.description}</Text>
                                    <Text style={styles.bodyText}>{moment(this.props.item.transaction[0].created_at).format('L')}</Text>
                                    <Text style={styles.bodyText}>کد پرداخت: {this.props.item.transaction[0].trace_number}</Text>
                                </View>
                                {
                                    this.props.item.description === 'شارژ کیف پول' ?  <Icon name="arrow-up" size={18} color="rgb(32, 193, 188)" style={{paddingLeft: 5, paddingTop: 20}} />
                                    : <Icon name="arrow-down" size={18} color="red" style={{paddingLeft: 5, paddingTop: 20}} />
                                }
                            </View>
                        </View>
                        : null
                }

            </View>

        );
    }
}
export default History;