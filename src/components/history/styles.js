import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingBottom: 15

    },
    bodyText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'lightgray'
    },
    paymentType: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'gray'
    },
    titleText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14
    },
    priceText: {
        color: 'black',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    right: {
        flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: 'space-between',
    },
    payment: {
        flexDirection: 'row'
    }
});