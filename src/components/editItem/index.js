import React, {Component} from 'react';
import {TextInput, View, Text} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/FontAwesome';

class EditItem extends Component {

    constructor(props){
        super(props);
        this.state = {
            text: ''
        };
    }
    handleChange(text){
        this.setState({text: text, fill: text.length});
        this.props.check(text);
    }
    focuse() {
        if(this.props.birthday) {
            this.props.openPicker();
        }
    }
    render() {
        return (
        <View style={[styles.container, {borderBottomColor: this.props.border ? 'rgb(237, 237, 237)' : 'transparent', borderBottomWidth: this.props.border ? 1 : 0}]}>
            <View style={styles.left}>
                <Icon name="chevron-left" size={12} color="rgb(180, 180, 180)" style={{paddingRight: 30}} />
                <TextInput
                    onFocus={() => this.focuse()}
                    placeholder={this.props.value}
                    placeholderTextColor={'gray'}
                    underlineColorAndroid='transparent'
                    value={this.state.text}
                    secureTextEntry={this.props.secure}
                    style={{
                        height: 40,
                        backgroundColor: 'white',
                        paddingRight: 15,
                        width: 180,
                        color: 'gray',
                        fontSize: 14,
                        textAlign: 'left'
                    }}
                    // onChangeText={(text) => this.setState({text: text, fill: ++this.state.fill})}
                    onChangeText={(text) => this.handleChange(text)}

                />
            </View>
            <Text style={styles.label}>{this.props.label}</Text>
        </View>
        );
    }
}
export default EditItem;