import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image
} from 'react-native';

import Swiper from 'react-native-swiper';

const styles = StyleSheet.create({
    wrapper: {
        width: '100%',
        height: 170
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },
    mainImage:{
        width: '100%',
        height: 170,
        // marginRight: 10,
        overflow: 'hidden',
        alignItems: "flex-end",
        justifyContent: "flex-end",
        marginRight: 30,
    },
    imageText: {
        backgroundColor: "transparent",
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: "white",
        textAlign: 'center'
    },
    paginationStyle: {
        backgroundColor: 'rgba(0,0,0, .5)',
        borderRadius: 30,
        width: 30,
        height: 30,
        position: 'absolute',
        top: 10,
        left: 30,
        alignItems: 'center',
        justifyContent: 'center',

    },
    paginationText: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)'

    }
});
const renderPagination = (index, total, context) => {
    return (
        <View style={styles.paginationStyle}>
            <Text style={{ color: 'white', fontFamily: 'IRANSansMobile(FaNum)'}}>
                <Text style={styles.paginationText}>{index + 1}</Text>/{total}
            </Text>
        </View>
    )
}
export default class NumberedSlideshow extends Component {
    render(){
        return (

            <Swiper style={styles.wrapper} showsPagination={true}
                    renderPagination={renderPagination}
                    loop={false}
            >
                {
                    this.props.attachments.map((item) =>  <Image source={{uri: "http://fitclub.ws/files?uid="+item.uid+"&width=300&height=100"}} style={styles.mainImage} key={item.id} />)

                }

            </Swiper>
        );
    }
}





