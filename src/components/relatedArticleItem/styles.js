import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        flex: 1,
        width: 170,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        marginLeft: 15
    },
    image: {
        width: '100%',
        height: 240,
        flex: 1,
        borderRadius: 12
        // resizeMode:'contain',
        // overflow: 'hidden'
    },
    caption: {
        fontSize: 14,
        color: 'black',
        paddingTop: 8,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    date: {
        fontSize: 13,
        color: 'lightgray',
        paddingTop: 5,
        fontFamily: 'IRANSansMobile(FaNum)'
    }
});
