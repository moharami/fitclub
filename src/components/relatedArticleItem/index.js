import React, {Component} from 'react';
import {Text, View, ScrollView, Image, TouchableOpacity} from 'react-native';
import styles from './styles'
import image from '../../assets/loginBackground.jpg'
import moment from 'moment';
import 'moment/locale/fa';
import {Actions} from 'react-native-router-flux'

class RelatedArticleItem extends Component {
    render() {
        return (
            <TouchableOpacity style={styles.container} onPress={() => Actions.articledetail({ item: this.props.item, relatedItems: this.props.relatedItems })}>
                <Image source={{uri: "http://fitclub.ws/files?uid="+this.props.item.attachments[0].uid+"&width=200&height=240"}} style={styles.image} />
                <Text style={styles.caption}>{this.props.item.title}</Text>
                <Text style={styles.date}>{moment(this.props.item.created_at, "YYYYMMDD").fromNow()}</Text>
            </TouchableOpacity>
        );
    }
}
export default RelatedArticleItem;