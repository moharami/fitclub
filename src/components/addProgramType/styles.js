import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: 'rgb(237, 237, 237)',
        marginBottom: 30

    },
    payment: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 16,
        paddingLeft: 15
    },
    type: {
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 13,
    },
    typeDetail: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 10,
    },
    lastUpdate: {
        color: 'lightgray',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 13,
    },
    left: {
        alignItems: 'stretch',
        justifyContent: 'space-between',
        width: '73%',
        padding: 5,
        paddingTop: 10
    },
    top: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
    },
    bottom: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'space-between',
        paddingTop: 10
    },
    right: {
        width: '27%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        width: 70,
        height: 70
    },
    price: {
        color: 'rgb(77, 205, 202)',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 20,
    },
    suportContainer: {
        backgroundColor: 'rgb(277, 5, 26)',
        paddingRight: 10,
        paddingLeft: 10,
        borderRadius: 20,
        marginTop: 10

    },
    suportText: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 10
    }
});
