import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import styles from './styles'
import pImage from '../../assets/programSlide.png'
import {Actions} from 'react-native-router-flux'

class AddProgramType extends Component {
    render() {

        return (
                <TouchableOpacity style={styles.container} onPress={() => Actions.programFeature({titleName: this.props.titleName, price: this.props.price})}>
                    <View style={styles.left}>
                        <View style={styles.top}>
                            <View style={styles.suportContainer}>
                                <Text style={styles.suportText}> + پشتیبانی 24 ساعته </Text>
                            </View>
                            <View style={styles.contentRight}>
                                <Text style={styles.type}>{this.props.title}</Text>
                                <Text style={styles.typeDetail}>{this.props.subLabel1}</Text>
                                <Text style={styles.typeDetail}>{this.props.subLabel2}</Text>
                            </View>
                        </View>
                        <View style={styles.bottom}>
                            <View style={styles.contentLeft}>
                                <Text style={styles.price}>{this.props.price} ریال</Text>
                            </View>
                            <View style={styles.contentRight}>
                                <Text style={styles.typeDetail}>دو ماهه</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.right}>
                        <Image source={this.props.imgSrc} style={styles.image} />
                    </View>
                </TouchableOpacity>

        );
    }
}
export default AddProgramType;