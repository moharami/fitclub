
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        flex: 1,
        width: 120,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 15
    },
    image: {
        // width: '100%',
        // height: 240,
        // width: '50%',
        flex: 1,
        resizeMode:'contain',
        // overflow: 'hidden'
    },
    caption: {
        fontSize: 14,
        color: 'black',
        paddingTop: 8,
        fontFamily: 'IRANSansMobile(FaNum)',
    }
});
