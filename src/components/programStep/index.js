import React, {Component} from 'react';
import {Text, View, ScrollView, Image} from 'react-native';
import styles from './styles'
import programSlide from '../../assets/programSlide.png'

class ProgramStep extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image source={programSlide} style={[styles.image, {opacity: this.props.active? 1 :.7, width: this.props.active? '70%' : '40%', height: this.props.active? 66 : 56}]} />
                <Text style={styles.caption}>{this.props.label}</Text>
            </View>
        );
    }
}
export default ProgramStep;