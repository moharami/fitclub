
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        flex: 1,
        width: 160,
        flexDirection: 'row',
        borderColor: 'rgb(93, 157, 255)',
        borderWidth: 1,
        marginLeft: 15,
        height: 60,
        position: 'relative'

    },
    left: {
        width: '85%'
    },
    right: {
        width: '15%',
        backgroundColor: 'rgb(93, 157, 255)',
        alignItems: 'center',
        justifyContent: 'center',
    },
    topRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingRight: 15
    },
    bottomRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        right: 15,
        position: 'absolute',
        bottom: 3
    },
    label: {
        color: 'rgb(132,132, 132)',
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    rightLabel: {
        color: 'white',
        fontSize: 9,
        fontFamily: 'IRANSansMobile(FaNum)',
        transform: [
            {rotate: '90deg'},
        ],
    },
    value: {
        color: 'black',
        fontSize: 20,
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    caption: {
        fontSize: 14,
        color: 'black',
        paddingTop: 8,
        fontFamily: 'IRANSansMobile(FaNum)',
    }
});
