import React, {Component} from 'react';
import {Text, View, ScrollView, Image} from 'react-native';
import styles from './styles'
import programSlide from '../../assets/programSlide.png'

class ProgramItem extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.left}>
                    <View style={styles.topRow}>
                        <Text style={styles.value}> {this.props.set}</Text>
                        <Text style={styles.label}>تکرار  </Text>
                    </View>
                    <View style={styles.bottomRow}>
                        <Text style={styles.value}>% {this.props.rm}</Text>
                        <Text style={styles.label}>درصد RM  </Text>
                    </View>
                </View>
                <View style={styles.right}>
                    <Text style={styles.rightLabel}>ست {this.props.id}</Text>
                </View>
            </View>
        );
    }
}
export default ProgramItem;