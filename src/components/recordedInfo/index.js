
import React, {Component} from 'react';
import {Text, View, Image} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/MaterialIcons';

class RecordedInfo extends Component {
    render() {
        return (
            <View style={[styles.container, {backgroundColor: this.props.backgroundColor, borderColor: this.props.borderColor}]}>
                <Text style={styles.topText}>{this.props.name}</Text>
                <Text style={styles.middleText}>{this.props.item}</Text>
                <Text style={styles.bottomText}>سانتی متر</Text>
            </View>
        );
    }
}
export default RecordedInfo;