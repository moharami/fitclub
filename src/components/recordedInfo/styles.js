import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        padding: 15,
        height: 90,
        position: 'relative',
        marginLeft: 15
    },
    topText: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 10,
        position: 'absolute',
        top: 7
    },
    middleText: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 30
    },
    bottomText: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 10,
        position: 'absolute',
        bottom: 15,
        right: 0,
        left: 0,
        textAlign: 'center'
    }
});
