import { StyleSheet, Dimensions, Platform } from 'react-native';
import { colors } from './index.style';

const IS_IOS = Platform.OS === 'ios';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp (percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}

const slideHeight = viewportHeight ;
const slideWidth = wp(100);
const itemHorizontalMargin = wp(0);

export const sliderWidth = viewportWidth;
export const itemWidth = slideWidth + itemHorizontalMargin * 2;

const entryBorderRadius = 8;

export default StyleSheet.create({
    slideInnerContainer: {
        width: itemWidth,
        height: slideHeight,
        position: 'relative',
        // bottom: '-50%',
        // top: 10,
        // paddingBottom: 30,
    },
    // shadow: {
    //     position: 'absolute',
    //     top: 0,
    //     left: itemHorizontalMargin,
    //     right: itemHorizontalMargin,
    //     bottom: 18,
    //     shadowColor: colors.black,
    //     shadowOpacity: 0.25,
    //     shadowOffset: { width: 0, height: 10 },
    //     shadowRadius: 10,
    //     borderRadius: entryBorderRadius
    // },
    imageContainer: {
        flex: 1,
        marginBottom: IS_IOS ? 0 : -1, // Prevent a random Android rendering issue
        backgroundColor: 'white',
        borderTopLeftRadius: entryBorderRadius,
        borderTopRightRadius: entryBorderRadius
    },
    imageContainerEven: {
        backgroundColor: colors.black
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'cover',
        borderRadius: IS_IOS ? entryBorderRadius : 0,
        borderTopLeftRadius: entryBorderRadius,
        borderTopRightRadius: entryBorderRadius
    },
    // image's border radius is buggy on iOS; let's hack it!
    radiusMask: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        height: entryBorderRadius,
        backgroundColor: 'white'
    },
    radiusMaskEven: {
        backgroundColor: colors.black
    },
    textContainer: {
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'flex-end',
        // paddingTop: 200,
        // paddingBottom: 40,
        // paddingLeft: 60,
        // paddingRight: 60,
        bottom: -10,
        right: 50,
        left: 50,
        // top: 190,
        height: 120,
        zIndex: 9999,
        paddingHorizontal: 16,
        // backgroundColor: 'transparent',
    },
    title: {
        color: 'white',
        fontSize: 18,
        textAlign: 'center',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    subtitle: {
        fontFamily: 'IRANSansMobile(FaNum)',
        // marginTop: 6,
        color: 'white',
        fontSize: 12,
        textAlign: 'center',
        lineHeight: 21,
        paddingBottom: 10
    },
});