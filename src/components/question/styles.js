import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        borderRadius: 5,
        elevation: 5,
        margin: 10
    },
    qContainer: {
        alignItems: 'flex-end',
        justifyContent: 'center',
        width:'100%',
        borderWidth: 1,
        borderColor: 'rgb(237, 237, 237)',
        backgroundColor: 'rgb(247, 247, 247)',
        padding: 10
    },
    rContainer: {
        alignItems: 'flex-end',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: 'rgb(237, 237, 237)',
        backgroundColor: 'white',
        padding: 10,
        width:'100%',

    },
    qText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 16,
        // color: 'rgb(178,34,34)'
        color: 'black'
    },
    rText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14,
        color: 'gray'
    }
});
