
import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/MaterialIcons';

class Question extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false
        };
    }
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.qContainer} onPress={() => this.setState({show: !this.state.show})}>
                    <Text style={styles.qText}>{this.props.question}</Text>
                </TouchableOpacity>
                {
                    this.state.show ?
                    <View style={styles.rContainer}>
                        <Text style={styles.rText}>{this.props.answer}</Text>
                    </View> : null
                }

            </View>
        );
    }
}
export default Question;