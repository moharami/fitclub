import React from 'react';
import {Text, View, TouchableOpacity, ScrollView} from 'react-native';

import styles from './styles'
export class Categories extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            active: 0
        };
    }
    render(){
        return (

            <ScrollView style={{ transform: [
                { scaleX: -1}

            ],}}
                        horizontal={true} showsHorizontalScrollIndicator={false}
            >
            <View style={styles.container}>
                <TouchableOpacity onPress={() => this.setState({active: 4})}>
                    <View style={[styles.navContainer, { borderBottomColor: this.state.active === 4 ? 'black': 'transparent', borderBottomWidth: this.state.active === 4 ? 1: 0}]}>
                        <Text style={[styles.text, {color: this.state.active === 4 ? 'black': 'rgb(170, 170, 170)'}]}>دسته 1</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.setState({active: 3})}>
                    <View style={[styles.navContainer, { borderBottomColor: this.state.active === 3 ? 'black':  'transparent', borderBottomWidth: this.state.active === 3 ? 1: 0}]}>
                        <Text style={[styles.text, {color: this.state.active === 3 ? 'black': 'rgb(170, 170, 170)'}]}>دسته 2</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.setState({active: 2})}>
                    <View style={[styles.navContainer, { borderBottomColor: this.state.active === 2 ? 'black':  'transparent', borderBottomWidth: this.state.active === 2 ? 1: 0}]}>
                        <Text style={[styles.text, {color: this.state.active ===2 ? 'black': 'rgb(170, 170, 170)'}]}>دسته 3</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.setState({active: 1})}>
                    <View style={[styles.navContainer, { borderBottomColor: this.state.active === 1 ? 'black':  'transparent', borderBottomWidth: this.state.active === 1 ? 1: 0}]}>
                        <Text style={[styles.text, {color: this.state.active === 1 ? 'black': 'rgb(170, 170, 170)'}]}>دسته 4</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </ScrollView>
        );
    }
}