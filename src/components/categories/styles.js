
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        transform: [
            {rotateY: '180deg'},
        ],
    },
    navContainer: {
        alignItems: "center",
        justifyContent: 'center',
        width: 84,
        paddingBottom: 40,

    },
    text: {
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        // paddingBottom: 55,
        position: 'absolute',
        bottom: 15

    }
});