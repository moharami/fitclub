import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ImageBackground
} from 'react-native';

import Swiper from 'react-native-swiper';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

const styles = StyleSheet.create({
    wrapper: {
        width: '100%',
        height: 260
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },
    mainImage:{
        width: '100%',
        height: 260,
        // marginRight: 10,
        overflow: 'hidden',
        alignItems: "flex-end",
        justifyContent: "flex-end",
        marginRight: 30
    },
    imageText: {
        fontSize: 18,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: "black",
        textAlign: 'center'
    },
    visited: {
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingRight: 10
        // color: "white",
    },
    visitedContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        borderRadius: 20,
        paddingRight: 8,
        paddingLeft: 8,
    },
    buttonText: {
        color: 'white'
    }
});

export default class ArticleSlideShow extends Component {
    render(){
        return (
            <Swiper style={styles.wrapper} showsButtons={true} showsPagination={false} autoplay={false}
                    nextButton={<Icon name="chevron-right" color="white" size={14} />}
                    prevButton={<Icon name="chevron-left" color="white" size={14} />}

            >
                <ImageBackground source={{uri: "http://fitclub.ws/files?uid="+this.props.sliderSrc+"&width=300&height=100"}} style={styles.mainImage}>
                    <View style={{backgroundColor:'rgba(0,0,0,.3)',
                        width: "100%",
                        height: 260,
                        alignItems: "flex-end",
                        justifyContent: "flex-end",
                        padding: 10,
                        zIndex: 9999
                    }}>
                        <View style={styles.visitedContainer}>
                            <Text style={styles.visited}>1243</Text>
                            <Icon name="eye" size={14} color="gray" />
                        </View>
                        <Text style={styles.imageText}>برنامه رژیم لاغری برای از بین بردن چربی شکم</Text>
                    </View>
                </ImageBackground>
                {/*<ImageBackground source={{uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT7oZ4kB0CPfNZBhB716E1dagG_E5h3VXVbEiRa74SV0-Kl220oPQ'}} style={styles.mainImage}>*/}
                    {/*<View style={{backgroundColor:'rgba(0,0,0,.1)',*/}
                        {/*width: "100%",*/}
                        {/*height: 260,*/}
                        {/*alignItems: "flex-end",*/}
                        {/*justifyContent: "flex-end",*/}
                        {/*padding: 10*/}
                    {/*}}>*/}
                        {/*<View style={styles.visitedContainer}>*/}
                            {/*<Text style={styles.visited}>1243</Text>*/}
                            {/*<Icon name="eye" size={14} color="gray" />*/}
                        {/*</View>*/}
                        {/*<Text style={styles.imageText}>برنامه رژیم لاغری برای از بین بردن چربی شکم</Text>*/}
                    {/*</View>*/}
                {/*</ImageBackground>*/}
            </Swiper>
        );
    }
}

