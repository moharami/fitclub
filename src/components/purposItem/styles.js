
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {

        flexDirection: 'row',
        alignItems: "flex-end",
        justifyContent: 'space-between',
        // paddingRight: 15,
        // paddingLeft: 15,
        // paddingBottom: 10,
        borderColor: 'rgb(237, 237, 237)',
        borderWidth: 1,
        marginBottom: 20

    },
    masterContainer: {
        // position: 'relative',
        flex: 1,
        width: '100%'

    },
    icon: {
        position: 'absolute',
        top: 0,
        right: 0,
        zIndex: 9999

    },
    left: {
        alignItems: "center",
        justifyContent: 'center',
        width: '53%',
        height: 92,
        paddingLeft: 10

    },
    right: {
        alignItems: "center",
        justifyContent: 'center',
        height: 92,
        width: '47%'

    },
    value: {
        color: 'black',
        fontSize: 25,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    textContainer: {
        alignItems: "flex-end",
        justifyContent: 'flex-end'
    },
    label: {
        color: '#b7b7b8',
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    labelValue: {
        color: '#c0c0c1',
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    image: {
        width: 42,
        height: 37,
        resizeMode: 'contain'
    },
    header: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-end'
    },
    purpos: {
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingRight: 10
    }
});
