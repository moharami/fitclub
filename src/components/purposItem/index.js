import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import styles from './styles'
import image1 from '../../assets/purposImages/loss.png';
import image2 from '../../assets/purposImages/muscle.png';
import image3 from '../../assets/purposImages/fatBurn.png';
import image4 from '../../assets/purposImages/fitness.png';
import image5 from '../../assets/purposImages/bodyBuilding.png';
import image6 from '../../assets/purposImages/cure.png';
import image7 from '../../assets/purposImages/food.png';
import Icon from 'react-native-vector-icons/Ionicons';
import OIcon from 'react-native-vector-icons/dist/Octicons';

class PurposItem extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: ''
        };
    }
    render() {
        console.log('componentDta', this.props.data)
        let src =  null;
        switch (this.props.imgSrc) {
            case 'image1':
                src = image1;
                break;
            case 'image2':
                src = image1;
                break;
            case 'image3':
                src = image2;
                break;
            case 'image4':
                src = image3;
                break;
            case 'image5':
                src = image4;
                break;
            case 'image6':
                src = image5;
                break;
            case 'image7':
                src = image6;
                break;
            case 'image8':
                src = image7;
        }
        return (
            <TouchableOpacity style={styles.masterContainer} onPress={() => this.props.handlePress()}>
                <View style={[styles.container, {marginRight: 7, marginLeft: 7, backgroundColor: this.props.data[this.props.id-1]? 'white' : 'rgb(246, 246, 246)'}]}>
                    <View style={styles.left}>
                        {this.props.data[this.props.id - 1] ?
                            <View>
                                <Text style={[styles.purpos, {color: this.props.data[this.props.id-1] ? 'black': 'gray'}]}>{this.props.purpose}</Text>
                                <Text style={styles.value}>{this.props.data[this.props.id - 1].data}</Text>
                            </View>
                            :
                            <View>
                                <Text style={[styles.purpos, {color: this.props.data[this.props.id-1] ? 'black': 'gray'}]}>{this.props.purpose}</Text>
                                <OIcon name="dash" size={25} color="rgb(125, 125, 127)"/>
                            </View>
                        }
                    </View>
                    <View style={styles.right}>
                        <View style={styles.header}>
                            <Image source={src} style={[styles.image, {tintColor: this.props.data[this.props.id - 1] ? 'red': 'gray'}]} />
                        </View>
                        <View style={styles.textContainer}>
                            <Text style={styles.label}>{this.props.label}</Text>
                            <Text style={styles.labelValue}>{this.props.labelValue}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>

        );
    }
}
export default PurposItem;