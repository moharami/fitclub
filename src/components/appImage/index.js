import React, {Component} from 'react';
import {
    Image,
} from 'react-native';

class AppImage extends Component {
    state = {...this.props};

    render() {
        const {source, style} = this.state;
        return (
            <Image
                source={{uri: source}}
                style={style}
                onError={() => this.setState({source: 'not_found.jpg'})}
            />
        )
    }
}

export default AppImage