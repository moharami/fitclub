import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import styles from './styles'
import {Actions} from 'react-native-router-flux'

export default class FooterMenu extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            active: 0
        };
    }
    handlePress(number) {
        switch (number){
            case 4:
                Actions.push('UserProfile');
                break;
            case 3:
                Actions.push('ContactUs');
                break;
            case 2:
                Actions.push('UserPrograms');
                break;
            case 1:
                Actions.push('home');
                break;
            default:
                break;
        }
    }
    render(){
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => this.handlePress(4)}>
                    <View style={[styles.navContainer, {borderTopColor: this.props.active === 'user' ? 'rgb(240, 122, 133)':  'transparent', borderTopWidth: this.props.active === 'user' ? 2: 0}]}>
                        <FIcon name="user" size={30} color={this.props.active === 'user' ? 'rgb(240, 122, 133)': 'gray'} />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.handlePress(3)}>
                    <View style={[styles.navContainer, { borderTopColor: this.props.active === 'contact' ? 'rgb(240, 122, 133)':  'transparent', borderTopWidth: this.props.active === 'contact' ? 2: 0}]}>
                        <FIcon name="phone-call" size={25} color={this.props.active === 'contact' ? 'rgb(240, 122, 133)': 'gray'} />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.handlePress(2)}>
                    <View style={[styles.navContainer, { borderTopColor: this.props.active === 'program' ? 'rgb(240, 122, 133)':  'transparent', borderTopWidth: this.props.active === 'program' ? 2: 0}]}>
                        <Icon name="mobile" size={40} color={this.props.active === 'program' ? 'rgb(240, 122, 133)': 'gray'} />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.handlePress(1)}>
                    <View style={[styles.navContainer, { borderTopColor: this.props.active === 'home' ? 'rgb(240, 122, 133)':  'transparent', borderTopWidth: this.props.active === 'home' ? 2: 0}]}>
                        <FIcon name="home" size={30} color={this.props.active === 'home' ? 'rgb(240, 122, 133)': 'gray'} />
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}