
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        // flex: 1,
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-around',
        backgroundColor: 'white'
    },
    navContainer: {
        alignItems: "center",
        justifyContent: 'center',
        paddingTop: 6,
        paddingBottom: 3

    }
});