import React, {Component} from 'react';
import {Dimensions} from 'react-native';
import {Router, Stack, Scene, Drawer, Actions, ActionConst} from 'react-native-router-flux'
import Sidebar from '../sidebar'
import Info from '../screens/info'
import Home from '../screens/home'
import Login from '../screens/Login'
import Confirmation from '../screens/confirmation'
import Profile from '../screens/profile'
import Search from '../screens/search'
import CommonQuestion from '../screens/commonQuestion'
import Payment from '../screens/payment'
import Receipt from '../screens/receipt'
import Wallet from '../screens/wallet'
import UserProfile from '../screens/userProfile'
import EditProfile from '../screens/editProfile'
import ArticleDetail from '../screens/articleDetail'
import UserPrograms from '../screens/userPrograms'
import Program from '../screens/program'
import FoodProgram from '../screens/foodProgram'
import AddProgram from '../screens/addProgram'
import ProgramFeature from '../screens/programFeature'
import BodyFeatures from '../screens/bodyFeatures'
import SportHistory from '../screens/sportHistory'
import SecondQuestion from '../screens/sportHistory/secondQuestion'
import Tickets from '../screens/tickets'
import AddTickets from '../screens/addTickets'
import TicketDetail from '../screens/ticketDetail'
import ThirdQuestion from '../screens/sportHistory/thirdQuestion'
import MedicalHistory from '../screens/medicalHistory'
import FoodHabbitation from '../screens/foodHabbitation'
import FoodHabbitationNext from '../screens/foodHabbitation/FoodHabbitationNext'
import PersonalHabits from '../screens/personalHabits'
import PracticeInfo from '../screens/practiceInfo'
import Purpose from '../screens/purpose'
import FinalConfirm from '../screens/finalConfirm'
import Pregnant from '../screens/pregnant'
import ContactUs from '../screens/contactUs'
import Splash from '../screens/splash'
import {store} from '../config/store';



import {Provider, connect} from 'react-redux';

const onBackAndroid = () => {
    return Actions.pop();
};
class MainDrawerNavigator extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }



    render() {
        const RouterWidthRedux = connect()(Router);

        return (
                <Provider store={store}>
                    <RouterWidthRedux>
                        <Scene>
                            <Scene key="root" hideNavBar>
                                {/*<Scene key="Splash" component={Splash} initial hideNavBar/>*/}
                                <Scene key="Info"  component={Info} title="Info" hideNavBar/>
                                <Scene key="Login" component={Login} title="Login" hideNavBar/>
                                <Scene key="confirmation" component={Confirmation} title="Confirmation" hideNavBar/>
                            </Scene>
                            <Drawer
                                drawerPosition='right'
                                key="drawer"
                                contentComponent={Sidebar}
                                drawerWidth={Dimensions.get('window').width* .8}
                                hideNavBar>
                                <Stack key="container">


                                    <Scene key="profile" component={Profile} title="Profile" hideNavBar/>
                                    <Scene key="home" component={Home} title="Home" hideNavBar/>
                                    <Scene key="Search" component={Search} title="Search" hideNavBar/>
                                    <Scene key="commonQuestion" component={CommonQuestion} title="CommonQuestion" hideNavBar/>
                                    <Scene key="payment" component={Payment} title="Payment" hideNavBar/>
                                    <Scene key="Receipt" component={Receipt} title="Receipt" hideNavBar/>
                                    <Scene key="wallet" component={Wallet} title="Wallet" hideNavBar store={store}/>
                                    <Scene key="UserProfile" component={UserProfile} title="UserProfile" hideNavBar store={store} />
                                    <Scene key="editProfile" component={EditProfile} title="EditProfile" hideNavBar store={store} />
                                    <Scene key="articledetail" component={ArticleDetail} title="ArticleDetail" hideNavBar store={store}/>
                                    <Scene key="UserPrograms" component={UserPrograms} title="UserPrograms" hideNavBar store={store}/>
                                    <Scene key="program" component={Program} title="Program" hideNavBar/>
                                    <Scene key="foodProgram" component={FoodProgram} title="FoodProgram" hideNavBar/>
                                    <Scene key="AddProgram" component={AddProgram} title="AddProgram" hideNavBar store={store} />
                                    <Scene key="programFeature" component={ProgramFeature} title="ProgramFeature" hideNavBar store={store} />
                                    <Scene key="bodyFeatures" component={BodyFeatures} title="BodyFeatures" hideNavBar/>
                                    <Scene key="sportHistory" component={SportHistory} title="SportHistory" hideNavBar/>
                                    <Scene key="secondQuestion" component={SecondQuestion} title="SecondQuestion" hideNavBar/>
                                    <Scene key="thirdQuestion" component={ThirdQuestion} title="ThirdQuestion" hideNavBar/>
                                    <Scene key="medicalHistory" component={MedicalHistory} title="MedicalHistory" hideNavBar/>
                                    <Scene key="foodHabbitation"  component={FoodHabbitation}  title="FoodHabbitation" hideNavBar/>
                                    <Scene key="foodHabbitationNext"  component={FoodHabbitationNext}  title="FoodHabbitationNext" hideNavBar/>
                                    <Scene key="personalHabits"  component={PersonalHabits} title="PersonalHabits" hideNavBar/>
                                    <Scene key="practiceInfo" component={PracticeInfo} title="PracticeInfo" hideNavBar/>
                                    <Scene key="purpose"  component={Purpose} title="Purpose" hideNavBar/>
                                    <Scene key="finalConfirm" component={FinalConfirm} title="FinalConfirm" hideNavBar/>
                                    <Scene key="ContactUs" component={ContactUs} title="ContactUs" hideNavBar/>
                                    <Scene key="Splash" component={Splash} title="Splash" hideNavBar/>
                                    <Scene key="tickets" component={Tickets} title="Tickets" hideNavBar/>
                                    <Scene key="addTickets" component={AddTickets} title="AddTickets" hideNavBar store={store}/>
                                    <Scene key="ticketDetail" component={TicketDetail} title="TicketDetail" hideNavBar/>
                                    <Scene key="pregnant" component={Pregnant} title="Pregnant" hideNavBar/>
                                </Stack>
                            </Drawer>
                        </Scene>
                    </RouterWidthRedux>
                </Provider>

        );
    }
}
export default MainDrawerNavigator;



